\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. 
% If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage{listings} % needed for the inclusion of source code

\usepackage{mips}
\usepackage[capitalise]{cleveref}
%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ASSEMBLY SYNTAX %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{ %
	language=Verilog,
  basicstyle=\footnotesize\ttfamily,
	numbers=left,      
	frame=tb,            
  captionpos=t,
	numberstyle=\tiny\ttfamily\color{gray}, 
  float=tp,
  floatplacement=tbp,
	stepnumber=1,                  
	numbersep=5pt,                  
	backgroundcolor=\color{white},  
	showspaces=false,               
	showstringspaces=false,         
	showtabs=false,                 
	rulecolor=\color{black},        
	tabsize=3,                      
	breaklines=true,      
	breakatwhitespace=false,        
	keywordstyle=\color{blue},     
	commentstyle=\color{dkgreen},  
	stringstyle=\color{mauve},     
	escapeinside={\%*}{*)},        
	morekeywords={*,...}           
}

\makeatletter
\def\lst@makecaption{%
	\def\@captype{table}%
	\@makecaption
}

\makeatother

\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}

\renewcommand{\ttdefault}{cmtt}
\renewcommand\lstlistingname{\textsc{Code Snippet}}
\crefname{listing}{\textsc{Code Snippet}}{\textsc{Code Snippet}}


%%%%%%%%%%%%%%%%%%%%%%%%%%
%% BEGIN DOCUMENT %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TITLE %%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{Putting Altogether in DaVinci v1.0 Computer System:
Verilog Behavioural/RTL Modelling}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% AUTHOR %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\author{\IEEEauthorblockN{Inhee Park}
\IEEEauthorblockA{\textit{Department of Computer Science}, 
\textit{San Jose State University}, San Jose, CA, USA \\ inhee.park@sjsu.edu} }

\maketitle
\thispagestyle{plain}
\pagestyle{plain}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ABSTRACT %%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}
The objective of this project is to build the integrated processor system,
called \emph{DaVinci v1.0} supporting \emph{CS147DV} instruction set,
by putting the HDL\footnote{Hardware Description Language} modules altogether:
a processor (including ALU, register file and control unit), 256MB memory,
a clock, and testbenches.
\end{abstract}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% KEY WORDS %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{IEEEkeywords}
Arithmetic Logical Unit (ALU), Hardware Description Language (HDL), 
Verilog, ModelSim, Simulator, Test Bench, Behavioural Modelling, Register-Transfer Level (RTL) Modelling
\end{IEEEkeywords}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INTRODUCTION %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\Large Introduction}
\paragraph{Objective}
The main aim of this project is to build a bare minimum computer system (``DaVinci v1.0'')
supporting the minimal 24 instruction set (``CS147DV'') listed in \cref{f:cs147dv}.
The DaVinci v1.0 computer system consists of 32-bit processor 
(where ALU, register file and control unit are inter-connected)
and 32x64M main memory (word addressable with 64M data lines) with a clock system.
To simulate behaviour among hardware in terms of data path and control path
in software environment, we use Verilog as one of Hardware Description 
Languages, also models at behavioural level.


\paragraph{HDL/Verilog/Behavioural Modelling}
There are three levels of modelling in Verilog:
behavioural model, RTL model and gate level model.
Behaviour model is where behaviour of logic is modelled using procedural blocks
such as \texttt{initial, always}.
At the behavioural level, the behaviour of logic is modelled; 
RTL model is logic is modelled at register level.

\paragraph{Outline of this report}
This report starts with detailed requirements of ALU and DaVinci v1.0 system is investigated in \cref{s:req}. Then the overview of the DaVinci system is provided in terms of constituent modules. Next, design outline and
detailed implementation are given for ALU, Register File, and Control Unit.
Majority of this project is to implement control unit system, where 
various R-type, I-type, and J-type instructions are implemented at each state 
of the instruction cycles (IF, ID/RF, EXE, MEM and WB). Finally, two given
benchmarking cases of Fibonacci sequence are tested. In addition, custom-version of
benchmarking case is provided for the iterative factorial case, ranging from 
creation of the memory data, all the way to memory dump and its investigation to
ensure correct implementation of the DaVinci v1.0 system. 
%%%%%%%%%%%%%%%%%%%%%%%%%%
%% REQUIREMENT %%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\Large Requirements for ALU}\label{s:req}
ALU is an arithmetic and logical unit that combines fundamental mathematical
(addition, subtraction, multiplication, and division) and basic logical operations
(AND, OR, NOR, Shift Right/Left, Set Less/Greater Than, etc.) into a single unit.
%
As depicted in the ALU digram in \cref{f:alu}, there are three inputs, namely, 
32-bit first operand (op1), 32-bit second operand (op2), and 6-bit operation (oprn) 
and one 32-bit output (result). Those width of inputs and output is defined in the Verilog file.
\begin{figure}[h!] 
  \centering
	\includegraphics[width=0.3\textwidth]{./Figs/ALU.png}
	\caption{Interface Diagram for ALU}
	\label{f:alu}
\end{figure}

\section{\Large Requirements for DaVinci v1.0}
\begin{itemize}
\item Download HDL source code from prj\_02.zip to start with.
\item Complete the following Verilog files to support CS147DV instruction set  (10 R-type, 10 I-type and 4 J-Type instructions in \cref{f:cs147dv}). 
(\texttt{alu.v, register\_file.v, control\_unit.v, da\_vinci\_tb.v}).
\item No modification on the other Verilog files: 
(\texttt{prj\_definition.v, clk\_gen.v, da\_vinci.v, mem\_64MB\_tb.v,
    memory.v, processor.v})
\item Complete the testbench and any test program code/data as required:
memory should be initialized using the given memory content file:
\texttt{mem\_content\_01.dat}.
\item Memory data files to be used for benchmarking are provided (RevFib.dat, fibonacci.dat)
as well as resulting memory dump files as references (RevFib\_mem\_dump.golden.dat,
fibonacci\_mem\_dump.golden.dat).
\end{itemize}
\begin{figure}[h!] 
  \centering
  \includegraphics[width=0.5\textwidth]{./Figs/CS147DV_R.png}
  \includegraphics[width=0.5\textwidth]{./Figs/CS147DV_I.png}
  \includegraphics[width=0.5\textwidth]{./Figs/CS147DV_J.png}
  \caption{The CS147DV Instruction Set}
	\label{f:cs147dv}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% DESIGN/IMPLEMENTATION %
%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%% DaVinci Model %%%%%%%%%%%%
\section{\Large Overview of DaVinci v1.0 Model}
Most of the Verilog source codes were given either complete form (as a reference) or skeletal form (connecting ports and wires were completed) along with several testbenches to test the each component of the system. 
The DaVinci v1.0 Model consists of the following eight Verilog modules:
\begin{enumerate}
  \item \texttt{alu.v} --- Model of ALU using in this processor.
  (Mostly done from ProjectI, ZERO signal functionality is required to be added)
\item \texttt{memory.v}  --- Parametrized memory model of a 32x64M memory.  
  (Completedly implemented as a reference for register file implementation and testing) 
\item \texttt{mem\_64MB\_tb.v}  --- Testbench source to test the memory model.
  (Completedly implemented as a reference for register file implementation and testing) 
\item \texttt{register\_file.v}  --- 32x32 register file (RF) model.
  \texttt{(**Need to be completed**)}
\item \texttt{control\_unit.v}  --- The control unit (CU) of the process.
  \texttt{(**Need to be completed**)}
\item \texttt{processor.v}  --- Processor implementation integrating CU, RF and ALU.
  (Completedly implemented for wires connection)
\item \texttt{da\_vinci.v}  --- Complete system integrating processor and 32x64M memory. 
  (Completedly implemented for integration)
\item \texttt{da\_vinci\_tb.v}  --- Testbench source to test the DaVinci 1.0 system.
  \texttt{(**Need to be completed**)}
\end{enumerate}

\begin{figure}[h!] 
  \centering
  \includegraphics[width=0.5\textwidth]{./Figs/davinci_model.png}
  \caption{Da Vinci Model}
  \label{f:davinci-model}
\end{figure}

\begin{figure}[h!] 
  \centering
  \includegraphics[width=0.5\textwidth]{./Figs/project2-model.png}
  \caption{Overview of ProjectII}
  \label{f:projII}
\end{figure}



%%%%%%%%% ALU %%%%%%%%%%%%
\section{\Large Design/Implementation of ALU}\label{s:alu}
\begin{enumerate}
\item We can copy ALU module from the Project-I.  
The funct codes defined in ALU module are \texttt{0x1 through 0x9},
however, those funct codes used in CS147DV is different 
as listed in \cref{f:cs147dv}. Thus we have to one-on-one map 
the original ALU funct code (\texttt{0x1 to 0x9}) to the corresponding
new funct code in CS147DV
(\texttt{0x20, 0x22, 0x2c, 0x24, 0x25, 0x27, 0x2a, 0x01, 0x02, 0x08})
when implementing Control Unit module (see \cref{code:exeR}).
\item We need to change the name of output port from `result'
to 'OUT' to be compatible with other modules in DaVinci v1.0 system.
\item To be used for branch type instruction (e.g. \texttt{bne, beq}),
we should extend the functionality to set the 'ZERO' output 
(set this ZERO flag 1 if ALU result is zero; set it 0 otherwise).
\end{enumerate}
\lstinputlisting[caption={Exceprt of ALU module pertinent to addition of ZERO 
    flag and its functionality},label={code:alu},float]
{Snippet/alu.v}

%%%%%%%%% RF %%%%%%%%%%%%
\section{\Large Design/Implementation of Register File}\label{s:rf}
\begin{enumerate}
\item Most of code was taken from the completed \texttt{memory.v} module
owing to the similarity as following:
\item Similar to 32x64M memory model, we need to define a register 
for the 32x32 storage (\texttt{rf\_32x32}) as a date type of array of 32-bit vector.
\item With that register, we can handle register-write-mode (\texttt{rf\_32x32[ADDR\_W] = DATA\_W;}).
\item In \texttt{initial} block, initialization of the registers. 
\item Within the \texttt{always} block, reset the register upon \texttt{RST===1'b0} upon
  negative edge of the clock signal; rest of the operations are done upon positive edge of the clock signal.
\end{enumerate}
\lstinputlisting[caption={Implementation of Register File Module},label={code:rf},float]
{Snippet/register_file.v}


%%%%%%%%% CU %%%%%%%%%%%%
\section{\Large Design/Implementation of Control Unit}\label{s:cu}
\subsection{\large State Machine Module}
The DaVinci v1.0 system supports the CS147DV instructions to be processed in 5 clock cycles, going through 5 states, namely, 1) instruction fetch $\to$ 2) instruction decode $\to$ 3) execution $\to$ 4) memory access $\to$ write back. Such a state machine is implemented by referring to the pre-defined states (in \texttt{prj\_definition.v}) 
in the module of \texttt{ROC\_SM(STATE,CLK,RST)}, where inputs are clock and reset signal; output is a state (\cref{code:sm}).
\lstinputlisting[caption={Defining current and next state for the instruction cycle},label={code:sm},float]
{Snippet/cu_state.v}
\lstinputlisting[caption={Definining registers for control unit},label={code:cu},float]
{Snippet/control_unit.v}
\lstinputlisting[caption={Implementation of Control Unit (CU) at IF State},label={code:if},float]
{Snippet/cu_if.v}
\begin{figure}[b!] 
	\centering
	\includegraphics[width=0.5\textwidth]{./Figs/proc_if.png}
	\caption{Instruction Fetch Process}
	\label{f:if}
\end{figure}
\subsection{\large Preliminary setup for the Control Unit}
\begin{enumerate}
\item 
Control Unit module centrally orchestrates input and output signals of
ALU (two operands and operation code), register files (two set of data/address ports, one set of writeable data/address and read/write signal), memory (address/data ports and read/write signal) upon responding to the clock/reset signals.
\item
To the given skeletal code of \texttt{control\_unit.v} with complete in/out ports, additional registers are defined used as data storage for the corresponding output ports. 
\item 
Memory is a type of \texttt{inout} port, thus we need to define a separate register, \texttt{mem\_data\_ret} to be used in case of write-mode (in case of read-mode, use already defined memory register, \texttt{MEM\_DATA}). 
\item 
It is required to add internal registers to support instructions whose operation needs to access PC register (\texttt{PC\_REG}) and Stack Pointer register (\texttt{SP\_REG}).
\item 
Finally, we need to define an instruction register (\texttt{INST\_REG}) along with a set of registers to store parsed instructions.
\item Prior to define operations per instruction at each state, the above mentioned register files are defined as shown in \cref{code:cu}.
\end{enumerate}



\subsection{\large Instruction Fetch (`PROC\_FETCH)}
As depicted in \cref{f:if}, the very first task 
is to fetch the instruction from the main memory.
We can do so by setting memory address to program counter with memory control as read mode,
register file control as hold mode (i.e. read/write both 00 or 11).
Corresponding implementation snippet is shown in \cref{code:if}.

\subsection{\large Instruction Decode (`PROC\_DECODE)}
Here we use instruction register (\texttt{INST\_REG}) to store the fetched instruction from the memory.
Main tasks in this stage are 1) to parse the instruction into required fields 
(opcode, rs, rt, rd, shamt, funct, immediate, address depending on the type of instructions);
2) to print out the current instruction fetched by using the given task code 
(\texttt{print\_instruction(INST\_REG)}); and 3)  
to define various type of immediate addresses (sign extension, zero extension, LUI).
Corresponding code snippet is shown in \cref{code:id}.


\begin{figure}[h!] 
	(a)\\\includegraphics[width=0.5\textwidth]{./Figs/R-type-general.png}
	(b)\\\includegraphics[width=0.5\textwidth]{./Figs/R-type-jr.png}
	(c)\\\includegraphics[width=0.5\textwidth]{./Figs/R-type-shift.png}
\caption{R-type Instruction Data/Control Path: (a) general; (b) jr; and (c) shift types}
	\label{f:R}
\end{figure}
%%%%%%%%% R-type
\subsection{\large Instruction Execution (`PROC\_EXE) : R-type}
In the execution stage, main task is to set the ALU operands and operation according to the
opecode/funct. The R-type instructions can be classified into 
general (\cref{f:R}a), jump register (\cref{f:R}b) and shift type (\cref{f:R}c) .  Corresponding data/control paths 
are depicted in \cref{f:r-gen} with detailed annotation of the implementation.
Code snippet is provided in \cref{code:exeR}.

\lstinputlisting[caption={Implementation of R-type Instructions at EXE Stae in CU},label={code:exeR},float]
{Snippet/cu_exe_r.v}
\begin{figure}[h!] 
(a)\\\includegraphics[width=0.5\textwidth]{./Figs/I-type-arith.png}
(b)\\\includegraphics[width=0.5\textwidth]{./Figs/I-type-branch.png}
(c)\\\includegraphics[width=0.5\textwidth]{./Figs/I-type-logic.png}  
\caption{I-type Instruction Data/Control Paths (1): 
	(a) arithmetic; (b) branch; and	(c) logic types}
	\label{f:I1}
\end{figure}
%%%%%%%%% I-type
\subsection{\large Instruction Execution (`PROC\_EXE) : I-type}
The I-type instructions can be classified into 
1) arithmetic, 2) branch, 3) logic, 4) LUI and 5) lw \& sw types.  
For correct implementations of I-tye instruction for the EXE stage, 
we followed the data/control paths 
depited in the schematic diagram
for arithmetic type (\cref{f:I1}a), 
for branch type (\cref{f:I1}b), 
for logic type (\cref{f:I1}c), 
for LUI type (\cref{f:I2}d),
for LW type (\cref{f:I2}e) and
and for SW type (\cref{f:I2}e).
And corresponding Verilog codes are given in \cref{code:exeI}.

\lstinputlisting[caption={Implementation of I-type Instructions at EXE State in CU},label={code:exeI},float]
{Snippet/cu_exe_i.v}

\lstinputlisting[caption={Implementation of Control Unit (CU) at ID/RF State},label={code:id},float]
{Snippet/cu_id.v}

\begin{figure}[h!] 
	(d)\\\includegraphics[width=0.5\textwidth]{./Figs/I-type-lui.png}
	(e)\\\includegraphics[width=0.5\textwidth]{./Figs/I-type-lw.png}
	(f)\\\includegraphics[width=0.5\textwidth]{./Figs/I-type-sw.png}    
	\caption{I-type Instruction Data/Control Paths (2): 
(d) LUI; (e) LW and (f) SW types}	
	\label{f:I2}
\end{figure}

%%%%%%%%% J-type
\subsection{\large Instruction Execution (`PROC\_EXE) : J-type}
For the J-type instructions, there are mainly three types depicted in the schematic diagram showing
the data/control path with annotations for implementation:
jump-and-link type (\cref{f:J}a),
jump type (\cref{f:J}b)
and stack pointer type such as pop/push (\cref{f:J}c).
Detailed code snippets are in \cref{code:exeJ}.
\lstinputlisting[caption={Implementation of J-type Instructions at EXE State in CU},label={code:exeJ},float]
{Snippet/cu_exe_j.v}
\begin{figure}[h!] 
(a)\\\includegraphics[width=0.5\textwidth]{./Figs/J-type-jal.png}
(b)\\\includegraphics[width=0.5\textwidth]{./Figs/J-type-jmp.png} 
(c)\\\includegraphics[width=0.5\textwidth]{./Figs/J-type-stack.png} \caption{J-Type Instruction Data/Control Paths:
(a)Jal; (b) Jmp; and (c) Stack types}
  \label{f:J}
\end{figure}


\subsection{\large Memory Access (`PROC\_MEM)}
There are only four instructions \texttt{lw, sw, push}, and \texttt{pop} in the CS147DV set
to be associated with memory related operations. 
For the \texttt{lw, pop}, memory read operation is required ($MEM\_READ=1$), whereas for the \texttt{sw, push}
memory write operation is required ($MEM\_WRITE=1$). Corresponding implementation is show in 
\cref{code:mem}.
\lstinputlisting[caption={Implementation of CU at MEM State},label={code:mem},float]
{Snippet/cu_mem.v}

\subsection{\large Write Back (`PROC\_WB)}
In this final state of instruction cycle, write back to either register file or PC register.
Main tasks are 1) increase the PC register by one to point the next instruction to be fetched;
2) reset the memory write signal to no-op (00 or 11);
3) for most of instructions, set the register writing address, data and control to write back into register file; 
4) for the branch instructions and jump instructions, modify PC register address depending on the branch 
address or jump address.
Implementations of R-, I-, and J-type instruction for the final WB stage are shown in 
\cref{code:wbR}, \cref{code:wbR}, and \cref{code:wbR}, respectively.
\lstinputlisting[caption={Implementation of R-type Instructions at WB State in CU},label={code:wbR},float]
{Snippet/cu_wb_r.v}
\lstinputlisting[caption={Implementation of I-type Instructions at WB State in CU},label={code:wbI},float]
{Snippet/cu_wb_i.v}
\lstinputlisting[caption={Implementation of J-type Instructions at WB State in CU},label={code:wbJ},float]
{Snippet/cu_wb_j.v}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TEST STRATEGY & IMPLEMENTATION & REUSLTS %%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\Large Testbench for ALU}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{\large Strategy \& Implementation : ALU}
Testbench is an HDL module that is used to test another unit module, which is called
Unit Under Test (UUT), similar strategy to Unit Testing in used software engineering. 
The testbench contains statements 1) to apply inputs to the UUT by means of stimulus,
2) to check that the correct outputs are produced to verify the implemented unit. The input stimulus
and desired output patterns are called test vectors \cite{Harris4}. \cref{f:testbench} 
depicts the concept of Testbench, aka UUT, describing generation of stimulus \& verification of the 
resulting signal by the module to be tested against desired signal produced from the testbench.

We used the same testbench for ALU from the Project-I, 
\texttt{alu\_tb.v} (\cref{code:testbench}) is a testbench module 
written in Verilog to benchmark the implemented ALU module against benchmarking 
signal generated from testbench.   
\begin{figure}[h!] 
	\centering
	\includegraphics[width=0.5\textwidth]{./Figs/testbench.png}
	\caption{Testbench Diagram for Testing for ALU under Test}
	\label{f:testbench}
\end{figure}

\subsection{\large Testing Results : ALU}
Testing results can be verified by text format printed in the transcript text file
generated upon completion of the testbench execution, also by observation of the
waveform in ModelSim shown in \cref{f:testbench-alu}.
\begin{figure}[h!] 
	\centering
  \includegraphics[width=0.5\textwidth]{./Figs/alu_tb_text.png}
  \includegraphics[width=0.5\textwidth]{./Figs/alu_tb_wave.png}
  \caption{Result of ALU Testbench in transcript and in waveform}
	\label{f:testbench-alu}
\end{figure}

\section{\Large Testbench for Register File (RF)}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{\large Strategy \& Implementation : RF}
As shown in \cref{code:rf_tb} 
the testbench for the register file (\texttt{reg\_32x32\_tb.v})
was written similar to the testbench for the memory (\texttt{mem\_64MB\_tb.v}).
For simple testing purpose, write values from 0 to 9 and then read,
followed by write test to \texttt{RF\_DATA\_R1} and \texttt{RF\_DATA\_R2}.
\lstinputlisting[caption={Exceprt of Implementated Testbench for RF},label={code:rf_tb},float]
{Snippet/reg_32x32_tb.v}
\subsection{\large Testing Results : RF}
Testbench output is verified by text format (passing read and write test cases) 
as well as by observing the corresponding waveform in decimal radix format (\cref{f:testbench-rf}).
\begin{figure}[h!] 
	\centering
  \includegraphics[width=0.5\textwidth]{./Figs/rf_tb_text.png}
  \includegraphics[width=0.5\textwidth]{./Figs/rf_tb_wave.png}
  \caption{Result of Register File Testbench in transcript and in waveform}
  \label{f:testbench-rf}
\end{figure}

\section{\Large Testbench for Memory (MEM)}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{\large Strategy \& Implementation : MEM}
\begin{itemize}
  \item In the testbench for memory (\texttt{mem\_64MB\_tb.v})
    first dump test is to write values ranging from 1 to 9,
    then read, and then write back to the read values (1 to 9) 
    to the memory addresses at 0x00000 to 0x0009.
   \item Second dump test is to write the incremented values 
     with a base value of 0x00414020 (\texttt{load\_data})
     to the memory address starting from 0x001000 to 0x001010.
   \item Corresponding operation is excerpted in \cref{code:mem_tb}.
\end{itemize}
\lstinputlisting[caption={Exceprt of Testbench for Memory},label={code:mem_tb},float]
{Snippet/mem_64MB_tb.v}
\subsection{\large Testing Results : MEM}
We could test memory module by text output, waveform,
as well as memory dump files (\cref{f:testbench-mem}):
\paragraph{mem\_dump\_01.dat} --- 
The memory content of the 16 address block (from 0x0000 to 0x000f)
was dumped. Only the first 9 lines show corresponding values from 0 to 9.  
Remainig lines from 10 to 16 show zero as expected.
\paragraph{mem\_dump\_02.dat} ---
The memory content of the 16 address block (from 0x0001000 to 0x000100f)
was dumped. All 16 lines have the incremented values from 0x00414020
to 0x0041402f.
\begin{figure}[h!] 
	\centering
  \includegraphics[width=0.5\textwidth]{./Figs/mem_tb_text.png}
  \includegraphics[width=0.5\textwidth]{./Figs/mem_tb_wave.png}
  \includegraphics[width=0.5\textwidth]{./Figs/mem_tb_dump1.png}
  \includegraphics[width=0.5\textwidth]{./Figs/mem_tb_dump2.png}
  \caption{Result of Memory Testbench in transcript and in waveform as well as in 
  memory dump files}
  \label{f:testbench-mem}
\end{figure}

\section{\Large Testbench for DaVinci v1.0}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{\large Testing Procedure \& Strategy}
\begin{figure}[h!] 
	\centering
  \includegraphics[width=0.5\textwidth]{./Figs/project_compile.png}
  \includegraphics[width=0.5\textwidth]{./Figs/davinci_tb_library.png}
  \caption{Result of Da Vinci System Testbench Compilation \& Library Loading}
  \label{f:testbench-dv0}
\end{figure}
\begin{figure}[t!] 
	\centering
	\includegraphics[width=0.5\textwidth]{./Figs/davinci_testbench.png}
	\caption{Schematics of the DaVinci Testbench}
	\label{f:davinci-tb}
\end{figure}
\begin{enumerate}
\item Once implementation of all the components of the DaVinci v1.0 system 
  is completed, in the PROJECT pane compile all the Verilog source files. 
\item Then in the \texttt{work} directory of the LIBRARY pane, 
  load the \texttt{da\_vinci\_tb} testbench by double click (\cref{f:testbench-dv0}).
\item By selecting the objects and add wave form to it, followed by clicking ``Run-all'' icon
  to execute simulation.
\item In the DaVinci v1.0 system, inputs are only RST and CLK, all other output ports are used 
for probing, depicted in \cref{f:davinci-tb}.
\item Two benchmarking cases were provided with memory data files and correct memory dump output files:
  Fibonacci sequence (\texttt{fibonacci.dat, fibonacci\_dump.golden.out}) 
    and the reverse version of Fibonacci sequence
    (\texttt{RevFib.dat, RevFib\_dump.golden.out}). 
\item 
    To verify the correct implementation supportive for the CS147DV instruction set,
    these memory data files are loaded into the DaVinci testbench. Once simulation is done,
    resulting memory dump files can be compared against the provided golden dump output. 
\item 
  Beside the given benchmarking cases, we can create memory data file
  by writing the assembly code (from the CS147DV instruction set), converting the
  assembly code to machine code. After loading the prepared memory data file,
  we can verify the correct implementation if we obtain the expected memory dump output.
\end{enumerate}
\subsection{\large Testing Results : DaVinci}


%%%%%%%%%%%%
\subsection{\large Benchmarking with Fibonacci Sequence using \texttt{sw}}
\paragraph{Loading Memory Data}
\lstinputlisting[caption={Memory Data File for Fibonacci Sequence},label={code:fibo},float]
{Snippet/fibonacci.dat}
First benchmarking case for the DaVinci v1.0 system is Fibonacci sequence,
where assembly codes were converted to machine codes in hexadecimal representation
(\cref{code:fibo}). Note that the annotated assembly code is 
consistent to the parsed instructions shown in the text output of the testbench
result (\cref{f:testbench-dv1}).  This consistency verifies that the implementation
is correct.

\paragraph{Result of the Memory Dump Output}
Given the following relation, $F_n = F_{n-1} + F_{n-2}$,
$F_0 = 1$ and $F_1 = 1$, thus expected 16 sequences at the memory address ranging
from \texttt{0x01000000 to 0x0100000f} are 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 
375 and 610 in decimal (equivalently \texttt{0x0000, 0x0001, 0x0002, 0x0003, 0x0005,
0x0008, 0x00d, 0x0015, 0x0022, 0x0037, 0x0059, 0x0090, 0x00e9, 0x0179}, and \texttt{0x0262}),
which is consistent to the values shown in the memory dump output (\cref{f:testbench-dv1}).
\begin{figure}[h!] 
	\centering
  \includegraphics[width=0.5\textwidth]{./Figs/davinci_tb_text.png}
  \includegraphics[width=0.5\textwidth]{./Figs/fib_dump.png}
  \caption{Result of DaVinci System Testbench for Fibonacci Sequence Case}
  \label{f:testbench-dv1}
\end{figure}

%%%%%%%%%%%%
\subsection{\large Benchmarking with Reverse Fibonacci Sequence using \texttt{push}}
\paragraph{Loading Memory Data}
Second benchmarking case for the DaVinci v1.0 system is a reverse Fibonacci sequence,
where push is used to stack the sequence.  Thus the smaller sequence is stored
at the smaller memory address, the larger sequence is located at the larger memory address.
Memory data consists of the converted machine codes in hexadecimal representation
from the assembly codes (\cref{code:Rfibo}). 
\lstinputlisting[caption={Memory Data File for Reverse Fibonacci},label={code:Rfibo},float]
{Snippet/RevFib.dat}

\paragraph{Result of the Memory Dump Output}
Given the first two sequences are 0x5 and 0x3, then keep subtracting the two numbers and
intermediate result is stored at the 1st register (\texttt{R[0]}), which is pushed
to the stack pointer (\texttt{\$sp}). Thus expected content on the stack pointer addresses
ranging from \texttt{0x03fffff0 to 0x03ffffff} the following in decimal representation: 
are 5, 3, 2(=5-3), 1(=3-2), 1(=2-1), 0(=1-1), 1(=1-0), -1(=0-1), 2(=1+1), -3(=-1-2),
5(=2+3), -8(=-3-5), 13(=5+8), -21(=-8-13), 34(=13+21), -55(=-21-34).
Corresponding 2's complement in hexadecimal representation are:
\texttt{0x5, 0x3, 0x2, 0x1, 0x1, 0x0, 0x1, 0xffffffff, 0x2, 0xfffffffd, 0x5, 0xfffffff8,
0xd, 0xffffffeb, 0x22, and 0xffffffc9}.  The result from the memory dump file shows
the exactly same sequence shown in \cref{f:testbench-dv2}.

\begin{figure}[h!] 
	\centering
  \includegraphics[width=0.5\textwidth]{./Figs/revF_dump.png}
  \caption{Result of DaVinci System Testbench for Reverse Fibonacci Case}
  \label{f:testbench-dv2}
\end{figure}

%%%%%%%%%%%%
\subsection{\large Benchmarking with Iterative Factorial using \texttt{push}}
\paragraph{Creating Memory Data from Assembly Code to Machine Code}
From the two Fibonacci benchmarking cases, we have verified that the implementation
of the DaVinci v1.0 system is correct.  In addition, to verify other instructions
from the CS147DV, we can create memory data file.  
\begin{enumerate}
\item We reused the MIPS assembly code for the iterative factorial calculation from
  one of the programming assignments in CS47. 
\item
    Original MIPS assembly code includes
    instructions such as \texttt{li, move, bgt}, which are not in the CS147DV set.
    Thus those unsupportive instructions are replaced with the instructions only supportive in the CS147DV.
    For example, \texttt{li} is replaced with \texttt{addi}; 
    \texttt{move} is replaced with \texttt{ori};
    and \texttt{bgt} is replaced with \texttt{slt} followed by \texttt{beq}.
\item
  Then the CS147DV assembly codes for the iterative factorial are converted to the
    corresponding 32-bit machine code by hand. Note that the first register is \texttt{R[0]}
    and its address is \texttt{0x0}. 
\item
  The memory data file is prepared consisting of the 32-bit machine code with annotations of
    corresponding assembly code (\cref{code:fact}).
\end{enumerate}

\lstinputlisting[caption={MIPS Assembly Code for the Iternative Factorial},label={code:mips},float]
{Snippet/p5.asm}

\paragraph{Loading Memory Data}
Similar to the reverse Fibonacci sequence benchmarking case, \texttt{push} is used to stack
the intermediate results from lower lower stack pointer address to the higher one.
The expected final result should be \texttt{10! = 3,628,800$_{10}$ = 0x375F00}.
\lstinputlisting[caption={Memory Data File for Iternative Factorial},label={code:fact},float]
{Snippet/factorial_iterative.dat}

\paragraph{Result of the Memory Dump Output}
By inspection of the parsed instructions in the text output (\cref{f:testbench-dv2}),
we could verify that the additional instructions in CS147DV 
(e.g. \texttt{beq, mul, slt}) are correct because the text output is consistent to 
the corresponding assembly code annotated in the memory file shown in \cref{code:fact}.

By memory dump at the stack pointer block ranging from \texttt{0x03fffff0 to 0x03ffffff},
iterative factorial results are the following in decimal representation:
\texttt{1!=1, 2!=2, 3!=6, 4!=24, 5!=120, 6!=720,
7!=5040, 8!=40320, 9!=362880, 10!=3628800}, the rest of six lines are zeros.  
Corresponding hexadecimal values are 
\texttt{0x1, 0x2, 0x6, 0x18, 0x78, 0x2D0, 0x13B0, 0x9D80, 0x58980, 0x375F00}, followed by
six lines of zeros, which is identical sequence in the memory dump output 
(\cref{f:testbench-dv2}).
\begin{figure}[h!] 
	\centering
  \includegraphics[width=0.5\textwidth]{./Figs/factorial_text.png}
  \includegraphics[width=0.5\textwidth]{./Figs/factorial10_dump.png}
  \caption{Result of DaVinci System Testbench for Iterative Factorial Case}
  \label{f:testbench-dv2}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% CONCLUSION 
%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\Large Conclusion}\label{s:conclusion}
This behavioural modelling for the DaVinci v1.0 computer system provided
invaluable opportunity to consolidate each hardware component
how they are interconnected through processor. In particular,
by detailed implementation of every single instruction in the CS147DV 
set, its corresponding data/control path can be well understood.
Most of all, the highlight of this project is memory data creation and loading the memory data, followed by memory dump under the testbench.
This consolidate framework provides an insight how the assembly code can be converted into machine code, and then interpreted to carry out instructions through the 5 clock cycles from IF, ID/RF, EXE, MEM to WB stages.
%%%%%%%%%%%%%%%%%%%%%
%% BIBLOIGRAPHY %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{thebibliography}{00}
\bibitem{Lect1} Kaushik Patra, CS147 Lecture Note 1, 
(Introduction to computer, Basic of Instruction Set, Arithmetic \& Logic Unit (ALU)),
SJSU, Spring Semester 2019
\bibitem{Lect11} Kaushik Patra, CS147 Lecture Note 11, 
(Putting together an ALU, Putting together a Processor),
SJSU, Spring Semester 2019
\bibitem{Lab3-4} Kaushik Patra, CS147 Lab 3-4, 
(Data Flow Modeling I-II), SJSU, Spring Semester 2019
\bibitem{Lab5} Kaushik Patra, CS147 Lab 5, 
(Memory Modeling), SJSU, Spring Semester 2019
\bibitem{Lab6} Kaushik Patra, CS147 Lab 6, 
(Project II: Da Vinci System and Testbench), SJSU, Spring Semester 2019
\bibitem{Lab7-10} Kaushik Patra, CS147 Lab 7-10, 
(Behavioral Model I-IV), SJSU, Spring Semester 2019
\bibitem{Harris5} Chapter 5. Digital Building Blocks; 
  David Harris and Sarah Harris. 2012. Digital Design and Computer Architecture, Second Edition (2nd ed.). Morgan Kaufmann Publishers Inc., San Francisco, CA, USA.
\bibitem{Harris4} Chapter 4. Hardware Description Language; Harris \& Harris, 2012
\end{thebibliography}

\vspace{12pt}
%%%%%%%%%%%%%%%%%%%%%%%%%%
%% END DOCUMENT %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
%\ref{AA}\cite{b6}
%\lstinputlisting{../MIPS\_Assembly/CS47_proj\_alu\_logical.asm}
%\begin{lstlisting}
%\end{lstlisting}
