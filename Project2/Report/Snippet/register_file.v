...
//////////////////////////////////////////////////
// RF : register_file.v (very much alike memory.v)
//////////////////////////////////////////////////
// 1. Add registers for corresponding output ports.
reg [`DATA_INDEX_LIMIT:0] DATA_R1_REG;
reg [`DATA_INDEX_LIMIT:0] DATA_R2_REG;

// 2. Add 32x32 memory storage similar to 32x64M memory in memory.v
reg [`DATA_INDEX_LIMIT:0] rf_32x32[0:`DATA_INDEX_LIMIT];
integer i;

assign DATA_R1 = ((READ===1'b1)&&(WRITE===1'b0))?DATA_R1_REG:{`DATA_WIDTH{1'bz}};
assign DATA_R2 = ((READ===1'b1)&&(WRITE===1'b0))?DATA_R2_REG:{`DATA_WIDTH{1'bz}};


// 3. Add 'initial' block for initializing content of all 32 registers as 0.
initial
begin // initial
  for(i = 0; i <= `DATA_INDEX_LIMIT; i = i + 1)
    rf_32x32[i] = {`DATA_WIDTH{1'b0}};
end // initial

always @ (negedge RST or posedge CLK)
begin // always @ (negedge RST or posedge CLK)
  // 4. Register block is reset on a negative edge of RST signal.
  if (RST === 1'b0) 
    begin //if (RST === 1'b0) 
      for(i = 0; i <= `DATA_INDEX_LIMIT; i = i + 1)
        rf_32x32[i] = {`DATA_WIDTH{1'b0}};
    end //if (RST === 1'b0) 
  else
    begin // else
    // 5. On read request, 
    // both the content from address ADDR_R1 and ADDR_R2 returned. 
    if ((READ === 1'b1) && (WRITE === 1'b0)) // read operation
      begin // read oprn
	    DATA_R1_REG =  rf_32x32[ADDR_R1];
	    DATA_R2_REG =  rf_32x32[ADDR_R2];
      end // read oprn
    // 6. On write request, ADDR_W content is modified to DATA_W.
    else if ((READ === 1'b0) && (WRITE === 1'b1)) // write operation
	    rf_32x32[ADDR_W] = DATA_W;
   end // else
end // always @ (negedge RST or posedge CLK)
endmodule // REGISTER_FILE_32x32
