`PROC_MEM: 
/////////////////
//Only 'lw', 'sw', 'push' and 'pop' instructions are involved in this stage. 
//By default, make the memory operation to 00 or 11 (which will set the memory 
//output to HighZ). For the four memory related operation, set the memory read 
//or write mode accordingly (e.g. for read you need to set MEM_READ to 1'b1 
//and MEM_WRITE to 1'b0). The Address for the stack operation needs to be set 
//carefully following the ISA specification.
//
begin 
  MEM_READ = 1'b0; MEM_WRITE = 1'b0;  // memory output to HighZ
  case(opcode)
  //I9)  6'h23 : lw  : R[rt]=M[R[rs]+SignExtImm] 
  6'h23:
    begin 
    MEM_READ = 1'b1; MEM_WRITE = 1'b0;  // read
    MEM_ADDR = ALU_RESULT;
    RF_ADDR_W = RF_DATA_R2; // rt
    RF_DATA_W = MEM_DATA;
    //$display("SW: RF_ADDR=0X%08h, RF_DATA=0X%08h", RF_ADDR_W, RF_DATA_W);
    end
  //I10) 6'h2b : sw  : M[R[rs]+SignExtImm]=R[rt]
  6'h2b:
    begin 
    MEM_READ = 1'b0; MEM_WRITE = 1'b1;  // write
    MEM_ADDR = ALU_RESULT;
    mem_data_ret = RF_DATA_R2; // rt
    end
  //J3)  6'h1b: push: M[$sp]=R[0]; $sp-=1
  6'h1b:
    begin 
    MEM_READ = 1'b0; MEM_WRITE = 1'b1;  // write
    MEM_ADDR = SP_REG; // $sp
    mem_data_ret = RF_DATA_R1; // R[0]
    SP_REG = ALU_RESULT; //sp-=1
    end
  //J4)  6'h1c: pop : $sp+=1; R[0]=M[$sp]
  6'h1c:
    begin 
    MEM_READ = 1'b1; MEM_WRITE = 1'b0;  // read
    SP_REG = ALU_RESULT; // sp+=1
    MEM_ADDR = SP_REG; // $sp
    RF_DATA_W = MEM_DATA;
    end
  endcase // opcode
end
