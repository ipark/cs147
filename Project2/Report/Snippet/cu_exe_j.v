`PROC_EXE : begin
case(opcode) 
  // J-Type //////////////////////////////////////
  //J1)  6'h02: jmp : PC=JumpAddr={6'b0, address}
  //J2)  6'h03: jal : R[31]=PC+1; PC=JumpAddr
  //J3)  6'h1b: push: M[$sp]=R[0]; $sp-=1
  6'h1b: begin
    ALU_OP1 = SP_REG;
    ALU_OP2 = 1'b1;
    ALU_OPRN = `ALU_OPRN_WIDTH'h02;
    RF_ADDR_R1 = 0; end
  //J4)  6'h1c: pop : $sp+=1; R[0]=M[$sp]
  6'h1c: begin
    ALU_OP1 = SP_REG;
    ALU_OP2 = 1'b1;
    ALU_OPRN = `ALU_OPRN_WIDTH'h01;
    RF_ADDR_R1 = 0; end
endcase //(opcode) 
end
