// 3d. Always at the change of processor state set the control signal 
// and address / data signal values properly for memory, register file and ALU.
always @ (proc_state)
begin // proc_state
case(proc_state)
`PROC_FETCH:
/////////////////
// Set memory address to program counter, memory control for read operation. 
// Also set the register file control to hold ({r,w} to 00 or 11) operation.
begin 
  MEM_ADDR = PC_REG;
  MEM_READ = 1'b1; MEM_WRITE = 1'b0; // memory control for read oprn
  RF_READ = 1'b0; RF_WRITE = 1'b0; // {r,w}=00
end 
