################
move $t1, $t0  # $t1 = N (read_int($t0))
li $t2, 1      # $t2 = product (=1; init)
li $t3, 1      # $t3 = j (=1; loop index)
loop: 
bgt $t3, $t1, exit   # if j(t3)>N(t1), exit
mul $t2, $t2, $t3    # product(t2) *= j(t3)
move $s0, $t2        # score result to $s0
addi $t3, $t3, 1     # j++
j loop 
exit: 
################

