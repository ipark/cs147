// 64MB memory instance
defparam mem_inst.mem_init_file = "mem_content_01.dat";
MEMORY_64MB mem_inst(.DATA(DATA), .ADDR(ADDR), .READ(READ), 
.WRITE(WRITE), .CLK(CLK), .RST(RST));
...
load_data = 'h00414020;
..
// Write cycle
for(i=1;i<10; i = i + 1) begin
#10     DATA_REG=i; READ=1'b0; WRITE=1'b1; ADDR = i; end
...
// Read Cycle
#10   READ=1'b0; WRITE=1'b0;
#5    no_of_test = no_of_test + 1;
      if (DATA !== {`DATA_WIDTH{1'bz}})
        $write("[TEST] Read %1b, Write %1b, expecting 32'hzzzzzzzz, 
      got %8h [FAILED]\n", READ, WRITE, DATA);
      else 
	no_of_pass  = no_of_pass + 1;

// test of write data
for(i=0;i<10; i = i + 1)
begin
#5      READ=1'b1; WRITE=1'b0; ADDR = i;
#5      no_of_test = no_of_test + 1;
        if (DATA !== i)
	    $write("[TEST] Read %1b, Write %1b, expecting %8h, 
    got %8h [FAILED]\n", READ, WRITE, i, DATA);
        else 
	    no_of_pass  = no_of_pass + 1;
end

// test for the initialize data
for(i='h001000; i<'h001010; i = i + 1)
begin
#5      READ=1'b1; WRITE=1'b0; ADDR = i;
#5      no_of_test = no_of_test + 1;
        if (DATA !== load_data)
            $write("[TEST] Read %1b, Write %1b, Addr %7h, expecting %8h, 
          got %8h [FAILED]\n", READ, WRITE, ADDR, load_data, DATA);
        else 
            no_of_pass  = no_of_pass + 1;
        load_data = load_data + 1;
end
...
