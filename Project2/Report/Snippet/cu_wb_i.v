`PROC_WB: 
/////////////////
begin // else if(proc_state === `PROC_WB)
  PC_REG = PC_REG + 1;
  MEM_READ = 1'b0; MEM_WRITE = 1'b0;  // no-op {00} 
  RF_READ = 1'b0; RF_WRITE = 1'b1; // RF-write
  case(opcode) 
  // I-Type //////////////////////////////////////
  //I1)  6'h08 : addi: R[rt]=R[rs]+SignExtImm              
  6'h08: begin                   
    RF_ADDR_W = rt;
    RF_DATA_W = ALU_RESULT; end
  //I2)  6'h1d : muli: R[rt]=R[rs]*SignExtImm
  6'h1d: begin                   
    RF_ADDR_W = rt;
    RF_DATA_W = ALU_RESULT; end
  //I3)  6'h0c : andi: R[rt]=R[rs]&ZeroExtImm
  6'h0c: begin                   
    RF_ADDR_W = rt;
    RF_DATA_W = ALU_RESULT; end
  //I4)  6'h0d : ori : R[rt]=R[rs]|ZeroExtImm
  6'h0d: begin                   
    RF_ADDR_W = rt;
    RF_DATA_W = ALU_RESULT; end
  //I6)  6'h0a : slti: R[rt]=(R[rs]<SignExtImm)?1:0
  6'h0a: begin                   
    RF_ADDR_W = rt;
    RF_DATA_W = ALU_RESULT; end
  //I7)  6'h04 : beq : If(R[rs]==R[rt]) PC=PC+1+BranchAddr
  6'h04:
    if (ZERO===1'b1) 
      PC_REG = BranchAddr; // PC=PC+1 already above
  //I8)  6'h05 : bne : If(R[rs]!=R[rt]) PC=PC+1+BranchAddr
  6'h05:
    if (ZERO===1'b0) 
      PC_REG = BranchAddr; // PC=PC+1 already above
  //I5)  6'h0f : lui : R[rt]={immediate,16'b0}
  6'h0f: begin                   
    RF_ADDR_W = rt;
    RF_DATA_W = Lui; end
  //I9)  6'h23 : lw  : R[rt]=M[R[rs]+SignExtImm] 
  6'h23: begin                   
    RF_ADDR_W = rt;
    RF_DATA_W = MEM_DATA; end
  //I10) 6'h2b : sw  : M[R[rs]+SignExtImm]=R[rt]
