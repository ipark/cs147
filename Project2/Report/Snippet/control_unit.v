//////////////////////////////////////////////
// 3. Control signal generation.
// 3a. Add registers for corresponding output ports.
// RF
reg [`DATA_INDEX_LIMIT:0] RF_DATA_W;
reg [`REG_INDEX_LIMIT:0] RF_ADDR_W, RF_ADDR_R1, RF_ADDR_R2;
reg RF_READ, RF_WRITE;
// ALU
reg [`DATA_INDEX_LIMIT:0]  ALU_OP1, ALU_OP2;
reg [`ALU_OPRN_INDEX_LIMIT:0] ALU_OPRN;
// MEM
reg [`DATA_INDEX_LIMIT:0]  MEM_ADDR;
reg MEM_READ, MEM_WRITE;

// 3b. Define a register for write data to memory and 
// connect DATA to this register similar to the DATA port in memory model 
// (but in opposite sense - in control unit, 
// for memory read operation DATA must be set to HighZ and 
// for write operation DATA must be set to internal write data register)
reg [`DATA_INDEX_LIMIT:0] mem_data_ret;
assign MEM_DATA=((MEM_READ===1'b0)&&(MEM_WRITE===1'b1))?mem_data_ret:{`DATA_WIDTH{1'bz}};


// 3c. Add internal registers as following
//    PC_REG : Holds program counter value
//    INST_REG : Stores the current instruction
//    SP_REF : Stack pointer register
reg [`DATA_INDEX_LIMIT:0] PC_REG, INST_REG, SP_REG;
// 
// I-type SignExtImm, ZeroExtImm, Lui, BranchAddr
reg [`DATA_INDEX_LIMIT:0] SignExtImm; // 32-bit vector
reg [`DATA_INDEX_LIMIT:0] ZeroExtImm; // 32-bit vector
reg [`DATA_INDEX_LIMIT:0] Lui; // 32-bit vector
reg [`DATA_INDEX_LIMIT:0] BranchAddr; // 32-bit vector
// J-Type JumpAddr (ZeroExt for 6-bit)
reg [`DATA_INDEX_LIMIT:0] JumpAddr; // 32-bit vector
// 32-bit instruction parsing 
reg [5:0] opcode; // 6-bit 
reg [4:0] rs;     // 5-bit
reg [4:0] rt;     // 5-bit
reg [4:0] rd;     // 5-bit
reg [4:0] shamt;  // 5-bit
reg [5:0] funct;  // 6-bit
reg [15:0] immediate; // 16-bit
reg [25:0] address;   // 26-bit

// initial PC power on address and SP
initial
begin
  PC_REG=`INST_START_ADDR; // 32'h00001000
  SP_REG=`INIT_STACK_POINTER; // 32'h03ffffff
end
