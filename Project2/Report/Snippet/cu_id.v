`PROC_DECODE : 
/////////////////
begin 
  // Store the memory read data into INST_REG; 
  // You may use the following task code to print the current instruction 
  // fetched (usage: print_instruction(INST_REG); ). 
  INST_REG = MEM_DATA;
  print_instruction(INST_REG);
  // It also shows how the instruction is parsed into required fields 
  // (opcode, rs, rt, rd, shamt, funct, immediate, address for 
  // different type of instructions). 
  {opcode, rs, rt, rd, shamt, funct} = INST_REG; // R-type
  {opcode, rs, rt, immediate} = INST_REG; // I-type
  {opcode, address} = INST_REG; // J-type
  // I-Type
  // You may want to calculate and store sign extended value of immediate, 
  // zero extended value of immediate, LUI value for I-type instruction. 
  // SignExtImm
  //6'h08 : addi: R[rt]=R[rs]+SignExtImm
  //6'h1d : muli: R[rt]=R[rs]*SignExtImm
  //6'h0a : slti: R[rt]=(R[rs]<SignExtImm)?1:0
  //6'h23 : lw  : R[rt]=M[R[rs]+SignExtImm] 
  //6'h2b : sw  : M[R[rs]+SignExtImm]=R[rt]
  SignExtImm = {{16{immediate[15]}}, immediate};
  // ZeroExtImm
  //6'h0c : andi: R[rt]=R[rs]&ZeroExtImm
  //6'h0d : ori : R[rt]=R[rs]|ZeroExtImm
  ZeroExtImm = {{16'b0}, immediate};
  // Lui
  //6'h0f : lui : R[rt]={immediate,16'b0}
  //Lui = {immediate, {16'b0}};
  Lui = {immediate, 16'h0000};
  //BranchAddr
  //6'h04 : beq : If(R[rs]==R[rt]) PC=PC+1+BranchAddr
  //6'h05 : bne : If(R[rs]!=R[rt]) PC=PC+1+BranchAddr
  BranchAddr = {{16{immediate[15]}}, immediate};
  // J-type
  // For J-type instruction it would be good to store the 32-bit 
  // jump address from the address field. 
  // JumpAddr
  //6'h02 : jmp : PC=JumpAddr={6'b0, address}
  //6'h03 : jal : R[31]=PC+1; PC=JumpAddr
  JumpAddr = {{6'b0}, address};
  // Also set the read address of RF as rs and 
  // rt field value with RF operation set to reading.
  RF_ADDR_R1 = rs; 
  RF_ADDR_R2 = rt; 
  RF_READ = 1'b1; RF_WRITE = 1'b0; // {r,w}=10
end 
