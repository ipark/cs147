`PROC_EXE : begin
case(opcode) 
  // I-Type //////////////////////////////////////
  //I1)  6'h08 : addi: R[rt]=R[rs]+SignExtImm              
  6'h08: begin 
    ALU_OP1 = RF_DATA_R1;           
    ALU_OP2 = SignExtImm;
    ALU_OPRN = `ALU_OPRN_WIDTH'h01; end 
  //I2)  6'h1d : muli: R[rt]=R[rs]*SignExtImm
  6'h1d: begin 
    ALU_OP1 = RF_DATA_R1;           
    ALU_OP2 = SignExtImm;
    ALU_OPRN = `ALU_OPRN_WIDTH'h03; end 
  //I3)  6'h0c : andi: R[rt]=R[rs]&ZeroExtImm
  6'h0c: begin 
    ALU_OP1 = RF_DATA_R1;           
    ALU_OP2 = ZeroExtImm;
    ALU_OPRN = `ALU_OPRN_WIDTH'h06; end 
  //I4)  6'h0d : ori : R[rt]=R[rs]|ZeroExtImm
  6'h0d: begin 
    ALU_OP1 = RF_DATA_R1;           
    ALU_OP2 = ZeroExtImm;
    ALU_OPRN = `ALU_OPRN_WIDTH'h07; end 
  //I5)  6'h0f : lui : R[rt]={immediate,16'b0}
  //I6)  6'h0a : slti: R[rt]=(R[rs]<SignExtImm)?1:0
  6'h0a: begin 
    ALU_OP1 = RF_DATA_R1;           
    ALU_OP2 = SignExtImm;
    ALU_OPRN = `ALU_OPRN_WIDTH'h09; end 
  //I7)  6'h04 : beq : If(R[rs]==R[rt]) PC=PC+1+BranchAddr
  6'h04: begin                             
    ALU_OP1 = RF_DATA_R1;           
    ALU_OP2 = RF_DATA_R2;
    ALU_OPRN = `ALU_OPRN_WIDTH'h02; end
  //I8)  6'h05 : bne : If(R[rs]!=R[rt]) PC=PC+1+BranchAddr
  6'h05: begin                             
    ALU_OP1 = RF_DATA_R1;           
    ALU_OP2 = RF_DATA_R2;
    ALU_OPRN = `ALU_OPRN_WIDTH'h02; end
  //I9)  6'h23 : lw  : R[rt]=M[R[rs]+SignExtImm] 
  6'h23: begin 
    ALU_OP1 = RF_DATA_R1; 
    ALU_OP2 = SignExtImm;
    ALU_OPRN = `ALU_OPRN_WIDTH'h01; end 
  //I10) 6'h2b : sw  : M[R[rs]+SignExtImm]=R[rt]
  6'h2b: begin 
    ALU_OP1 = RF_DATA_R1; 
    ALU_OP2 = SignExtImm;
    ALU_OPRN = `ALU_OPRN_WIDTH'h01; end 
