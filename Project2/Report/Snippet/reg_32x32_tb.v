// Register File 32x32 instance
REGISTER_FILE_32x32 reg_inst (.DATA_R1(DATA_R1), .DATA_R2(DATA_R2), 
  .ADDR_R1(ADDR_R1), .ADDR_R2(ADDR_R2), .DATA_W(DATA_W), .ADDR_W(ADDR_W), 
  .READ(READ), .WRITE(WRITE), .CLK(CLK), .RST(RST));
...
// Write cycle
for(i=1;i<10;i=i+1) begin
#10     DATA_W=i; READ=1'b0; WRITE=1'b1; ADDR_W = i; end

// Read Cycle (DATA_R1)
#10   READ=1'b0; WRITE=1'b0;
#5    no_of_test_R = no_of_test_R + 1;
     if (DATA_R1 !== {`DATA_WIDTH{1'bz}})
       $write("[TEST_R_DATA_R1] Read %1b, Write %1b, expecting 32'hzzzzzzzz, got %8h [FAILED]\n", READ, WRITE, DATA_R1);
     else 
	no_of_pass_R  = no_of_pass_R + 1;

// test of write data (DATA_R1)
for(i=1;i<10;i=i+1) begin
#5      READ=1'b1; WRITE=1'b0; ADDR_R1 = i;
#5      no_of_test_W = no_of_test_W + 1;
        if (DATA_R1 !== i)
	    $write("[TEST_W_DATA_R1] Read %1b, Write %1b, expecting %8h, got %8h [FAILED]\n", READ, WRITE, i, DATA_R1);
        else 
	    no_of_pass_W  = no_of_pass_W + 1; end

// test of write data (DATA_R2)
for(i=1;i<10;i=i+1) begin
#5      READ=1'b1; WRITE=1'b0; ADDR_R2 = i;
#5      no_of_test_W = no_of_test_W + 1;
        if (DATA_R2 !== i)
	    $write("[TEST_W_DATA_R2] Read %1b, Write %1b, expecting %8h, got %8h [FAILED]\n", READ, WRITE, i, DATA_R2);
        else 
	    no_of_pass_W  = no_of_pass_W + 1; end
....
