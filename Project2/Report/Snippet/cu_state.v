module PROC_SM(STATE,CLK,RST);
// list of inputs
input CLK, RST;
// list of outputs
output [2:0] STATE;
// 2. State machine implementation 
// (refer to lab08 source code for writing state machine)
// 2a. Modify module 'module PROC_SM(STATE,CLK,RST);'
// 2b. Define state and next_state register.
reg [2:0] STATE;
reg [2:0] next_state;
// 2c. State machine is reset on negative edge of RST. 
//     At reset and 'initial' set the next state as `PROC_FETCH 
//     (defined in proj_definition.v) and state to 3 bit unknown (3'bxxx).
// initiation of state
initial
begin
  STATE = 3'bxxx;
  next_state = `PROC_FETCH;
end
// reset signal handling
always @ (negedge RST)
begin
  STATE = 3'bxxx;
  next_state = `PROC_FETCH;
end
// 2d. Upon each positive edge of clock, assign state with next_state value. 
//     Also determine next_state depending on current state value.
//    `PROC_FETCH -> `PROC_DECODE;
//    `PROC_DECODE -> `PROC_EXE;
//    `PROC_EXE -> `PROC_MEM;
//    `PROC_MEM -> `PROC_WB;
//    `PROC_WB -> `PROC_FETCH;
// state switching
always @(posedge CLK)
begin
  case(STATE)
    `PROC_FETCH: next_state = `PROC_DECODE; 
    `PROC_DECODE: next_state = `PROC_EXE;
    `PROC_EXE: next_state = `PROC_MEM;
    `PROC_MEM: next_state = `PROC_WB;
    `PROC_WB: next_state = `PROC_FETCH;
  endcase // STATE
  STATE = next_state;
end 
endmodule // PROC_SM

// definition for processor state in prj_definition.v
//`define PROC_FETCH    3'h0
//`define PROC_DECODE   3'h1
//`define PROC_EXE      3'h2
//`define PROC_MEM      3'h3
//`define PROC_WB       3'h4

