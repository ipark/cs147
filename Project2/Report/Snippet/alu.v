  ...
module ALU(OUT, ZERO, OP1, OP2, OPRN);
  ...
output ZERO;
reg ZERO;
// register list
always @(OP1 or OP2 or OPRN)
begin
  case (OPRN)
  `ALU_OPRN_WIDTH'h01 : OUT = OP1 + OP2; // addition        
  `ALU_OPRN_WIDTH'h02 : OUT = OP1 - OP2; // subtraction
  ...
  `ALU_OPRN_WIDTH'h09 : OUT = (OP1 < OP2); // set less than
  default: OUT = `DATA_WIDTH'hxxxxxxxx;
endcase
// 3. Extend the functionality to set the 'ZERO' output, which is 
// set to 1 if result of the current ALU operation is zero.  
// It is set to 1 otherwise.
ZERO = (OUT === 1'b0) ? 1'b1 : 1'b0;
end
endmodule
