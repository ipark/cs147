`PROC_EXE : begin
case(opcode) 
  // R-Type //////////////////////////////////////
  6'h00: begin // opcode===6'h00
  case(funct)
  //R1)  6'h20: add : R[rd]=R[rs]+R[rt]        
  6'h20: begin
    ALU_OP1 = RF_DATA_R1; 
    ALU_OP2 = RF_DATA_R2; 
    ALU_OPRN = `ALU_OPRN_WIDTH'h01; end
  //R2)  6'h22: sub : R[rd]=R[rs]-R[rt]
  6'h22: begin
    ALU_OP1 = RF_DATA_R1; 
    ALU_OP2 = RF_DATA_R2; 
    ALU_OPRN = `ALU_OPRN_WIDTH'h02; end
  //R3)  6'h2c: mul : R[rd]=R[rs]*R[rt]
  6'h2c: begin
    ALU_OP1 = RF_DATA_R1; 
    ALU_OP2 = RF_DATA_R2; 
    ALU_OPRN = `ALU_OPRN_WIDTH'h03; end
  //R4)  6'h24: and : R[rd]=R[rs]&R[rt]
  6'h24: begin
    ALU_OP1 = RF_DATA_R1; 
    ALU_OP2 = RF_DATA_R2; 
    ALU_OPRN = `ALU_OPRN_WIDTH'h06; end
  //R5)  6'h25: or  : R[rd]=R[rs]|R[rt]
  6'h25: begin
    ALU_OP1 = RF_DATA_R1; 
    ALU_OP2 = RF_DATA_R2; 
    ALU_OPRN = `ALU_OPRN_WIDTH'h07; end
  //R6)  6'h27: nor : R[rd]=~(R[rs]|R[rt])
  6'h27: begin
    ALU_OP1 = RF_DATA_R1; 
    ALU_OP2 = RF_DATA_R2; 
    ALU_OPRN = `ALU_OPRN_WIDTH'h08; end
  //R7)  6'h2a: slt : R[rd]=(R[rs]<R[rt])?0:1
  6'h2a: begin
    ALU_OP1 = RF_DATA_R1; 
    ALU_OP2 = RF_DATA_R2; 
    ALU_OPRN = `ALU_OPRN_WIDTH'h09; end
  //R8)  6'h01: sll : R[rd]=R[rs]<<shamt
  6'h01: begin 
    ALU_OP1 = RF_DATA_R1; 
    ALU_OP2 = shamt; 
    ALU_OPRN = `ALU_OPRN_WIDTH'h05; end
  //R9)  6'h02: srl : R[rd]=R[rs]>>shamt
  6'h02: begin 
    ALU_OP1 = RF_DATA_R1; 
    ALU_OP2 = shamt; 
    ALU_OPRN = `ALU_OPRN_WIDTH'h04; end
  //R10) 6'h08: jr  : PC=R[rs]
  //6'h08: PC_REG = RF_DATA_R1; 
  endcase // case(funct)
  end // opcode===6'h00
