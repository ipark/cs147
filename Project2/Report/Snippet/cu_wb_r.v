`PROC_WB: 
/////////////////
begin // else if(proc_state === `PROC_WB)
  PC_REG = PC_REG + 1;
  MEM_READ = 1'b0; MEM_WRITE = 1'b0;  // no-op {00} 
  RF_READ = 1'b0; RF_WRITE = 1'b1; // RF-write
  case(opcode) 
  // R-Type //////////////////////////////////////
  6'h00: 
  begin // opcode===6'h00
    case(funct)
      //R10) 6'h08: jr  : PC=R[rs]
      6'h08:
        PC_REG = RF_DATA_R1;
      //R1)  6'h20: add : R[rd]=R[rs]+R[rt]        
      //R2)  6'h22: sub : R[rd]=R[rs]-R[rt]
      //R3)  6'h2c: mul : R[rd]=R[rs]*R[rt]
      //R4)  6'h24: and : R[rd]=R[rs]&R[rt]
      //R5)  6'h25: or  : R[rd]=R[rs]|R[rt]
      //R6)  6'h27: nor : R[rd]=~(R[rs]|R[rt])
      //R7)  6'h2a: slt : R[rd]=(R[rs]<R[rt])?0:1
      //R8)  6'h01: sll : R[rd]=R[rs]<<shamt
      //R9)  6'h02: srl : R[rd]=R[rs]>>shamt
      default:
        begin
        RF_ADDR_W = rd;
        RF_DATA_W = ALU_RESULT;
        end
    endcase // funct
  end // opcode===6'h00
