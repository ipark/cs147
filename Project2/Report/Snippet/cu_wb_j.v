`PROC_WB: 
/////////////////
begin // else if(proc_state === `PROC_WB)
  PC_REG = PC_REG + 1;
  MEM_READ = 1'b0; MEM_WRITE = 1'b0;  // no-op {00} 
  RF_READ = 1'b0; RF_WRITE = 1'b1; // RF-write
  case(opcode) 
  // J-Type //////////////////////////////////////
  //J1)  6'h02: jmp : PC=JumpAddr={6'b0, address}
  6'h02:
    PC_REG = JumpAddr;
  //J2)  6'h03: jal : R[31]=PC+1; PC=JumpAddr
  6'h03: begin
    RF_ADDR_W = 31;
    RF_DATA_W = PC_REG + 1; 
    PC_REG = JumpAddr; end
  //J3)  6'h1b: push: M[$sp]=R[0]; $sp-=1
  //J4)  6'h1c: pop : $sp+=1; R[0]=M[$sp]
  6'h1c: begin
    RF_ADDR_W = 0;
    RF_DATA_W = MEM_DATA; end
endcase // opcode                                   
end // proc_state_begin
endcase // (proc_state)
end // always @ (proc_state)
