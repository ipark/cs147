// Name: control_unit.v
// Module: CONTROL_UNIT
// Output: RF_DATA_W  : Data to be written at register file address RF_ADDR_W
//         RF_ADDR_W  : Register file address of the memory location to be written
//         RF_ADDR_R1 : Register file address of the memory location to be read for RF_DATA_R1
//         RF_ADDR_R2 : Registere file address of the memory location to be read for RF_DATA_R2
//         RF_READ    : Register file Read signal
//         RF_WRITE   : Register file Write signal
//         ALU_OP1    : ALU operand 1
//         ALU_OP2    : ALU operand 2
//         ALU_OPRN   : ALU operation code
//         MEM_ADDR   : Memory address to be read in
//         MEM_READ   : Memory read signal
//         MEM_WRITE  : Memory write signal
//         
// Input:  RF_DATA_R1 : Data at ADDR_R1 address
//         RF_DATA_R2 : Data at ADDR_R1 address
//         ALU_RESULT : ALU output data
//         CLK        : Clock signal
//         RST        : Reset signal
//
// INOUT: MEM_DATA    : Data to be read in from or write to the memory
//
// Notes: - Control unit synchronize operations of a processor
//
// Revision History:
//
// Version	Date		Who		email			note
//------------------------------------------------------------------------------------------
//  1.0     Sep 10, 2014	Kaushik Patra	kpatra@sjsu.edu		Initial creation
//  1.1     Oct 19, 2014        Kaushik Patra   kpatra@sjsu.edu         Added ZERO status output
//------------------------------------------------------------------------------------------

`include "prj_definition.v"
module CONTROL_UNIT(MEM_DATA, RF_DATA_W, RF_ADDR_W, RF_ADDR_R1, RF_ADDR_R2, 
  RF_READ, RF_WRITE, ALU_OP1, ALU_OP2, ALU_OPRN, MEM_ADDR, MEM_READ,
  MEM_WRITE, RF_DATA_R1, RF_DATA_R2, ALU_RESULT, ZERO, CLK, RST); 

// Output signals
// Outputs for register file 
output [`DATA_INDEX_LIMIT:0] RF_DATA_W;
output [`ADDRESS_INDEX_LIMIT:0] RF_ADDR_W, RF_ADDR_R1, RF_ADDR_R2;
output RF_READ, RF_WRITE;
// Outputs for ALU
output [`DATA_INDEX_LIMIT:0]  ALU_OP1, ALU_OP2;
output  [`ALU_OPRN_INDEX_LIMIT:0] ALU_OPRN;
// Outputs for memory
output [`ADDRESS_INDEX_LIMIT:0]  MEM_ADDR;
output MEM_READ, MEM_WRITE;

// Input signals
input [`DATA_INDEX_LIMIT:0] RF_DATA_R1, RF_DATA_R2, ALU_RESULT;
input ZERO, CLK, RST;

// Inout signal
inout [`DATA_INDEX_LIMIT:0] MEM_DATA;

// State nets
wire [2:0] proc_state;

PROC_SM state_machine(.STATE(proc_state),.CLK(CLK),.RST(RST));
//////////////////////////////////////////////
// 3. Control signal generation.
// 3a. Add registers for corresponding output ports.
// RF
reg [`DATA_INDEX_LIMIT:0] RF_DATA_W;
reg [`REG_INDEX_LIMIT:0] RF_ADDR_W, RF_ADDR_R1, RF_ADDR_R2;
reg RF_READ, RF_WRITE;
// ALU
reg [`DATA_INDEX_LIMIT:0]  ALU_OP1, ALU_OP2;
reg [`ALU_OPRN_INDEX_LIMIT:0] ALU_OPRN;
// MEM
reg [`DATA_INDEX_LIMIT:0]  MEM_ADDR;
reg MEM_READ, MEM_WRITE;

// 3b. Define a register for write data to memory and 
// connect DATA to this register similar to the DATA port in memory model 
// (but in opposite sense - in control unit, 
// for memory read operation DATA must be set to HighZ and 
// for write operation DATA must be set to internal write data register)
reg [`DATA_INDEX_LIMIT:0] mem_data_ret;
assign MEM_DATA=((MEM_READ===1'b0)&&(MEM_WRITE===1'b1))?mem_data_ret:{`DATA_WIDTH{1'bz}};


// 3c. Add internal registers as following
//    PC_REG : Holds program counter value
//    INST_REG : Stores the current instruction
//    SP_REF : Stack pointer register
reg [`DATA_INDEX_LIMIT:0] PC_REG, INST_REG, SP_REG;
// 
// I-type SignExtImm, ZeroExtImm, Lui, BranchAddr
reg [`DATA_INDEX_LIMIT:0] SignExtImm; // 32-bit vector
reg [`DATA_INDEX_LIMIT:0] ZeroExtImm; // 32-bit vector
reg [`DATA_INDEX_LIMIT:0] Lui; // 32-bit vector
reg [`DATA_INDEX_LIMIT:0] BranchAddr; // 32-bit vector
// J-Type JumpAddr (ZeroExt for 6-bit)
reg [`DATA_INDEX_LIMIT:0] JumpAddr; // 32-bit vector
// 32-bit instruction parsing 
reg [5:0] opcode; // 6-bit 
reg [4:0] rs;     // 5-bit
reg [4:0] rt;     // 5-bit
reg [4:0] rd;     // 5-bit
reg [4:0] shamt;  // 5-bit
reg [5:0] funct;  // 6-bit
reg [15:0] immediate; // 16-bit
reg [25:0] address;   // 26-bit


// initial PC power on address and SP
initial
begin
  PC_REG=`INST_START_ADDR; // 32'h00001000
  SP_REG=`INIT_STACK_POINTER; // 32'h03ffffff
end


// 3d. Always at the change of processor state set the control signal 
// and address / data signal values properly for memory, register file and ALU.
always @ (proc_state)
begin // proc_state
case(proc_state)

`PROC_FETCH:
/////////////////
// Set memory address to program counter, memory control for read operation. 
// Also set the register file control to hold ({r,w} to 00 or 11) operation.
begin 
  MEM_ADDR = PC_REG;
  MEM_READ = 1'b1; MEM_WRITE = 1'b0; // memory control for read oprn
  RF_READ = 1'b0; RF_WRITE = 1'b0; // {r,w}=00
end 


`PROC_DECODE : 
/////////////////
begin 
  // Store the memory read data into INST_REG; 
  // You may use the following task code to print the current instruction 
  // fetched (usage: print_instruction(INST_REG); ). 
  INST_REG = MEM_DATA;
  print_instruction(INST_REG);
  // It also shows how the instruction is parsed into required fields 
  // (opcode, rs, rt, rd, shamt, funct, immediate, address for 
  // different type of instructions). 
  {opcode, rs, rt, rd, shamt, funct} = INST_REG; // R-type
  {opcode, rs, rt, immediate} = INST_REG; // I-type
  {opcode, address} = INST_REG; // J-type
  // I-Type
  // You may want to calculate and store sign extended value of immediate, 
  // zero extended value of immediate, LUI value for I-type instruction. 
  // SignExtImm
  //6'h08 : addi: R[rt]=R[rs]+SignExtImm
  //6'h1d : muli: R[rt]=R[rs]*SignExtImm
  //6'h0a : slti: R[rt]=(R[rs]<SignExtImm)?1:0
  //6'h23 : lw  : R[rt]=M[R[rs]+SignExtImm] 
  //6'h2b : sw  : M[R[rs]+SignExtImm]=R[rt]
  SignExtImm = {{16{immediate[15]}}, immediate};
  // ZeroExtImm
  //6'h0c : andi: R[rt]=R[rs]&ZeroExtImm
  //6'h0d : ori : R[rt]=R[rs]|ZeroExtImm
  ZeroExtImm = {{16'b0}, immediate};
  // Lui
  //6'h0f : lui : R[rt]={immediate,16'b0}
  //Lui = {immediate, {16'b0}};
  Lui = {immediate, 16'h0000};
  //BranchAddr
  //6'h04 : beq : If(R[rs]==R[rt]) PC=PC+1+BranchAddr
  //6'h05 : bne : If(R[rs]!=R[rt]) PC=PC+1+BranchAddr
  BranchAddr = {{16{immediate[15]}}, immediate};
  // J-type
  // For J-type instruction it would be good to store the 32-bit 
  // jump address from the address field. 
  // JumpAddr
  //6'h02 : jmp : PC=JumpAddr={6'b0, address}
  //6'h03 : jal : R[31]=PC+1; PC=JumpAddr
  JumpAddr = {{6'b0}, address};
  // Also set the read address of RF as rs and 
  // rt field value with RF operation set to reading.
  RF_ADDR_R1 = rs; 
  RF_ADDR_R2 = rt; 
  RF_READ = 1'b1; RF_WRITE = 1'b0; // {r,w}=10
end 


`PROC_EXE : 
/////////////////
//In this stage, set the ALU operands and operation code accordingly 
//the opcode/funct of the instruction. 
//Some operation may not need ALU operation (like lui, jmp or jal). 
//In those cases, just do not put anything on the operands or operation. 
//For 'push' operation, the RF ADDR_R1 needs to be set to 0 
//(because push stores the data at r[0] into the stack).
begin
case(opcode) 
  // R-Type //////////////////////////////////////
  6'h00: 
  begin // opcode===6'h00

    case(funct)
    //R1)  6'h20: add : R[rd]=R[rs]+R[rt]        
    //`ALU_OPRN_WIDTH'h01 : OUT = OP1 + OP2; // addition        
    6'h20: 
      begin
      ALU_OP1 = RF_DATA_R1; 
      ALU_OP2 = RF_DATA_R2; 
      ALU_OPRN = `ALU_OPRN_WIDTH'h01;
      //$display("ADD: op1=0X%04h, op2=0X%04h", ALU_OP1, ALU_OP2);
      end

    //R2)  6'h22: sub : R[rd]=R[rs]-R[rt]
    //`ALU_OPRN_WIDTH'h02 : OUT = OP1 - OP2; // subtraction
    6'h22: 
      begin
      ALU_OP1 = RF_DATA_R1; 
      ALU_OP2 = RF_DATA_R2; 
      ALU_OPRN = `ALU_OPRN_WIDTH'h02;
      end

    //R3)  6'h2c: mul : R[rd]=R[rs]*R[rt]
    //`ALU_OPRN_WIDTH'h03 : OUT = OP1 * OP2; // multiplication
    6'h2c: 
      begin
      ALU_OP1 = RF_DATA_R1; 
      ALU_OP2 = RF_DATA_R2; 
      ALU_OPRN = `ALU_OPRN_WIDTH'h03;
      end

    //R4)  6'h24: and : R[rd]=R[rs]&R[rt]
    //`ALU_OPRN_WIDTH'h06 : OUT = OP1 & OP2; // bitwise and
    6'h24: 
      begin
      ALU_OP1 = RF_DATA_R1; 
      ALU_OP2 = RF_DATA_R2; 
      ALU_OPRN = `ALU_OPRN_WIDTH'h06;
      end

    //R5)  6'h25: or  : R[rd]=R[rs]|R[rt]
    //`ALU_OPRN_WIDTH'h07 : OUT = OP1 | OP2; // bitwise or
    6'h25: 
      begin
      ALU_OP1 = RF_DATA_R1; 
      ALU_OP2 = RF_DATA_R2; 
      ALU_OPRN = `ALU_OPRN_WIDTH'h07;
      end

    //R6)  6'h27: nor : R[rd]=~(R[rs]|R[rt])
    //`ALU_OPRN_WIDTH'h08 : OUT = ~(OP1 | OP2); // bitwise nor
    6'h27:
      begin
      ALU_OP1 = RF_DATA_R1; 
      ALU_OP2 = RF_DATA_R2; 
      ALU_OPRN = `ALU_OPRN_WIDTH'h08;
      end

    //R7)  6'h2a: slt : R[rd]=(R[rs]<R[rt])?0:1
    //`ALU_OPRN_WIDTH'h09 : OUT = (OP1 < OP2); // set less than
    6'h2a: 
      begin
      ALU_OP1 = RF_DATA_R1; 
      ALU_OP2 = RF_DATA_R2; 
      ALU_OPRN = `ALU_OPRN_WIDTH'h09;
      end

    //R8)  6'h01: sll : R[rd]=R[rs]<<shamt
    //`ALU_OPRN_WIDTH'h05 : OUT = OP1 << OP2; // shift_left
    6'h01: 
      begin 
      ALU_OP1 = RF_DATA_R1; 
      ALU_OP2 = shamt; 
      ALU_OPRN = `ALU_OPRN_WIDTH'h05;
      end

    //R9)  6'h02: srl : R[rd]=R[rs]>>shamt
    //`ALU_OPRN_WIDTH'h04 : OUT = OP1 >> OP2; // shift_right
    6'h02: 
      begin 
      ALU_OP1 = RF_DATA_R1; 
      ALU_OP2 = shamt; 
      ALU_OPRN = `ALU_OPRN_WIDTH'h04;
      end

    //R10) 6'h08: jr  : PC=R[rs]
    //6'h08: PC_REG = RF_DATA_R1; 
    endcase // case(funct)
  end // opcode===6'h00
  // I-Type //////////////////////////////////////
  //I1)  6'h08 : addi: R[rt]=R[rs]+SignExtImm              
  6'h08: 
    begin 
    ALU_OP1 = RF_DATA_R1;           
    ALU_OP2 = SignExtImm;
    //`ALU_OPRN_WIDTH'h01 : OUT = OP1 + OP2; // addition        
    ALU_OPRN = `ALU_OPRN_WIDTH'h01;
    end 
  //I2)  6'h1d : muli: R[rt]=R[rs]*SignExtImm
  6'h1d: 
    begin 
    ALU_OP1 = RF_DATA_R1;           
    ALU_OP2 = SignExtImm;
    //`ALU_OPRN_WIDTH'h03 : OUT = OP1 * OP2; // multiplication
    ALU_OPRN = `ALU_OPRN_WIDTH'h03;
    end 
  //I3)  6'h0c : andi: R[rt]=R[rs]&ZeroExtImm
  6'h0c: 
    begin 
    ALU_OP1 = RF_DATA_R1;           
    ALU_OP2 = ZeroExtImm;
    //`ALU_OPRN_WIDTH'h06 : OUT = OP1 & OP2; // bitwise and
    ALU_OPRN = `ALU_OPRN_WIDTH'h06;
    end 
  //I4)  6'h0d : ori : R[rt]=R[rs]|ZeroExtImm
  6'h0d: 
    begin 
    ALU_OP1 = RF_DATA_R1;           
    ALU_OP2 = ZeroExtImm;
    //`ALU_OPRN_WIDTH'h07 : OUT = OP1 | OP2; // bitwise or
    ALU_OPRN = `ALU_OPRN_WIDTH'h07;
    end 

  //I5)  6'h0f : lui : R[rt]={immediate,16'b0}
    
  //I6)  6'h0a : slti: R[rt]=(R[rs]<SignExtImm)?1:0
  6'h0a:
    begin 
    ALU_OP1 = RF_DATA_R1;           
    ALU_OP2 = SignExtImm;
    //`ALU_OPRN_WIDTH'h09 : OUT = (OP1 < OP2); // set less than
    ALU_OPRN = `ALU_OPRN_WIDTH'h09;
    end 
  //I7)  6'h04 : beq : If(R[rs]==R[rt]) PC=PC+1+BranchAddr
  6'h04:
    begin                             
    ALU_OP1 = RF_DATA_R1;           
    ALU_OP2 = RF_DATA_R2;
    //`ALU_OPRN_WIDTH'h02 : OUT = OP1 - OP2; // subtraction
    ALU_OPRN = `ALU_OPRN_WIDTH'h02;
    end
  //I8)  6'h05 : bne : If(R[rs]!=R[rt]) PC=PC+1+BranchAddr
  6'h05:
    begin                             
    ALU_OP1 = RF_DATA_R1;           
    ALU_OP2 = RF_DATA_R2;
    //`ALU_OPRN_WIDTH'h02 : OUT = OP1 - OP2; // subtraction
    ALU_OPRN = `ALU_OPRN_WIDTH'h02;
    end
  //I9)  6'h23 : lw  : R[rt]=M[R[rs]+SignExtImm] 
  6'h23:
    begin 
    ALU_OP1 = RF_DATA_R1; 
    ALU_OP2 = SignExtImm;
    //`ALU_OPRN_WIDTH'h01 : OUT = OP1 + OP2; // addition        
    ALU_OPRN = `ALU_OPRN_WIDTH'h01;
    end 
  //I10) 6'h2b : sw  : M[R[rs]+SignExtImm]=R[rt]
  6'h2b:
    begin 
    ALU_OP1 = RF_DATA_R1; 
    ALU_OP2 = SignExtImm;
    //`ALU_OPRN_WIDTH'h01 : OUT = OP1 + OP2; // addition        
    ALU_OPRN = `ALU_OPRN_WIDTH'h01;
    //$display("SW: op1=0X%04h, op2=0X%04h", ALU_OP1, ALU_OP2);
    end 
  // J-Type //////////////////////////////////////
  //J1)  6'h02: jmp : PC=JumpAddr={6'b0, address}
    
  //J2)  6'h03: jal : R[31]=PC+1; PC=JumpAddr
    
  //J3)  6'h1b: push: M[$sp]=R[0]; $sp-=1
  6'h1b:
    begin
    ALU_OP1 = SP_REG;
    ALU_OP2 = 1'b1;
    //`ALU_OPRN_WIDTH'h02 : OUT = OP1 - OP2; // subtraction
    ALU_OPRN = `ALU_OPRN_WIDTH'h02;
    RF_ADDR_R1 = 0;
    end
  //J4)  6'h1c: pop : $sp+=1; R[0]=M[$sp]
  6'h1c:
    begin
    ALU_OP1 = SP_REG;
    ALU_OP2 = 1'b1;
    //`ALU_OPRN_WIDTH'h01 : OUT = OP1 + OP2; // addition        
    ALU_OPRN = `ALU_OPRN_WIDTH'h01;
    RF_ADDR_R1 = 0;
    end
endcase //(opcode) 
end


`PROC_MEM: 
/////////////////
//Only 'lw', 'sw', 'push' and 'pop' instructions are involved in this stage. 
//By default, make the memory operation to 00 or 11 (which will set the memory 
//output to HighZ). For the four memory related operation, set the memory read 
//or write mode accordingly (e.g. for read you need to set MEM_READ to 1'b1 
//and MEM_WRITE to 1'b0). The Address for the stack operation needs to be set 
//carefully following the ISA specification.
//
begin 
  MEM_READ = 1'b0; MEM_WRITE = 1'b0;  // memory output to HighZ
  case(opcode)
  //I9)  6'h23 : lw  : R[rt]=M[R[rs]+SignExtImm] 
  6'h23:
    begin 
    MEM_READ = 1'b1; MEM_WRITE = 1'b0;  // read
    MEM_ADDR = ALU_RESULT;
    RF_ADDR_W = RF_DATA_R2; // rt
    RF_DATA_W = MEM_DATA;
    //$display("SW: RF_ADDR=0X%08h, RF_DATA=0X%08h", RF_ADDR_W, RF_DATA_W);
    end
  //I10) 6'h2b : sw  : M[R[rs]+SignExtImm]=R[rt]
  6'h2b:
    begin 
    MEM_READ = 1'b0; MEM_WRITE = 1'b1;  // write
    MEM_ADDR = ALU_RESULT;
    mem_data_ret = RF_DATA_R2; // rt
    end
  //J3)  6'h1b: push: M[$sp]=R[0]; $sp-=1
  6'h1b:
    begin 
    MEM_READ = 1'b0; MEM_WRITE = 1'b1;  // write
    MEM_ADDR = SP_REG; // $sp
    mem_data_ret = RF_DATA_R1; // R[0]
    SP_REG = ALU_RESULT; //sp-=1
    end
  //J4)  6'h1c: pop : $sp+=1; R[0]=M[$sp]
  6'h1c:
    begin 
    MEM_READ = 1'b1; MEM_WRITE = 1'b0;  // read
    SP_REG = ALU_RESULT; // sp+=1
    MEM_ADDR = SP_REG; // $sp
    RF_DATA_W = MEM_DATA;
    end
  endcase // opcode
end


`PROC_WB: 
/////////////////
//Write back to RF or PC _REG is done here.
//Increase PC_REG by 1 by default.
//Reset the memory write signal to no-op (00 or 11)
//Set RF writing address, data and control to write back into register file. 
//For most of the instruction this is needed.
//The instructions beq, bne, jmp and jal needs to modify the PC_REG
//accordingly the instruction specification.
begin // else if(proc_state === `PROC_WB)
  PC_REG = PC_REG + 1;
  MEM_READ = 1'b0; MEM_WRITE = 1'b0;  // no-op {00} 
  RF_READ = 1'b0; RF_WRITE = 1'b1; // RF-write
  case(opcode) 
  // R-Type //////////////////////////////////////
  6'h00: 
  begin // opcode===6'h00
    case(funct)
      //R10) 6'h08: jr  : PC=R[rs]
      6'h08:
        PC_REG = RF_DATA_R1;
      //R1)  6'h20: add : R[rd]=R[rs]+R[rt]        
      //R2)  6'h22: sub : R[rd]=R[rs]-R[rt]
      //R3)  6'h2c: mul : R[rd]=R[rs]*R[rt]
      //R4)  6'h24: and : R[rd]=R[rs]&R[rt]
      //R5)  6'h25: or  : R[rd]=R[rs]|R[rt]
      //R6)  6'h27: nor : R[rd]=~(R[rs]|R[rt])
      //R7)  6'h2a: slt : R[rd]=(R[rs]<R[rt])?0:1
      //R8)  6'h01: sll : R[rd]=R[rs]<<shamt
      //R9)  6'h02: srl : R[rd]=R[rs]>>shamt
      default:
        begin
        RF_ADDR_W = rd;
        RF_DATA_W = ALU_RESULT;
        end
    endcase // funct
  end // opcode===6'h00
  // I-Type //////////////////////////////////////
  //I1)  6'h08 : addi: R[rt]=R[rs]+SignExtImm              
  6'h08: 
    begin                   
    RF_ADDR_W = rt;
    RF_DATA_W = ALU_RESULT;
    end
  //I2)  6'h1d : muli: R[rt]=R[rs]*SignExtImm
  6'h1d:
    begin                   
    RF_ADDR_W = rt;
    RF_DATA_W = ALU_RESULT;
    end
  //I3)  6'h0c : andi: R[rt]=R[rs]&ZeroExtImm
  6'h0c:
    begin                   
    RF_ADDR_W = rt;
    RF_DATA_W = ALU_RESULT;
    end
  //I4)  6'h0d : ori : R[rt]=R[rs]|ZeroExtImm
  6'h0d:
    begin                   
    RF_ADDR_W = rt;
    RF_DATA_W = ALU_RESULT;
    end
  //I6)  6'h0a : slti: R[rt]=(R[rs]<SignExtImm)?1:0
  6'h0a:
    begin                   
    RF_ADDR_W = rt;
    RF_DATA_W = ALU_RESULT;
    end
  //I7)  6'h04 : beq : If(R[rs]==R[rt]) PC=PC+1+BranchAddr
  6'h04:
    if (ZERO===1'b1) 
      PC_REG = BranchAddr; // PC=PC+1 already above
  //I8)  6'h05 : bne : If(R[rs]!=R[rt]) PC=PC+1+BranchAddr
  6'h05:
    if (ZERO===1'b0) 
      PC_REG = BranchAddr; // PC=PC+1 already above
  //I5)  6'h0f : lui : R[rt]={immediate,16'b0}
  6'h0f:
    begin                   
    RF_ADDR_W = rt;
    RF_DATA_W = Lui;
    end
  //I9)  6'h23 : lw  : R[rt]=M[R[rs]+SignExtImm] 
  6'h23:
    begin                   
    RF_ADDR_W = rt;
    RF_DATA_W = MEM_DATA;
    end
    
  //I10) 6'h2b : sw  : M[R[rs]+SignExtImm]=R[rt]
    
  // J-Type //////////////////////////////////////
  //J1)  6'h02: jmp : PC=JumpAddr={6'b0, address}
  6'h02:
    PC_REG = JumpAddr;
  //J2)  6'h03: jal : R[31]=PC+1; PC=JumpAddr
  6'h03: 
    begin
    RF_ADDR_W = 31;
    RF_DATA_W = PC_REG + 1; 
    PC_REG = JumpAddr;
    end
  
  //J3)  6'h1b: push: M[$sp]=R[0]; $sp-=1

  //J4)  6'h1c: pop : $sp+=1; R[0]=M[$sp]
  6'h1c:
    begin
    RF_ADDR_W = 0;
    RF_DATA_W = MEM_DATA;
    end
endcase // opcode                                   
end // proc_state_begin
endcase // (proc_state)
end // always @ (proc_state)

//////////////////////////////////////////////
task print_instruction;
input [`DATA_INDEX_LIMIT:0] inst;
//reg [5:0] opcode; reg [4:0] rs; reg [4:0] rt;
//reg [4:0] rd; reg [4:0] shamt; reg [5:0] funct; 
//reg [15:0] immediate; reg [25:0] address;
begin // print_instruction
// parse the instruction
{opcode, rs, rt, rd, shamt, funct} = inst; // R-type
{opcode, rs, rt, immediate } = inst; // I-type
{opcode, address} = inst; // J-type
$display("@ %6dns -> [0X%08h] ", $time, inst);
case(opcode) 
  // R-Type 
  6'h00 :                                                             
  begin
    case(funct)                                                     
    6'h20: $display("add  r[%02d], r[%02d], r[%02d];", rd, rs, rt);
    6'h22: $display("sub  r[%02d], r[%02d], r[%02d];", rd, rs, rt);
    6'h2c: $display("mul  r[%02d], r[%02d], r[%02d];", rd, rs, rt);
    6'h24: $display("and  r[%02d], r[%02d], r[%02d];", rd, rs, rt);
    6'h25: $display("or   r[%02d], r[%02d], r[%02d];", rd, rs, rt);
    6'h27: $display("nor  r[%02d], r[%02d], r[%02d];", rd, rs, rt);
    6'h2a: $display("slt  r[%02d], r[%02d], r[%02d];", rd, rs, rt);
    6'h01: $display("sll  r[%02d], %2d, r[%02d];", rd, rs, shamt);
    6'h02: $display("srl  r[%02d], 0X%02h, r[%02d];", rd, rs, shamt);
    6'h08: $display("jr   r[%02d];", rs);
    default: $display("");
    endcase
  end 
  // I-Type
  6'h08: $display("addi  r[%02d], r[%02d], 0X%04h;", rt, rs, immediate);
  6'h1d: $display("muli  r[%02d], r[%02d], 0X%04h;", rt, rs, immediate);
  6'h0c: $display("andi  r[%02d], r[%02d], 0X%04h;", rt, rs, immediate);
  6'h0d: $display("ori   r[%02d], r[%02d], 0X%04h;", rt, rs, immediate);
  6'h0f: $display("lui   r[%02d], 0X%04h;", rt, immediate);
  6'h0a: $display("slti  r[%02d], r[%02d], 0X%04h;", rt, rs, immediate);
  6'h04: $display("beq   r[%02d], r[%02d], 0X%04h;", rt, rs, immediate);
  6'h05: $display("bne   r[%02d], r[%02d], 0X%04h;", rt, rs, immediate);
  6'h23: $display("lw    r[%02d], r[%02d], 0X%04h;", rt, rs, immediate);
  6'h2b: $display("sw    r[%02d], r[%02d], 0X%04h;", rt, rs, immediate);
  // J-Type //////////////////////////////////////
  6'h02: $display("jmp   0X%07h;", address);   
  6'h03: $display("jal   0X%07h;", address); 
  6'h1b: $display("push;");
  6'h1c: $display("pop;");
  default: $display(""); 
endcase // opcode
//$display(""); 
end // print_instruction
endtask
//////////////////////////

endmodule // CONTROL_UNIT
//------------------------------------------------------------------------------------------
// Module: CONTROL_UNIT
// Output: STATE      : State of the processor
//         
// Input:  CLK        : Clock signal
//         RST        : Reset signal
//
// INOUT: MEM_DATA    : Data to be read in from or write to the memory
//
// Notes: - Processor continuously cycle witnin fetch, decode, execute, 
//          memory, write back state. State values are in the prj_definition.v
//
// Revision History:
//
// Version	Date		Who		email			note
//------------------------------------------------------------------------------------------
//  1.0     Sep 10, 2014	Kaushik Patra	kpatra@sjsu.edu		Initial creation
//------------------------------------------------------------------------------------------
module PROC_SM(STATE,CLK,RST);
// list of inputs
input CLK, RST;
// list of outputs
output [2:0] STATE;

// 2. State machine implementation 
// (refer to lab08 source code for writing state machine)
// 2a. Modify module 'module PROC_SM(STATE,CLK,RST);'
// 2b. Define state and next_state register.
reg [2:0] STATE;
reg [2:0] next_state;

// 2c. State machine is reset on negative edge of RST. 
//     At reset and 'initial' set the next state as `PROC_FETCH 
//     (defined in proj_definition.v) and state to 3 bit unknown (3'bxxx).
// initiation of state
initial
begin
  STATE = 3'bxxx;
  next_state = `PROC_FETCH;
end
// reset signal handling
always @ (negedge RST)
begin
  STATE = 3'bxxx;
  next_state = `PROC_FETCH;
end
// 2d. Upon each positive edge of clock, assign state with next_state value. 
//     Also determine next_state depending on current state value.
//    `PROC_FETCH -> `PROC_DECODE;
//    `PROC_DECODE -> `PROC_EXE;
//    `PROC_EXE -> `PROC_MEM;
//    `PROC_MEM -> `PROC_WB;
//    `PROC_WB -> `PROC_FETCH;
// state switching
always @(posedge CLK)
begin
  case(STATE)
    `PROC_FETCH: next_state = `PROC_DECODE; 
    `PROC_DECODE: next_state = `PROC_EXE;
    `PROC_EXE: next_state = `PROC_MEM;
    `PROC_MEM: next_state = `PROC_WB;
    `PROC_WB: next_state = `PROC_FETCH;
  endcase // STATE
  STATE = next_state;
end 
endmodule // PROC_SM


// R-Type //////////////////////////////////////
//R1)  6'h20: add : R[rd]=R[rs]+R[rt]       
//R2)  6'h22: sub : R[rd]=R[rs]-R[rt]
//R3)  6'h2c: mul : R[rd]=R[rs]*R[rt]
//R4)  6'h24: and : R[rd]=R[rs]&R[rt]
//R5)  6'h25: or  : R[rd]=R[rs]|R[rt]
//R6)  6'h27: nor : R[rd]=~(R[rs]|R[rt])
//R7)  6'h2a: slt : R[rd]=(R[rs]<R[rt])?0:1
//R8)  6'h01: sll : R[rd]=R[rs]<<shamt
//R9)  6'h02: srl : R[rd]=R[rs]>>shamt
//R10) 6'h08: jr  : PC=R[rs]
// I-Type //////////////////////////////////////
//I1)  6'h08 : addi: R[rt]=R[rs]+SignExtImm              
//I2)  6'h1d : muli: R[rt]=R[rs]*SignExtImm
//I3)  6'h0c : andi: R[rt]=R[rs]&ZeroExtImm
//I4)  6'h0d : ori : R[rt]=R[rs]|ZeroExtImm
//I5)  6'h0f : lui : R[rt]={immediate,16'b0}
//I6)  6'h0a : slti: R[rt]=(R[rs]<SignExtImm)?1:0
//I7)  6'h04 : beq : If(R[rs]==R[rt]) PC=PC+1+BranchAddr
//I8)  6'h05 : bne : If(R[rs]!=R[rt]) PC=PC+1+BranchAddr
//I9)  6'h23 : lw  : R[rt]=M[R[rs]+SignExtImm] 
//I10) 6'h2b : sw  : M[R[rs]+SignExtImm]=R[rt]
// J-Type //////////////////////////////////////
//J1)  6'h02: jmp : PC=JumpAddr={6'b0, address}
//J2)  6'h03: jal : R[31]=PC+1; PC=JumpAddr
//J3)  6'h1b: push: M[$sp]=R[0]; $sp-=1
//J4)  6'h1c: pop : $sp+=1; R[0]=M[$sp]

//`ALU_OPRN_WIDTH'h01 : OUT = OP1 + OP2; // addition        
//`ALU_OPRN_WIDTH'h02 : OUT = OP1 - OP2; // subtraction
//`ALU_OPRN_WIDTH'h03 : OUT = OP1 * OP2; // multiplication
//`ALU_OPRN_WIDTH'h04 : OUT = OP1 >> OP2; // shift_right
//`ALU_OPRN_WIDTH'h05 : OUT = OP1 << OP2; // shift_left
//`ALU_OPRN_WIDTH'h06 : OUT = OP1 & OP2; // bitwise and
//`ALU_OPRN_WIDTH'h07 : OUT = OP1 | OP2; // bitwise or
//`ALU_OPRN_WIDTH'h08 : OUT = ~(OP1 | OP2); // bitwise nor
//`ALU_OPRN_WIDTH'h09 : OUT = (OP1 < OP2); // set less than
//////////////////////////////////////////////

