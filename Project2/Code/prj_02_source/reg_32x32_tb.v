// Name: reg_32x32_tb
// Module: REG_32x32_TB
//
// Monitors: 
//   ADDR_W : Address of the memory location to be written
//   ADDR_R1 : Address of the memory location to be read for DATA_R1
//   ADDR_R2 : Address of the memory location to be read for DATA_R2
//   READ    : Read signal
//   WRITE   : Write signal
//   DATA_R1 : Data at ADDR_R1 address
//   DATA_R2 : Data at ADDR_R1 address
//
// Input:   DATA_W : Data read out in the read operation
//          CLK  : Clock signal
//          RST  : Reset signal
//
// Notes: - Testbench for REG_32x32 memory system
//-------------------------------------------------------------------------
`include "prj_definition.v"
module REG_32x32_TB;
// Write a testbench similar to memory 
// (do not need to load the RF externally though like the memory testing). 
// Just write some data in all the register location, and read them back.

// Storage list
//reg [`ADDRESS_INDEX_LIMIT:0] ADDR;
reg [`REG_ADDR_INDEX_LIMIT:0] ADDR_R1, ADDR_R2, ADDR_W;

// reset
reg READ, WRITE, RST;

// data register
//reg [`DATA_INDEX_LIMIT:0] DATA_REG;
reg [`DATA_INDEX_LIMIT:0] DATA_W;

integer i; // index for memory operation
integer no_of_test_R, no_of_pass_R;
integer no_of_test_W, no_of_pass_W;
integer load_data;

// wire lists
wire  CLK;
//wire [`DATA_INDEX_LIMIT:0] DATA;
wire [`DATA_INDEX_LIMIT:0] DATA_R1;
wire [`DATA_INDEX_LIMIT:0] DATA_R2;

//assign DATA = ((READ===1'b0)&&(WRITE===1'b1))?DATA_REG:{`DATA_WIDTH{1'bz}};
assign DATA_R1 = ((READ===1'b0)&&(WRITE===1'b1))?DATA_R1:{`DATA_WIDTH{1'bz}};
assign DATA_R2 = ((READ===1'b0)&&(WRITE===1'b1))?DATA_R2:{`DATA_WIDTH{1'bz}};

// Clock generator instance
CLK_GENERATOR clk_gen_inst(.CLK(CLK));

// Register File 32x32 instance
// (do not need to load the RF externally though like the memory testing). 
//defparam mem_inst.mem_init_file = "mem_content_01.dat";
//MEMORY_64MB mem_inst(.DATA(DATA), .ADDR(ADDR), .READ(READ), 
//                     .WRITE(WRITE), .CLK(CLK), .RST(RST));
REGISTER_FILE_32x32 reg_inst (.DATA_R1(DATA_R1), .DATA_R2(DATA_R2), 
  .ADDR_R1(ADDR_R1), .ADDR_R2(ADDR_R2), .DATA_W(DATA_W), .ADDR_W(ADDR_W), 
  .READ(READ), .WRITE(WRITE), .CLK(CLK), .RST(RST));
initial
begin
RST=1'b1;
READ=1'b0;
WRITE=1'b0;
//DATA_REG = {`DATA_WIDTH{1'b0}};
no_of_test_R = 0;
no_of_pass_R = 0;
no_of_test_W = 0;
no_of_pass_W = 0;
//load_data = 'h00414020;

// Start the operation
#10    RST=1'b0;
#10    RST=1'b1;

// Write cycle
for(i = 1; i < 10; i = i + 1)
begin
//#10   DATA_REG=i; READ=1'b0; WRITE=1'b1; ADDR = i;
#10     DATA_W=i; READ=1'b0; WRITE=1'b1; ADDR_W = i;
end

// Read Cycle (DATA_R1)
#10   READ=1'b0; WRITE=1'b0;
#5    no_of_test_R = no_of_test_R + 1;
     if (DATA_R1 !== {`DATA_WIDTH{1'bz}})
       $write("[TEST_R_DATA_R1] Read %1b, Write %1b, expecting 32'hzzzzzzzz, got %8h [FAILED]\n", READ, WRITE, DATA_R1);
     else 
	no_of_pass_R  = no_of_pass_R + 1;



// Read Cycle (DATA_R2)
//#10   READ=1'b0; WRITE=1'b0;
//#5    no_of_test_R = no_of_test_R + 1;
//      if (DATA_R2 !== {`DATA_WIDTH{1'bz}})
//        $write("[TEST_R_DATA_R2] Read %1b, Write %1b, expecting 32'hzzzzzzzz, got %8h [FAILED]\n", READ, WRITE, DATA_R2);
//      else 
//	no_of_pass_R  = no_of_pass_R + 1;


// test of write data (DATA_R1)
for(i=0;i<10; i = i + 1)
begin
//#5      READ=1'b1; WRITE=1'b0; ADDR = i;
#5      READ=1'b1; WRITE=1'b0; ADDR_R1 = i;
#5      no_of_test_W = no_of_test_W + 1;
        if (DATA_R1 !== i)
	    $write("[TEST_W_DATA_R1] Read %1b, Write %1b, expecting %8h, got %8h [FAILED]\n", READ, WRITE, i, DATA_R1);
        else 
	    no_of_pass_W  = no_of_pass_W + 1;
end

// test of write data (DATA_R2)
for(i=0;i<10; i = i + 1)
begin
//#5      READ=1'b1; WRITE=1'b0; ADDR = i;
#5      READ=1'b1; WRITE=1'b0; ADDR_R2 = i;
#5      no_of_test_W = no_of_test_W + 1;
        if (DATA_R2 !== i)
	    $write("[TEST_W_DATA_R2] Read %1b, Write %1b, expecting %8h, got %8h [FAILED]\n", READ, WRITE, i, DATA_R2);
        else 
	    no_of_pass_W  = no_of_pass_W + 1;

end

#10    READ=1'b0; WRITE=1'b0; // No op

#10 $write("\n");
    $write("\tTotal number of R tests %d\n", no_of_test_R);
    $write("\tTotal number of R pass  %d\n", no_of_pass_R);
    $write("\tTotal number of W tests %d\n", no_of_test_W);
    $write("\tTotal number of W pass  %d\n", no_of_pass_W);

    $write("\n");
    $stop;

end
endmodule

