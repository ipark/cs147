CS147. Computer Architecture (Spring2019 @SJSU)
=========

Project 1: Implementation of ALU Module in Verilog, Simulation and Test using ModelSim
---------
Project 2: Putting Altogether in DaVinci v1.0 Computer System: Verilog Behavioural/RTL Modelling
---------

Project 3: The DaVinci v1.0m Computer System: Mixed Modeling in Verilog: Behavioral + Gate Level
---------

Topics:
-------

Introduction to Computer, Basic Instruction Set, ALU | Tool Setup for ModelSim

Clock, Memory, Controller, Von-Neumann Architecture, System Software | Hierarchical Models

Digital Synthesis, Number Representation  | Simulation Project

Boolean Algebra | Data Flow Modeling 

Combinational/Sequential Logic | Memory Modeling

Sequential Logic Design, Common Digital Components | Behavioral Modeling 

Addition / Subtraction Logic Circuit | Behavioral Modeling

Multiplication / Division Logic Circuit | Behavioral Modeling 

Putting Together a Microprocessor 

Instruction Set Architecture, RISC/CISC Project 

Processor Performance Measurement  | Gate Level Modeling 

Pipeline Architecture | Gate Level Modeling 

ILP, Hardware Threading 

Parallel Processing 

Memory Hierarchy, Cache Memory 

Virtual Memory Concept