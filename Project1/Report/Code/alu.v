// 
// Name: alu.v
// Module: simp_alu_comb
// Input: op1[32] - operand 1
//        op2[32] - operand 2
//        oprn[6] - operation code
// Output: result[32] - output result for the operation
//
// Notes: 32 bit combinatorial ALU
// 
// Supports the following functions
//- Integer add (0x1), sub(0x2), mul(0x3)
//- Integer shift_rigth (0x4), shift_left (0x5)
//- Bitwise and (0x6), or (0x7), nor (0x8)
//- set less than (0x9)
//
//
`include "prj_definition.v"
///////////////////////////////////
module alu(result, op1, op2, oprn);
///////////////////////////////////
// input list
// operand 1
input [`DATA_INDEX_LIMIT:0] op1; 
// operand 2
input [`DATA_INDEX_LIMIT:0] op2; 
// operation code
input [`ALU_OPRN_INDEX_LIMIT:0] oprn; 

// output list
// result of the operation.
output [`DATA_INDEX_LIMIT:0] result; 

// simulator internal storage - this is not h/w register
reg [`DATA_INDEX_LIMIT:0] result;

// Whenever op1, op2 or oprn changes do something
always @ (op1 or op2 or oprn)
begin
 case (oprn)
  // Integer add (0x1), sub(0x2), mul(0x3)                     
  `ALU_OPRN_WIDTH'h01:result= op1 + op2; // add
  `ALU_OPRN_WIDTH'h02:result= op1 - op2; // sub
  `ALU_OPRN_WIDTH'h03:result= op1 * op2; // mult
  // Integer shift_rigth (0x4), shift_left (0x5)
  `ALU_OPRN_WIDTH'h04:result= op1 >> op2; //shftR
  `ALU_OPRN_WIDTH'h05:result= op1 << op2; //shftL
  // Bitwise and (0x6), or (0x7), nor (0x8)
  `ALU_OPRN_WIDTH'h06:result= op1 & op2; // AND
  `ALU_OPRN_WIDTH'h07:result= op1 | op2; // OR
  `ALU_OPRN_WIDTH'h08:result= ~(op1 | op2); //NOR
  // Set less than (0x9)
  `ALU_OPRN_WIDTH'h09:result= (op1 < op2); // <
  
 default: result = `DATA_WIDTH'hxxxxxxxx;
          
 endcase
end

endmodule
