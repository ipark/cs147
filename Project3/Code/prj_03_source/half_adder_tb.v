/* CS147-Lab11 */
`timescale 1ns/1ps // 1000, 3 digits after decimal 
module half_adder_tb;
/*******************/
/* registers for inputs */
reg A, B;
/* wires for outputs */
wire Y, C;
/* instance */
HALF_ADDER hs_inst_1(.Y(Y), .C(C), .A(A), .B(B));
initial
begin
A = 0; B = 0;
#5 A = 1; B = 0;
#5 A = 0; B = 1;
#5 A = 1; B = 1;
end
endmodule

