/* CS147 - Spring2019 - Inhee Park */
`timescale 1ns/1ps // 1000, 3 digits after decimal 
module mult_tb;
/*************/

/* Testbench for 32-bit combinational multipler */
// input list
reg [31:0] A; // MCND
reg [31:0] B; // MPLR
// output wire
wire [31:0] HI;
wire [31:0] LO;
// instantiation
/*
MULT32_U mult_unsigned_tb(.HI(HI), .LO(LO), .A(A), .B(B));
initial
begin
    A=0; B=0;
#5  A='b1011; B='b1100;
#5  A=0; B=0;
end
*/

// instantiation
MULT32 mult_signed_tb(.HI(HI), .LO(LO), .A(A), .B(B));
initial
begin
    A=0; B=0;
#5  A=11; B=-12;
#5  A=-11; B=-12;
#5  A=0; B=0;
end

/////////
endmodule
