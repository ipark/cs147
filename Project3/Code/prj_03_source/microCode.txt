// R-type general
//R1)  6'h20: add : R[rd]=R[rs]+R[rt]       
//R2)  6'h22: sub : R[rd]=R[rs]-R[rt]
//R3)  6'h2c: mul : R[rd]=R[rs]*R[rt]
//R4)  6'h24: and : R[rd]=R[rs]&R[rt]
//R5)  6'h25: or  : R[rd]=R[rs]|R[rt]
//R6)  6'h27: nor : R[rd]=~(R[rs]|R[rt])
//R7)  6'h2a: slt : R[rd]=(R[rs]<R[rt])?0:1
// R-type shift
//R8)  6'h01: sll : R[rd]=R[rs]<<shamt
//R9)  6'h02: srl : R[rd]=R[rs]>>shamt
/ R-type jump register
//R10) 6'h08: jr  : PC=R[rs]
// I-type arithmetic
//I1)  6'h08 : addi: R[rt]=R[rs]+SignExtImm              
//I2)  6'h1d : muli: R[rt]=R[rs]*SignExtImm
// I-type logical
//I3)  6'h0c : andi: R[rt]=R[rs]&ZeroExtImm
//I4)  6'h0d : ori : R[rt]=R[rs]|ZeroExtImm
//I5)  6'h0f : lui : R[rt]={immediate,16'b0}
//I6)  6'h0a : slti: R[rt]=(R[rs]<SignExtImm)?1:0
// I-type branch
//I7)  6'h04 : beq : If(R[rs]==R[rt]) PC=PC+1+BranchAddr
//I8)  6'h05 : bne : If(R[rs]!=R[rt]) PC=PC+1+BranchAddr
// I-type lw
//I9)  6'h23 : lw  : R[rt]=M[R[rs]+SignExtImm] 
// I-type lw
//I10) 6'h2b : sw  : M[R[rs]+SignExtImm]=R[rt]
// J-type jmp
//J1)  6'h02: jmp : PC=JumpAddr={6'b0, address}
// J-type jal
//J2)  6'h03: jal : R[31]=PC+1; PC=JumpAddr
// J-type pop/push
//J3)  6'h1b: push: M[$sp]=R[0]; $sp-=1
//J4)  6'h1c: pop : $sp+=1; R[0]=M[$sp]
