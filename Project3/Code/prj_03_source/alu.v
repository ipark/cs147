// Name: alu.v
// Module: ALU
// Input: OP1[32] - operand 1
//        OP2[32] - operand 2
//        OPRN[6] - operation code
// Output: OUT[32] - output result for the operation
//
// Notes: 32 bit combinatorial ALU
// 
// Supports the following functions
//	- Integer add (0x1), sub(0x2), mul(0x3)
//	- Integer shift_rigth (0x4), shift_left (0x5)
//	- Bitwise and (0x6), or (0x7), nor (0x8)
//  - set less than (0x9)
//
// Revision History:
//
// Version	Date		Who		email			note
//------------------------------------------------------------------------------------------
//  1.0     Sep 10, 2014	Kaushik Patra	kpatra@sjsu.edu		Initial creation
//  1.1     Oct 19, 2014        Kaushik Patra   kpatra@sjsu.edu         Added ZERO status output
//------------------------------------------------------------------------------------------
`include "prj_definition.v"
// 2. Copy functionality from project 1. 
// Need to change the port names to match the latest port naming.
module ALU(OUT, ZERO, OP1, OP2, OPRN);
/************************************/
// input list
input [`DATA_INDEX_LIMIT:0] OP1; // operand 1
input [`DATA_INDEX_LIMIT:0] OP2; // operand 2
input [`ALU_OPRN_INDEX_LIMIT:0] OPRN; // operation code

// output list
output [`DATA_INDEX_LIMIT:0] OUT; // result of the operation.
output ZERO;

// wire for outputs
wire SnA, LnR, OPRN0n, OPRN3andOPRN0, orOut;
not not_G(OPRN0n, OPRN[0]); // OPRN[0]'
and and_G(OPRN3andOPRN0, OPRN[3], OPRN[0]);
or or_G(orOut, OPRN0n, OPRN3andOPRN0); 
buf buf_G1(SnA, orOut); // SnA
buf buf_G2(LnR, OPRN[0]); // LnR

// OPRN_CODE = xx00001 (add) SnA=OPRN[0]'+OPRN[3].OPRN[0]
//  RC_ADD_SUB_32(Y, CO, A, B, SnA); 
//  output [31:0] Y; => Y_add
//  output CO; => CO_add
//  input [31:0] A; => OP1
//  input [31:0] B; => OP2
//  input SnA;
wire [31:0] Y_add;  // => I1
wire CO_add;
RC_ADD_SUB_32 add32(Y_add, CO_add, OP1, OP2, SnA);

// OPRN_CODE = xx00010 (sub) SnA=OPRN[0]'+OPRN[3].OPRN[0]
wire [31:0] Y_sub;  // => I2
wire CO_sub;
RC_ADD_SUB_32 sub32(Y_sub, CO_sub, OP1, OP2, SnA);

// OPRN_CODE = xx00011 (mul)
wire [31:0] Y_multL; // 
wire [31:0] Y_multH; // => I3; 
// MULT32(HI, LO, A, B);
//output [31:0] HI, LO;
//input [31:0] A, B;
MULT32 mul32(Y_multH, Y_multL, OP1, OP2);

// OPRN_CODE = xx00100 (shtR) LnR=OPRN[0]
wire [31:0] Y_shtR; // => I4; 
// SHIFT32(Y, D, S, LnR);
//output [31:0] Y;
//input [31:0] D;
//input [31:0] S; 
//input LnR;
SHIFT32 sr32(Y_shtR, OP1, OP2, 1'b0);

// OPRN_CODE = xx00101 (shtL) LnR=OPRN[0]
wire [31:0] Y_shtL; // => I5; 
SHIFT32 sl32(Y_shtL, OP1, OP2, 1'b1);

// OPRN_CODE = xx00110 (and)
wire [31:0] Y_and; // => I6; 
// AND32_2x1(Y,A,B);
AND32_2x1 and32(Y_and, OP1, OP2);

// OPRN_CODE = xx00111 (or )
wire [31:0] Y_or; // => I7; 
OR32_2x1 or32(Y_or, OP1, OP2);

// OPRN_CODE = xx01000 (nor)
wire [31:0] Y_nor; // => I8; 
NOR32_2x1 nor32(Y_nor, OP1, OP2);

// OPRN_CODE = xx01001 (slt) SnA=OPRN[0]'+OPRN[3].OPRN[0]
wire [31:0] Y_slt; // => I9; 
wire CO_slt;
RC_ADD_SUB_32 slt32(Y_slt, CO_slt, OP1, OP2, SnA);

//module MUX32_16x1(Y, I0, I1, I2, I3, I4, I5, I6, I7,
//                     I8, I9, I10, I11, I12, I13, I14, I15, S);
//output [31:0] Y;
//input  [31:0] I0, I1, I2, I3, I4, I5, I6, I7, I8, I9, I10, 
//  I11, I12, I13, I14, I15;
//input [3:0] S;
wire [31:0] mSel32;
MUX32_16x1 mux32_alu(mSel32, 
  Y_add, Y_add, Y_sub, Y_multL, Y_shtR, Y_shtL, Y_and, Y_or, 
  Y_nor, {{31{1'b0}}, {Y_slt[31]}}, Y_add, Y_add, Y_add, Y_add, Y_add, Y_add, 
  OPRN[3:0]);

// XOR1_32x0(Zout, A, 1'b0);
// output Zout;
// input [31:0] A;
XOR1_32x0 xor1_G(ZERO, mSel32);
BUF32 b32(OUT, mSel32);

/*
always @(OP1 or OP2 or OPRN)
begin
  case (OPRN)
    `ALU_OPRN_WIDTH'h01 : OUT = OP1 + OP2; // addition        
    `ALU_OPRN_WIDTH'h02 : OUT = OP1 - OP2; // subtraction
    `ALU_OPRN_WIDTH'h03 : OUT = OP1 * OP2; // multiplication
    `ALU_OPRN_WIDTH'h04 : OUT = OP1 >> OP2; // shift_right
    `ALU_OPRN_WIDTH'h05 : OUT = OP1 << OP2; // shift_left
    `ALU_OPRN_WIDTH'h06 : OUT = OP1 & OP2; // bitwise and
    `ALU_OPRN_WIDTH'h07 : OUT = OP1 | OP2; // bitwise or
    `ALU_OPRN_WIDTH'h08 : OUT = ~(OP1 | OP2); // bitwise nor
    `ALU_OPRN_WIDTH'h09 : OUT = (OP1 < OP2); // set less than
    default: OUT = `DATA_WIDTH'hxxxxxxxx;
  endcase
  // 3. Extend the functionality to set the 'ZERO' output, which is 
  // set to 1 if result of the current ALU operation is zero.  
  // It is set to 1 otherwise.
  ZERO = (OUT === 1'b0) ? 1'b1 : 1'b0;
end
*/

endmodule
