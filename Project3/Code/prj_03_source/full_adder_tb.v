/* CS147 - Spring2019 - Inhee Park */
`timescale 1ns/1ps // 1000, 3 digits after decimal 
module full_adder_tb;
/*******************/
/* registers for inputs */
reg A, B, CI;
/* wires for outputs */
wire S, CO;
/* instance */
FULL_ADDER fa_inst_1(.S(S), .CO(CO), .A(A), .B(B), .CI(CI));
initial
begin
   A = 0; B = 0; CI = 0; // 1
#5 A = 0; B = 1; CI = 0; // 2
#5 A = 1; B = 0; CI = 0; // 3
#5 A = 1; B = 1; CI = 0; // 4
#5 A = 0; B = 0; CI = 1; // 5
#5 A = 0; B = 1; CI = 1; // 6
#5 A = 1; B = 0; CI = 1; // 7
#5 A = 1; B = 1; CI = 1; // 8
end
endmodule

