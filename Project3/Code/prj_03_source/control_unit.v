// Name: control_unit.v
// Module: CONTROL_UNIT
// Output: CTRL  : Control signal for data path
//         READ  : Memory read signal
//         WRITE : Memory Write signal
//
// Input:  ZERO : Zero status from ALU
//         CLK  : Clock signal
//         RST  : Reset Signal
//
// Notes: - Control unit synchronize operations of a processor
//          Assign each bit of control signal to control one part of data path
//
// Revision History:
//
// Version	Date		Who		email			note
//------------------------------------------------------------------------------------------
//  1.0     Sep 10, 2014	Kaushik Patra	kpatra@sjsu.edu		Initial creation
//------------------------------------------------------------------------------------------
`include "prj_definition.v"
module CONTROL_UNIT(CTRL, READ, WRITE, ZERO, INSTRUCTION, CLK, RST); 
/******************************************************************/
// Output signals
output [`CTRL_WIDTH_INDEX_LIMIT:0]  CTRL;
output READ, WRITE;

// input signals
input ZERO, CLK, RST;
input [`DATA_INDEX_LIMIT:0] INSTRUCTION;
// State nets
wire [2:0] proc_state;

PROC_SM state_machine(.STATE(proc_state),.CLK(CLK),.RST(RST));
// registers for output
reg [`CTRL_WIDTH_INDEX_LIMIT:0]  CTRL;
reg READ, WRITE;
reg [`DATA_INDEX_LIMIT:0] INST_REG;
//            3         2         1         0;
//           10987654321098765432109876543210; 
//  CTRL=32'b00000000000000000000000000000000; 
// Always at the change of processor state set the control signal 
// and address / data signal values properly for memory, register file and ALU.
always @ (proc_state) begin // proc_state
case(proc_state)  // proc_state
//////////////////////////////////
`PROC_FETCH: begin  // PROF_FETCH
  // Set memory address to program counter, 
  // memory control for read operation. 
  READ = 1'b1; WRITE = 1'b0; 
  //-------10987654321098765432109876543210; 
  CTRL=32'b00000000001000000000000000000000; // CTRL[21] ma_sel_2=1
end  // PROF_FETCH
//////////////////////////////////
`PROC_DECODE: begin // PROC_DECODE
  // hold for previous bits
  READ = 1'b0; WRITE = 1'b0; 
  // Store the memory read data into INST_REG; 
  INST_REG =  INSTRUCTION;
  //-------10987654321098765432109876543210; 
  CTRL=32'b00000000000000000000000001010000; // CTRL[6]reg_r=1; CTRL[4]ir_load=1
end  // PROC_DECODE
//////////////////////////////////
`PROC_EXE: begin // PROC_EXE
// hold for previous bits
READ = 1'b0; WRITE = 1'b0; 
case(INST_REG[31:26]) // opcode
// R-Type ///////////////////
6'h00: begin  // R-type
  case(INST_REG[5:0])
  6'h20: begin //R1)  6'h20: add : R[rd]=R[rs]+R[rt] 
    //-------10987654321098765432109876543210; 
    CTRL=32'b00000000000000000110000000000000;
    //1; CTRL[13]op2_sel1_4=1; alu_oprn[19:14]=000001
  end
  6'h22: begin //R2)  6'h22: sub : R[rd]=R[rs]-R[rt]
    //-------10987654321098765432109876543210; 
    CTRL=32'b00000000000000001010000000000000;
    //2; CTRL[13]op2_sel1_4=1; alu_oprn[19:14]=000010
  end
  6'h2c: begin //R3)  6'h2c: mul : R[rd]=R[rs]*R[rt]
    //-------10987654321098765432109876543210; 
    CTRL=32'b00000000000000001110000000000000;
    //3; CTRL[13]op2_sel1_4=1; alu_oprn[19:14]=000011
  end
  6'h24: begin //R4)  6'h24: and : R[rd]=R[rs]&R[rt]
    //-------10987654321098765432109876543210; 
    CTRL=32'b00000000000000011010000000000000;
    //6; CTRL[13]op2_sel1_4=1; alu_oprn[19:14]=000110
  end
  6'h25: begin //R5)  6'h25: or  : R[rd]=R[rs]|R[rt]
    //-------10987654321098765432109876543210; 
    CTRL=32'b00000000000000011110000000000000;
    //7; CTRL[13]op2_sel1_4=1; alu_oprn[19:14]=000111
  end
  6'h27: begin //R6)  6'h27: nor : R[rd]=~(R[rs]|R[rt])
    //-------10987654321098765432109876543210; 
    CTRL=32'b00000000000000100010000000000000;
    //8; CTRL[13]op2_sel1_4=1; alu_oprn[19:14]=001000
  end
  6'h2a: begin //R7)  6'h2a: slt : R[rd]=(R[rs]<R[rt])?0:1
    //-------10987654321098765432109876543210; 
    CTRL=32'b00000000000000100110000000000000;
    //9; CTRL[13]op2_sel1_4=1; alu_oprn[19:14]=001001
  end
  6'h01: begin //R8)  6'h01: sll : R[rd]=R[rs]<<shamt
    //-------10987654321098765432109876543210; 
    CTRL=32'b00000000000000010110000000000000;
    //5; CTRL[13]op2_sel1_4=1; alu_oprn[19:14]=000101
  end
  6'h02: begin //R9)  6'h02: srl : R[rd]=R[rs]>>shamt
    //-------10987654321098765432109876543210; 
    CTRL=32'b00000000000000010010000000000000;
    //4; CTRL[13]op2_sel1_4=1; alu_oprn[19:14]=000100
  end
  6'h08: begin //R10) 6'h08: jr  : PC=R[rs]
    //-------10987654321098765432109876543210; 
    CTRL=32'b00000000010000000000000000000000;
    //CTRL[22]md_sel_1=1;
  end
  endcase // case(INST_REG[5:0]) 
end // R-type
// I-Type //////////////////////////////////////
6'h08: begin//I1)  6'h08 : addi: R[rt]=R[rs]+SignExtImm              
  //-------10987654321098765432109876543210;                                              
  CTRL=32'b00000000000000000100100000000000;
  //CTRL[11]op2_sel_2=1; alu_oprn[19:14]=000001
end
6'h1d: begin//I2)  6'h1d : muli: R[rt]=R[rs]*SignExtImm
  //-------10987654321098765432109876543210;                                              
  CTRL=32'b00000000000000001100100000000000;
  //CTRL[11]op2_sel_2=1; alu_oprn[19:14]=000011
end
6'h0c: begin//I3)  6'h0c : andi: R[rt]=R[rs]&ZeroExtImm
  //-------10987654321098765432109876543210;                                              
  CTRL=32'b00000000000000011000100000000000;
  //CTRL[11]op2_sel_2=1; alu_oprn[19:14]=000110
end
6'h0d: begin//I4)  6'h0d : ori : R[rt]=R[rs]|ZeroExtImm
  //-------10987654321098765432109876543210;                                              
  CTRL=32'b00000000000000011100100000000000;
  //CTRL[11]op2_sel_2=1; alu_oprn[19:14]=000111
end
6'h0f: begin//I5)  6'h0f : lui : R[rt]={immediate,16'b0}
  //-------10987654321098765432109876543210;                                              
  CTRL=32'b00000000000000000000000000000000;
end
6'h0a: begin//I6)  6'h0a : slti: R[rt]=(R[rs]<SignExtImm)?1:0
  //-------10987654321098765432109876543210;                                              
  CTRL=32'b00000000000000100100100000000000;
  //CTRL[11]op2_sel_2=1; alu_oprn[19:14]=001001
end
6'h04: begin//I7)  6'h04 : beq : If(R[rs]==R[rt]) PC=PC+1+BranchAddr
  //-------10987654321098765432109876543210;                                              
  CTRL=32'b00000000000000001010000000000000;
end
6'h05: begin//I8)  6'h05 : bne : If(R[rs]!=R[rt]) PC=PC+1+BranchAddr
  //-------10987654321098765432109876543210;                                              
  CTRL=32'b00000000000000000000000000000000;
end
6'h23: begin//I9)  6'h23 : lw  : R[rt]=M[R[rs]+SignExtImm] 
  //-------10987654321098765432109876543210;                                              
  CTRL=32'b00000000000000000100100000000000;
end
6'h2b: begin//I10) 6'h2b : sw  : M[R[rs]+SignExtImm]=R[rt]
  //-------10987654321098765432109876543210;                                              
  CTRL=32'b00000000000000000100100000000000;
end
// J-Type //////////////////////////////////////
6'h02: begin//J1)  6'h02: jmp : PC=JumpAddr={6'b0, address}
  //-------10987654321098765432109876543210;                                              
  CTRL=32'b00000000000000000000000000000000;
end
6'h03: begin//J2)  6'h03: jal : R[31]=PC+1; PC=JumpAddr
  //-------10987654321098765432109876543210;                                              
  CTRL=32'b00000000000000000000000000000000;
end
6'h1b: begin//J3)  6'h1b: push: M[$sp]=R[0]; $sp-=1
  //-------10987654321098765432109876543210;                                              
  CTRL=32'b00000000000000001001001100000000;
end
6'h1c: begin//J4)  6'h1c: pop : $sp+=1; R[0]=M[$sp]
  //-------10987654321098765432109876543210;                                              
  CTRL=32'b00000000000000000101001100000000;
end
endcase // case(INST_REG[31:26]) // opcode
end // PROC_EXE
//////////////////////////////////
`PROC_MEM: begin  // PROC_MEM
//Only 'lw', 'sw', 'push' and 'pop' instructions are involved in this stage. 
READ = 1'b0; WRITE = 1'b0;  // memory output to HighZ (hold)
case(INST_REG[31:26]) // opcode
6'h23: begin //I9)  6'h23 : lw  : R[rt]=M[R[rs]+SignExtImm] 
  READ=1'b0; WRITE=1'b1;
  //-------10987654321098765432109876543210; 
  CTRL=32'b00000000000000000100100000000000;
end
6'h2b: begin //I10) 6'h2b : sw  : M[R[rs]+SignExtImm]=R[rt]
  //-------10987654321098765432109876543210; 
  CTRL=32'b00000000000000000100100000000000;
end
6'h1b: begin //J3)  6'h1b: push: M[$sp]=R[0]; $sp-=1
  READ=1'b0; WRITE=1'b1;
  //-------10987654321098765432109876543210; 
  CTRL=32'b00000000000100001000000000000000;
  // CTRL[15]=1; CTRL[20]=1
end
6'h1c: begin //J4)  6'h1c: pop : $sp+=1; R[0]=M[$sp]
  //-------10987654321098765432109876543210; 
  CTRL=32'b00000000000100000100000000000000;
  // CTRL[14]=1; CTRL[20]=1
end
endcase // case(INST_REG[31:26]) // opcode
end // PROC_MEM
//////////////////////////////////
`PROC_WB: begin // PROC_WB
READ = 1'b0; WRITE = 1'b0;  // no-op {00} 
case(INST_REG[31:26]) // opcode
// R-Type ///////////////////
6'h00: begin  // R-type
  case(INST_REG[5:0])
  6'h20: begin //R1)  6'h20: add : R[rd]=R[rs]+R[rt] 
    //-------10987654321098765432109876543210; 
    CTRL=32'b00010010000000000110000010001011;
  end
  6'h22: begin //R2)  6'h22: sub : R[rd]=R[rs]-R[rt]
    //-------10987654321098765432109876543210; 
    CTRL=32'b00010010000000000110000010001011;
    //2; CTRL[13]op2_sel1_4=1; alu_oprn[19:14]=000010
  end
  6'h2c: begin //R3)  6'h2c: mul : R[rd]=R[rs]*R[rt]
    //-------10987654321098765432109876543210; 
    CTRL=32'b00010010000000000110000010001011;
    //3; CTRL[13]op2_sel1_4=1; alu_oprn[19:14]=000011
  end
  6'h24: begin //R4)  6'h24: and : R[rd]=R[rs]&R[rt]
    //-------10987654321098765432109876543210; 
    CTRL=32'b00010010000000000110000010001011;
    //6; CTRL[13]op2_sel1_4=1; alu_oprn[19:14]=000110
  end
  6'h25: begin //R5)  6'h25: or  : R[rd]=R[rs]|R[rt]
    //-------10987654321098765432109876543210; 
    CTRL=32'b00010010000000000110000010001011;
    //7; CTRL[13]op2_sel1_4=1; alu_oprn[19:14]=000111
  end
  6'h27: begin //R6)  6'h27: nor : R[rd]=~(R[rs]|R[rt])
    //-------10987654321098765432109876543210; 
    CTRL=32'b00010010000000000110000010001011;
    //8; CTRL[13]op2_sel1_4=1; alu_oprn[19:14]=001000
  end
  6'h2a: begin //R7)  6'h2a: slt : R[rd]=(R[rs]<R[rt])?0:1
    //-------10987654321098765432109876543210; 
    CTRL=32'b00010010000000000110000010001011;
    //9; CTRL[13]op2_sel1_4=1; alu_oprn[19:14]=001001
  end
  6'h01: begin //R8)  6'h01: sll : R[rd]=R[rs]<<shamt
    //-------10987654321098765432109876543210; 
    CTRL=32'b00010110000000010101010010001011;
    //5; CTRL[13]op2_sel1_4=1; alu_oprn[19:14]=000101
  end
  6'h02: begin //R9)  6'h02: srl : R[rd]=R[rs]>>shamt
    //-------10987654321098765432109876543210; 
    CTRL=32'b00010110000000010001010010001011;
    //4; CTRL[13]op2_sel1_4=1; alu_oprn[19:14]=000100
  end
  6'h08: begin //R10) 6'h08: jr  : PC=R[rs]
    //-------10987654321098765432109876543210; 
    CTRL=32'b00000000010000010000000000001001;
    //CTRL[22]md_sel_1=1;
  end
  endcase // case(INST_REG[5:0]) 
end // R-type
// I-Type //////////////////////////////////////
6'h08: begin//I1)  6'h08 : addi: R[rt]=R[rs]+SignExtImm              
  //-------10987654321098765432109876543210;                                              
  CTRL=32'b00000000000000000100100000000000;
  //CTRL[11]op2_sel_2=1; alu_oprn[19:14]=000001
end
6'h1d: begin//I2)  6'h1d : muli: R[rt]=R[rs]*SignExtImm
  //-------10987654321098765432109876543210;                                              
  CTRL=32'b00000000000000001100100000000000;
  //CTRL[11]op2_sel_2=1; alu_oprn[19:14]=000011
end
6'h0c: begin//I3)  6'h0c : andi: R[rt]=R[rs]&ZeroExtImm
  //-------10987654321098765432109876543210;                                              
  CTRL=32'b00000000000000011000100000000000;
  //CTRL[11]op2_sel_2=1; alu_oprn[19:14]=000110
end
6'h0d: begin//I4)  6'h0d : ori : R[rt]=R[rs]|ZeroExtImm
  //-------10987654321098765432109876543210;                                              
  CTRL=32'b00000000000000011100100000000000;
  //CTRL[11]op2_sel_2=1; alu_oprn[19:14]=000111
end
6'h0f: begin//I5)  6'h0f : lui : R[rt]={immediate,16'b0}
  //-------10987654321098765432109876543210;                                              
  CTRL=32'b00000000000000000000000000000000;
end
6'h0a: begin//I6)  6'h0a : slti: R[rt]=(R[rs]<SignExtImm)?1:0
  //-------10987654321098765432109876543210;                                              
  CTRL=32'b00000000000000100100100000000000;
  //CTRL[11]op2_sel_2=1; alu_oprn[19:14]=001001
end
6'h04: begin//I7)  6'h04 : beq : If(R[rs]==R[rt]) PC=PC+1+BranchAddr
  //-------10987654321098765432109876543210;                                              
  CTRL=32'b00000000000000001010000000000000;
end
6'h05: begin//I8)  6'h05 : bne : If(R[rs]!=R[rt]) PC=PC+1+BranchAddr
  //-------10987654321098765432109876543210;                                              
  CTRL=32'b00000000000000000000000000000000;
end
6'h23: begin//I9)  6'h23 : lw  : R[rt]=M[R[rs]+SignExtImm] 
  //-------10987654321098765432109876543210;                                              
  CTRL=32'b00000000000000000100100000000000;
end
6'h2b: begin//I10) 6'h2b : sw  : M[R[rs]+SignExtImm]=R[rt]
  //-------10987654321098765432109876543210;                                              
  CTRL=32'b00000000000000000100100000000000;
end
// J-Type //////////////////////////////////////
6'h02: begin//J1)  6'h02: jmp : PC=JumpAddr={6'b0, address}
  //-------10987654321098765432109876543210;                                              
  CTRL=32'b00000000000000000000000000000000;
end
6'h03: begin//J2)  6'h03: jal : R[31]=PC+1; PC=JumpAddr
  //-------10987654321098765432109876543210;                                              
  CTRL=32'b00000000000000000000000000000000;
end
6'h1b: begin//J3)  6'h1b: push: M[$sp]=R[0]; $sp-=1
  //-------10987654321098765432109876543210;                                              
  CTRL=32'b00000000000000001001001100000000;
end
6'h1c: begin//J4)  6'h1c: pop : $sp+=1; R[0]=M[$sp]
  //-------10987654321098765432109876543210;                                              
  CTRL=32'b00000000000000000101001100000000;
end
endcase // case(INST_REG[31:26]) // opcode
end // PROC_WB
////////////////////////
endcase // (proc_state)
end // always @ (proc_state)
endmodule


//------------------------------------------------------------------------------------------
// Module: PROC_SM
// Output: STATE      : State of the processor
//         
// Input:  CLK        : Clock signal
//         RST        : Reset signal
//
// INOUT: MEM_DATA    : Data to be read in from or write to the memory
//
// Notes: - Processor continuously cycle witnin fetch, decode, execute, 
//          memory, write back state. State values are in the prj_definition.v
//
// Revision History:
//
// Version	Date		Who		email			note
//------------------------------------------------------------------------------------------
//  1.0     Sep 10, 2014	Kaushik Patra	kpatra@sjsu.edu		Initial creation
//------------------------------------------------------------------------------------------
module PROC_SM(STATE,CLK,RST);
/****************************/
// list of inputs
input CLK, RST;
// list of outputs
output [2:0] STATE;

// 2. State machine implementation 
// (refer to lab08 source code for writing state machine)
// 2a. Modify module 'module PROC_SM(STATE,CLK,RST);'
// 2b. Define state and next_state register.
reg [2:0] STATE;
reg [2:0] next_state;

// 2c. State machine is reset on negative edge of RST. 
//     At reset and 'initial' set the next state as `PROC_FETCH 
//     (defined in proj_definition.v) and state to 3 bit unknown (3'bxxx).
// initiation of state
initial
begin
  STATE = 3'bxxx;
  next_state = `PROC_FETCH;
end
// reset signal handling
always @ (negedge RST)
begin
  STATE = 3'bxxx;
  next_state = `PROC_FETCH;
end
// 2d. Upon each positive edge of clock, assign state with next_state value. 
//     Also determine next_state depending on current state value.
//    `PROC_FETCH -> `PROC_DECODE;
//    `PROC_DECODE -> `PROC_EXE;
//    `PROC_EXE -> `PROC_MEM;
//    `PROC_MEM -> `PROC_WB;
//    `PROC_WB -> `PROC_FETCH;
// state switching
always @(posedge CLK)
begin
  case(STATE)
    `PROC_FETCH: next_state = `PROC_DECODE; 
    `PROC_DECODE: next_state = `PROC_EXE;
    `PROC_EXE: next_state = `PROC_MEM;
    `PROC_MEM: next_state = `PROC_WB;
    `PROC_WB: next_state = `PROC_FETCH;
  endcase // STATE
  STATE = next_state;
end 
endmodule // PROC_SM


// R-Type //////////////////////////////////////
//R1)  6'h20: add : R[rd]=R[rs]+R[rt]       
//R2)  6'h22: sub : R[rd]=R[rs]-R[rt]
//R3)  6'h2c: mul : R[rd]=R[rs]*R[rt]
//R4)  6'h24: and : R[rd]=R[rs]&R[rt]
//R5)  6'h25: or  : R[rd]=R[rs]|R[rt]
//R6)  6'h27: nor : R[rd]=~(R[rs]|R[rt])
//R7)  6'h2a: slt : R[rd]=(R[rs]<R[rt])?0:1
//R8)  6'h01: sll : R[rd]=R[rs]<<shamt
//R9)  6'h02: srl : R[rd]=R[rs]>>shamt
//R10) 6'h08: jr  : PC=R[rs]
// I-Type //////////////////////////////////////
//I1)  6'h08 : addi: R[rt]=R[rs]+SignExtImm              
//I2)  6'h1d : muli: R[rt]=R[rs]*SignExtImm
//I3)  6'h0c : andi: R[rt]=R[rs]&ZeroExtImm
//I4)  6'h0d : ori : R[rt]=R[rs]|ZeroExtImm
//I5)  6'h0f : lui : R[rt]={immediate,16'b0}
//I6)  6'h0a : slti: R[rt]=(R[rs]<SignExtImm)?1:0
//I7)  6'h04 : beq : If(R[rs]==R[rt]) PC=PC+1+BranchAddr
//I8)  6'h05 : bne : If(R[rs]!=R[rt]) PC=PC+1+BranchAddr
//I9)  6'h23 : lw  : R[rt]=M[R[rs]+SignExtImm] 
//I10) 6'h2b : sw  : M[R[rs]+SignExtImm]=R[rt]
// J-Type //////////////////////////////////////
//J1)  6'h02: jmp : PC=JumpAddr={6'b0, address}
//J2)  6'h03: jal : R[31]=PC+1; PC=JumpAddr
//J3)  6'h1b: push: M[$sp]=R[0]; $sp-=1
//J4)  6'h1c: pop : $sp+=1; R[0]=M[$sp]

//`ALU_OPRN_WIDTH'h01 : OUT = OP1 + OP2; // addition        
//`ALU_OPRN_WIDTH'h02 : OUT = OP1 - OP2; // subtraction
//`ALU_OPRN_WIDTH'h03 : OUT = OP1 * OP2; // multiplication
//`ALU_OPRN_WIDTH'h04 : OUT = OP1 >> OP2; // shift_right
//`ALU_OPRN_WIDTH'h05 : OUT = OP1 << OP2; // shift_left
//`ALU_OPRN_WIDTH'h06 : OUT = OP1 & OP2; // bitwise and
//`ALU_OPRN_WIDTH'h07 : OUT = OP1 | OP2; // bitwise or
//`ALU_OPRN_WIDTH'h08 : OUT = ~(OP1 | OP2); // bitwise nor
//`ALU_OPRN_WIDTH'h09 : OUT = (OP1 < OP2); // set less than
//////////////////////////////////////////////

