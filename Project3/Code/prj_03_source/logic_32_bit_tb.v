/* CS147 - Spring2019 - Inhee Park */
`timescale 1ns/1ps // 1000, 3 digits after decimal 
module logic_32_bit_tb;
/*********************/

/* Testbench for 32-bit invert */
// input list
reg [31:0] A;
// output wire
wire [31:0] Y;
// instantiation
BUF32 buf32_tb(.Y(Y), .A(A));
initial
begin
    A=0;
#5  A=32'b11110000000000000000000;
#5  A=0;
end

/////////
endmodule
