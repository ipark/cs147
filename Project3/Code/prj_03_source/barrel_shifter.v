// Name: barrel_shifter.v
// Module: SHIFT32_L , SHIFT32_R, SHIFT32
//
// Notes: 32-bit barrel shifter
// 
//
// Revision History:
//
// Version	Date		Who		email			note
//------------------------------------------------------------------------------------------
//  1.0     Sep 10, 2014	Kaushik Patra	kpatra@sjsu.edu		Initial creation
//------------------------------------------------------------------------------------------
`include "prj_definition.v"

// 32-bit shift amount shifter
module SHIFT32(Y, D, S, LnR);
/***************************/
// output list
output [31:0] Y;
// input list
input [31:0] D;
/* 32-bit inside ALU */
input [31:0] S; 
input LnR;
/* wire for output */
wire [31:0] bsOut;
// BARREL_SHIFTER32(Y, D, S, LnR);
BARREL_SHIFTER32 bshifter32(bsOut, D, S[4:0], LnR); 
// OR gate for S[31:5] bits
// 1-bit output from 27-bit OR
// OR1_27bit(orOut, S);
wire orOut;
OR1_27bit or27(orOut, S[31:5]);

/* 32-bit ;2x1 mux */
// MUX32_2x1(Y, I0, I1, S);
//output [31:0] Y;
//input [31:0] I0;
//input [31:0] I1;
//input S;
MUX32_2x1 mux32(Y, bsOut, {32{1'b0}}, orOut);
endmodule


// Shift with control L or R shift
module BARREL_SHIFTER32(Y, D, S, LnR);
/***********************************/
// output list
output [31:0] Y;
// input list
input [31:0] D;
/* 5-bit */
input [4:0] S; 
input LnR;
/* wire for output */
wire [31:0] slOut, srOut;
/* right shifter */
SHIFT32_R sr32(srOut, D, S);
/* left shifter */
SHIFT32_L sl32(slOut, D, S); 
/* 32-bit 2x1 mux */
// MUX32_2x1(Y, I0, I1, S);
//output [31:0] Y;
//input [31:0] I0;
//input [31:0] I1;
//input S;
MUX32_2x1 mux32(Y, srOut, slOut, LnR); 
endmodule


// Left shifter
module SHIFT32_L(Y, D, S);
/************************/
// output list
output [31:0] Y;
// input list
input [31:0] D;
input [4:0] S;
// 2-diemsional array for 1-bit wire intermediate output
wire  mout[0:4][0:31];  // mout[column_index][row_index]

genvar r, c;
generate
for (c = 0; c < 5; c = c + 1) begin: col_loop 
  for (r = 0; r < 32; r = r + 1) begin: row_loop
    if (c == 0) begin
      if (r == 0) 
        MUX1_2x1 mux_c0(mout[c][r], D[r], 1'b0,   S[c]); 
      else        
        MUX1_2x1 mux_c0(mout[c][r], D[r], D[r-1], S[c]); 
    end // EO(c==0)

    else if (c == 4) begin
      if (r < 2**c) 
        MUX1_2x1 mux_c4(Y[r], mout[c-1][r], 1'b0, S[c]); 
      else          
        MUX1_2x1 mux_c4(Y[r], mout[c-1][r], mout[c-1][r-2**c], S[c]); 
    end // EO(c==4)

    else begin
      if (r < 2**c) 
        MUX1_2x1 mux_c123(mout[c][r], mout[c-1][r], 1'b0, S[c]); 
      else
        MUX1_2x1 mux_c123(mout[c][r], mout[c-1][r], mout[c-1][r-2**c], S[c]); 
    end // EO(c=1,2,3)
  end // EO_row_loop
end // EO_col_loop
endgenerate
endmodule

// Right shifter
module SHIFT32_R(Y, D, S);
/************************/
// output list
output [31:0] Y;
// input list
input [31:0] D;
input [4:0] S;
// 2-diemsional array for 1-bit wire intermediate output
wire  mout[0:4][0:31];  // mout[column_index][row_index]

genvar r, c;
generate
for (c = 0; c < 5; c = c + 1) begin: col_loop 
  for (r = 31; r >= 0 ; r = r - 1) begin: row_loop
    if (c == 0) begin
      if (r == 31) 
        MUX1_2x1 mux_c0(mout[c][r], D[r], 1'b0,   S[c]); 
      else        
        MUX1_2x1 mux_c0(mout[c][r], D[r], D[r+1], S[c]); 
    end // EO(c==0)

    else if (c == 4) begin
      if (r > 31-(2**c))
        MUX1_2x1 mux_c4(Y[r], mout[c-1][r], 1'b0, S[c]); 
      else          
        MUX1_2x1 mux_c4(Y[r], mout[c-1][r], mout[c-1][r+(2**c)], S[c]); 
    end // EO(c==4)

    else begin
      if (r > 31-(2**c))
        MUX1_2x1 mux_c123(mout[c][r], mout[c-1][r], 1'b0, S[c]);
      else
        MUX1_2x1 mux_c123(mout[c][r], mout[c-1][r], mout[c-1][r+(2**c)], S[c]);
    end // EO(c=1,2,3)
  end // EO_row_loop
end // EO_col_loop
endgenerate
endmodule


/* 4-bit left barrel shifter */
module SHIFT4_L(Y, D, S);
/***********************/
// output list
output [3:0] Y;
// input list
input [3:0] D;
input [1:0] S;
// wire for output
wire  mout[0:1][0:3];  // mout[column_index][row_index]

genvar r, c;
generate
for (c = 0; c < 2; c = c + 1) begin: col_loop 
  for (r = 0; r < 4; r = r + 1) begin: row_loop
    if (c == 0) begin
      if (r == 0) 
        MUX1_2x1 mux_c0(mout[c][r], D[r], 1'b0,   S[c]); 
      else        
        MUX1_2x1 mux_c0(mout[c][r], D[r], D[r-1], S[c]); 
    end // EO(c==0)

    else  begin
      if (r < 2**c) 
        MUX1_2x1 mux_c4(Y[r], mout[c-1][r], 1'b0, S[c]); 
      else          
        MUX1_2x1 mux_c4(Y[r], mout[c-1][r], mout[c-1][r-2**c], S[c]); 
    end // EO(c==1)
  end // EO_row_loop
end // EO_col_loop
endgenerate
endmodule


/* 4-bit right barrel shifter */
module SHIFT4_R(Y, D, S);
/***********************/
// output list
output [3:0] Y;
// input list
input [3:0] D;
input [1:0] S;
// wire for output
wire  mout[0:1][0:3];  // mout[column_index][row_index]

genvar r, c;
generate
for (c = 0; c < 2; c = c + 1) begin: col_loop 
  for (r = 4; r >= 0 ; r = r - 1) begin: row_loop
    if (c == 0) begin
      if (r == 4) 
        MUX1_2x1 mux_c0(mout[c][r], D[r], 1'b0,   S[c]); 
      else        
        MUX1_2x1 mux_c0(mout[c][r], D[r], D[r+1], S[c]); 
    end // EO(c==0)

    else  begin
      if (r > 31-(2**c))
        MUX1_2x1 mux_c4(Y[r], mout[c-1][r], 1'b0, S[c]); 
      else          
        MUX1_2x1 mux_c4(Y[r], mout[c-1][r], mout[c-1][r+(2**c)], S[c]); 
    end // EO(c==1)
  end // EO_row_loop
end // EO_col_loop
endgenerate
endmodule

