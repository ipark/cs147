// Name: register_file.v
// Module: REGISTER_FILE_32x32
// Input:  
//   DATA_W : Data to be written at address ADDR_W                    
//   ADDR_W : Address of the memory location to be written
//   ADDR_R1 : Address of the memory location to be read for DATA_R1
//   ADDR_R2 : Address of the memory location to be read for DATA_R2
//   READ    : Read signal
//   WRITE   : Write signal
//   CLK     : Clock signal
//   RST     : Reset signal
// Output: 
//   DATA_R1 : Data at ADDR_R1 address
//   DATA_R2 : Data at ADDR_R1 address
//
// Notes: 
// - 32 bit word accessible dual read register file having 32 regsisters.
// - Reset is done at -ve edge of the RST signal
// - Rest of the operation is done at the +ve edge of the CLK signal
// - Read operation is done if READ=1 and WRITE=0
// - Write operation is done if WRITE=1 and READ=0
// - X is the value at DATA_R* if both READ and WRITE are 0 or 1
//
// Revision History:
//
// Version	Date		Who		email			note
//------------------------------------------------------------------------
//  1.0     Sep 10, 2014	Kaushik Patra	kpatra@sjsu.edu		Initial creation
//------------------------------------------------------------------------
//
`include "prj_definition.v"
module REGISTER_FILE_32x32(DATA_R1, DATA_R2, ADDR_R1, ADDR_R2, 
                            DATA_W, ADDR_W, READ, WRITE, CLK, RST);
/*****************************************************************/

// input list
input READ, WRITE, CLK, RST;
input [`DATA_INDEX_LIMIT:0] DATA_W;
// DATA_INDEX_LIMIT 31
input [`REG_ADDR_INDEX_LIMIT:0] ADDR_R1, ADDR_R2, ADDR_W;
// REG_ADDR_INDEX_LIMIT = 4

// output list
output [`DATA_INDEX_LIMIT:0] DATA_R1;
output [`DATA_INDEX_LIMIT:0] DATA_R2;

//////////////////////////////////////////////////
// Gate Level Model
//////////////////////////////////////////////////

// 5x32 Line decoder:  DECODER_5x32(D, I)
// input  : [4:0] ADDR_W
// output : [31:0] D =>
wire [31:0] D;
DECODER_5x32 ld_5x32(D, ADDR_W);

// 32 instantiations of AND gate
wire [31:0] andOut;
genvar i;
generate
for (i = 0; i < 32; i = i + 1) begin: loop_andi
  and and_i(andOut[i], D[i], WRITE);
end
endgenerate

// 32 instantiations of REG32(Q, D, LOAD, CLK, RESET)
// input: [31:0] DATA_W, L (from AND gates)
// input: CLK, RST 
// output: array of [31:0] Q => 
wire [31:0] Q[0:31];
genvar j;
generate
for (j = 0; j < 32; j = j + 1) begin: loop_reg32
  REG32 reg32_j(Q[j], DATA_W, andOut[j], CLK, RST);
end
endgenerate


// 32-bit mux_32x1 : MUX32_32x1(Y, I0,..., I31, S)
// input  : [31:0] Y, I0, I1, ..., I31
// input  : [4:0] S
// output : [31:0] Y =>
wire [31:0] Y1, Y2;
MUX32_32x1 mux32_G1(Y1, Q[0], Q[1], Q[2], Q[3], Q[4], Q[5], 
Q[6], Q[7], Q[8], Q[9], Q[10], Q[11], Q[12], Q[13], Q[14], 
Q[15], Q[16], Q[17], Q[18], Q[19], Q[20], Q[21], Q[22], Q[23],
Q[24], Q[25], Q[26], Q[27], Q[28], Q[29], Q[30], Q[31], ADDR_R1);

MUX32_32x1 mux32_G2(Y2, Q[0], Q[1], Q[2], Q[3], Q[4], Q[5], 
Q[6], Q[7], Q[8], Q[9], Q[10], Q[11], Q[12], Q[13], Q[14], 
Q[15], Q[16], Q[17], Q[18], Q[19], Q[20], Q[21], Q[22], Q[23],
Q[24], Q[25], Q[26], Q[27], Q[28], Q[29], Q[30], Q[31], ADDR_R2);


// 32-bit mux_2x1 : MUX32_2x1(Y, I0, I1, S)
// input  : [31:0] Y, I0, I1
// input  : [4:0] S
// output : [31:0] Y =>
reg [31:0] I0, I1;
reg S;
wire [31:0] Y;
// MUX32_2x1(Y, I0, I1, S);
MUX32_2x1 mux32_r1(DATA_R1, 32'bz, Y1, READ);
MUX32_2x1 mux32_r2(DATA_R2, 32'bz, Y2, READ);

endmodule

//////////////////////////////////////////////////
// Behavioral Model
// RF : register_file.v (very much alike memory.v)
//////////////////////////////////////////////////
/*
// 1. Add registers for corresponding output ports.
reg [`DATA_INDEX_LIMIT:0] DATA_R1_REG;
reg [`DATA_INDEX_LIMIT:0] DATA_R2_REG;

// 2. Add 32x32 memory storage similar to 32x64M memory in memory.v
reg [`DATA_INDEX_LIMIT:0] rf_32x32[0:`DATA_INDEX_LIMIT];
integer i;

assign DATA_R1 = ((READ===1'b1)&&(WRITE===1'b0))?DATA_R1_REG:{`DATA_WIDTH{1'bz}};
assign DATA_R2 = ((READ===1'b1)&&(WRITE===1'b0))?DATA_R2_REG:{`DATA_WIDTH{1'bz}};


// 3. Add 'initial' block for initializing content of all 32 registers as 0.
initial
begin // initial
  for(i = 0; i <= `DATA_INDEX_LIMIT; i = i + 1)
    rf_32x32[i] = {`DATA_WIDTH{1'b0}};
end // initial

always @ (negedge RST or posedge CLK)
begin // always @ (negedge RST or posedge CLK)
  // 4. Register block is reset on a negative edge of RST signal.
  if (RST === 1'b0) 
    begin //if (RST === 1'b0) 
      for(i = 0; i <= `DATA_INDEX_LIMIT; i = i + 1)
        rf_32x32[i] = {`DATA_WIDTH{1'b0}};
    end //if (RST === 1'b0) 
  else
    begin // else
    // 5. On read request, 
    // both the content from address ADDR_R1 and ADDR_R2 returned. 
    if ((READ === 1'b1) && (WRITE === 1'b0)) // read operation
      begin // read oprn
	    DATA_R1_REG =  rf_32x32[ADDR_R1];
	    DATA_R2_REG =  rf_32x32[ADDR_R2];
      end // read oprn
    // 6. On write request, ADDR_W content is modified to DATA_W.
    else if ((READ === 1'b0) && (WRITE === 1'b1)) // write operation
	    rf_32x32[ADDR_W] = DATA_W;
   end // else
end // always @ (negedge RST or posedge CLK)
endmodule // REGISTER_FILE_32x32
*/
