// Name: mult.v
// Module: MULT32 , MULT32_U
//
// Output: HI: 32 higher bits
//         LO: 32 lower bits
//         
//
// Input: A : 32-bit input
//        B : 32-bit input
//
// Notes: 32-bit multiplication
// 
//
// Revision HIstory:
//
// Version	Date		Who		email			note
//------------------------------------------------------------------------------------------
//  1.0     Sep 10, 2014	Kaushik Patra	kpatra@sjsu.edu		Initial creation
//------------------------------------------------------------------------------------------
`include "prj_definition.v"

module MULT32(HI, LO, A, B);
/**************************/
// output list
output [31:0] HI;
output [31:0] LO;
// input list
input [31:0] A; // MCND
input [31:0] B; // MPLR
// wires for output
wire [31:0] invAplus1, invBplus1;
// TWOSCOMP32(Y, A);
TWOSCOMP32 tc32A(invAplus1, A); // MCND
TWOSCOMP32 tc32B(invBplus1, B); // MPLR
//
// MUX32_2x1(Y, I0, I1, S);
// input for I0: A
// input for I1: invAplus1 
// input for S: A[31]
// output: Asel
wire [31:0] Asel, Bsel;
MUX32_2x1 mux32_A(Asel, A, invAplus1, A[31]); // MCND
MUX32_2x1 mux32_B(Bsel, B, invBplus1, B[31]); // MPLR
// MULT32_U(HI, LO, A, B);
wire [63:0] prodU64; // 64-bit unsigned product 
MULT32_U multU32(prodU64[63:32], prodU64[31:0], Asel, Bsel);
// Overflow
wire xor_co;
xor xor_overflow(xor_co, A[31], B[31]);
// TWOSCOMP64(Y, A);
// input: {HI, LO}
// output: invCplus1;
wire [63:0] invCplus1;
TWOSCOMP64 tc64(invCplus1, prodU64);
// MUX64_2x1(Y, I0, I1, S);
// input for I0: C
// input for I1: invCplus1 
// input for S: xor_co
// output: Csel
wire [63:0] Csel;
MUX64_2x1 mux64_C(Csel, prodU64, invCplus1, xor_co); 
// BUF32(Y, A, B);
BUF32 buf32_Hi(HI, Csel[63:32]);
BUF32 buf32_Lo(LO, Csel[31:0]);
endmodule

// 32-bit unsigned multiplication
module MULT32_U(HI, LO, A, B);
// output list
output [31:0] HI;
output [31:0] LO;
// input list
input [31:0] A; // MCND
input [31:0] B; // MPLR
// wire for 32-bit width vector array 
wire [31:0]  andOut[0:31];
// wire for 1-bit width vector array of 32-bit Adder/Subtractor output
wire cout[0:31];
// wire for 32-bit width vector array of 32-bit Adder/Subtractor output
wire [31:0] sum[0:31];

genvar i;
generate
  for (i = 0; i < 32; i = i + 1) begin: and_loop
    // 32-bit AND gates with 32-bit MCND A, 32-bit repeat of MCPLR bit
    AND32_2x1 andG(andOut[i], A, {32{B[i]}});
    if (i == 0) begin
      assign cout[i] = 1'b0;
      assign sum[i] = andOut[0];
      buf buf_lo(LO[i], sum[i][0]);
      end
    else begin
      AND32_2x1 andG(andOut[i], A, {32{B[i]}});
      // 32-bit adder/subtractor with SnA=0 (i.e. adder)
      RC_ADD_SUB_32 adderi(sum[i], cout[i], andOut[i], {cout[i-1], sum[i-1][31:1]}, 1'b0);
      buf buf_lo(LO[i], sum[i][0]);
      end  
  end // EO-for
endgenerate

genvar j;
generate
  for (j = 0; j < 32; j = j + 1) begin: hi_loop
    buf buf_hi(HI[j], {cout[30], sum[30][31:1]});
  end
endgenerate
endmodule

