/* CS147 - Spring2019 - Inhee Park */
`timescale 1ns/1ps // 1000, 3 digits after decimal 
module barrel_shift_tb;
/*********************/
// Testbench for SHIFT32(Y, D, S, LnR);
// input list
reg [31:0] D;
reg [31:0] S; 
reg LnR;
// output wire
wire [31:0] Y;
// instantiation
BARREL_SHIFTER32 bshifter32(.Y(Y), .D(D), .S(S), .LnR(LnR));
initial
begin
    D = 0; S = 0; LnR=0;
#10 D = 32'b10000000; S = 0; LnR=0;
#10 D = 32'b10000000; S = 1; LnR=0;
#10 D = 32'b10000000; S = 2; LnR=0;
#10 D = 32'b10000000; S = 3; LnR=0;
#10 D = 32'b10000000; S = 4; LnR=0;
#10 D = 32'b10000000; S = 5; LnR=0;
#10 D = 32'b10000000; S = 6; LnR=0;
#10 D = 32'b10000000; S = 7; LnR=0;
#10 D = 32'b10000000; S = 8; LnR=0;
#10 D = 32'b10000000; S = 0; LnR=0;
#10 D = 0; S = 0; LnR=1;
#10 D = 1; S = 0; LnR=1;
#10 D = 1; S = 1; LnR=1;
#10 D = 1; S = 2; LnR=1;
#10 D = 1; S = 3; LnR=1;
#10 D = 1; S = 4; LnR=1;
#10 D = 1; S = 5; LnR=1;
#10 D = 1; S = 6; LnR=1;
#10 D = 1; S = 7; LnR=1;
#10 D = 1; S = 8; LnR=1;
#10 D = 0; S = 0; LnR=1;
end

// Testbench for 32-bit barrel shift
/*
// input list
reg [31:0] D;
reg [4:0] S; 
// output wire
wire [31:0] Y;
// instantiation
SHIFT32_L shift32_left_tb(.Y(Y), .D(D), .S(S));
initial
begin
    D = 0; S = 0;
#10 D = 1; S = 0;
#10 D = 1; S = 1;
#10 D = 1; S = 2;
#10 D = 1; S = 3;
#10 D = 1; S = 4;
#10 D = 1; S = 5;
#10 D = 1; S = 6;
#10 D = 1; S = 7;
#10 D = 1; S = 8;
#10 D = 0; S = 0;
end
*/


// Testbench for 32-bit barrel shift
/*
// input list
reg [31:0] D;
reg [4:0] S; 
// output wire
wire [31:0] Y;
// instantiation
SHIFT32_R shift32_right_tb(.Y(Y), .D(D), .S(S));
initial
begin
    D = 0; S = 0;
#10 D = 32'b10000000; S = 0;
#10 D = 32'b10000000; S = 1;
#10 D = 32'b10000000; S = 2;
#10 D = 32'b10000000; S = 3;
#10 D = 32'b10000000; S = 4;
#10 D = 32'b10000000; S = 5;
#10 D = 32'b10000000; S = 6;
#10 D = 32'b10000000; S = 7;
#10 D = 32'b10000000; S = 8;
#10 D = 32'b10000000; S = 0;
#10 D = 0; S = 0;
end
*/

// Testbench for 4-bit barrel shift 
/*
// input list
reg [3:0] D;
reg [1:0] S; 
// output wire
wire [3:0] Y;
// instantiation
SHIFT4_L shift4_left_tb(.Y(Y), .D(D), .S(S));
initial
begin
    D = 0; S = 0;
#10 D = 1; S = 0;
#10 D = 1; S = 1;
#10 D = 1; S = 2;
#10 D = 1; S = 3;
#10 D = 0; S = 0;
end
*/



// Testbench for 4-bit barrel shift 
/*
// input list
reg [3:0] D;
reg [1:0] S; 
// output wire
wire [3:0] Y;
// instantiation
SHIFT4_R shift4_right_tb(.Y(Y), .D(D), .S(S));
initial
begin
    D = 4'b0; S = 0;
#10 D = 4'b1000; S = 1;
#10 S = 2;
#10 S = 3;
#10 D = 0; S = 0;
end
*/

endmodule
