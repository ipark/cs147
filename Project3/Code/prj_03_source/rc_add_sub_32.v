// Name: rc_add_sub_32.v
// Module: RC_ADD_SUB_32
//
// Output: Y : Output 32-bit
//         CO : Carry Out
//         
//
// Input: A : 32-bit input
//        B : 32-bit input
//        SnA : if SnA=0 it is add, subtraction otherwise
//
// Notes: 32-bit adder / subtractor implementaiton.
// 
//
// Revision History:
//
// Version	Date		Who		email			note
//------------------------------------------------------------------------------------------
//  1.0     Sep 10, 2014	Kaushik Patra	kpatra@sjsu.edu		Initial creation
//------------------------------------------------------------------------------------------
`include "prj_definition.v"

module RC_ADD_SUB_64(Y, CO, A, B, SnA);
/*************************************/
// output list
output [63:0] Y;
output CO;
// input list
input [63:0] A;
input [63:0] B;
input SnA;
// wire for one output of full-adders
wire  cout [63:0];
// wire for xor gate output
wire  bxor[63:0];

genvar i;
generate
  for (i = 0; i < 64; i = i + 1) 
    begin: loop
    // XOR Gates
    xor xorG(bxor[i], B[i], SnA);
    // Full Adders
    if (i == 0) 
      FULL_ADDER FA0(Y[i], cout[i], A[i], bxor[i], SnA);
    else if (i == 63) 
      FULL_ADDER FA1(Y[i], CO, A[i], bxor[i], cout[i-1]);
    else        
      FULL_ADDER FA1(Y[i], cout[i], A[i], bxor[i], cout[i-1]);
    end
endgenerate
endmodule


module RC_ADD_SUB_32(Y, CO, A, B, SnA);
/*************************************/
// output list
output [31:0] Y; // sum
output CO; // carry-out
// input list
input [31:0] A;
input [31:0] B;
input SnA;
// wire for one output of full-adders
wire  cout [31:0];
// wire for xor gate output
wire  bxor[31:0];

genvar i;
generate
  for (i = 0; i < 32; i = i + 1) 
    begin: loop
    // XOR Gates
    xor xorG(bxor[i], B[i], SnA);
    // Full Adders
    if (i == 0) 
      FULL_ADDER FA0(Y[i], cout[i], A[i], bxor[i], SnA);
    else if (i == 31) 
      FULL_ADDER FA31(Y[i], CO, A[i], bxor[i], cout[i-1]);
    else        
      FULL_ADDER FA1(Y[i], cout[i], A[i], bxor[i], cout[i-1]);
    end
endgenerate
endmodule
