MAX_BIT=32
#
for x in range(MAX_BIT):
    if (x==31):
        print("buf b1_G%d_h(HI[%d], cout30);" % (x, x))
    else:
        print("buf b1_G%d_h(HI[%d], sum30[%d]);" % (x, x, x+1))
"""
for x in range(MAX_BIT):
    if (x==0):
        print("buf b1_G0(LO[0], and0[0]");
    else:
        print("buf b1_G%d(LO[%d], sum%d[0]);" % (x, x, x-1))
"""

"""
for x in range(MAX_BIT):
    if (x == 0):
        print("assign LO[%d] = out0[0];" % (x));
    else:
        print("assign LO[%d] = sum%d[0];" % (x, x-1));
"""
"""
print("wire [31:0] "),
for x in range(MAX_BIT):
    if (x==MAX_BIT-1):
        print("and"+str(x)),   
    else:
        print("and"+str(x)+","),   
print(";")
#
print("wire [31:0] "),
for x in range(MAX_BIT):
    if (x==MAX_BIT-1):
        print("cout"+str(x)),   
    else:
        print("cout"+str(x)+","),   
print(";")
#
print("wire [31:0] "),
for x in range(MAX_BIT):
    if (x==MAX_BIT-1):
        print("sum"+str(x)),   
    else:
        print("sum"+str(x)+","),   
print(";")
#
for x in range(MAX_BIT):
    print("AND32_2x1 and_G%d(and%d, A, {32{B[%d]}});" % (x, x, x));
#
for x in range(MAX_BIT-1):
    if (x==0): # x=0
        print("RC_ADD_SUB_32 adder%d(sum0, cout0, and0, and1, 1'b0);" % (x));
    else:
        print("RC_ADD_SUB_32 adder%d(sum%d, cout%d, and%d, {cout%d, sum%d[31:1]}, 1'b0);"
                % (x, x, x, x+1, x-1, x-1));
"""

"""
for x in range(MAX_BIT):
    if (x==MAX_BIT-1):
        print("cout"+str(x)),   
    else:
        print("cout"+str(x)+","),   
print(";")
#
print("wire "),
for x in range(MAX_BIT):
    if (x==MAX_BIT-1):
        print("b"+str(x)),   
    else:
        print("b"+str(x)+","),   
print(";")
#
for x in range(MAX_BIT):
    print("xor xor_G%d(b%d, B[%d], SnA);" % (x, x, x));
#
for x in range(MAX_BIT):
    if (x==MAX_BIT-1):
        print("FULL_ADDER FA%d(Y[%d], CO, A[%d], b%d, cout%d);" % (x, x, x, x, x-1));
    elif (x==0):
        print("FULL_ADDER FA%d(Y[%d], cout%d, A[%d], b%d, SnA);" % (x, x, x, x, x));
    else:
        print("FULL_ADDER FA%d(Y[%d], cout%d, A[%d], b%d, cout%d);" % (x, x, x, x, x, x-1));
"""
