/* CS147 - Spring2019 - Inhee Park */
`include "prj_definition.v"
`timescale 1ns/1ps // 1000, 3 digits after decimal 
module data_path_tb;
/*******************/

/* Testbench for data_path */
//DATA_PATH(DATA_OUT, ADDR, ZERO, INSTRUCTION, DATA_IN, CTRL, CLK, RST);
// output list
wire [`ADDRESS_INDEX_LIMIT:0] ADDR;
wire ZERO;
wire [`DATA_INDEX_LIMIT:0] DATA_OUT, INSTRUCTION;
// input list
reg [`CTRL_WIDTH_INDEX_LIMIT:0]  CTRL;
reg CLK, RST;
reg [`DATA_INDEX_LIMIT:0] DATA_IN;
//
DATA_PATH dataFlow(
  .DATA_OUT(DATA_OUT), //output
  .ADDR(ADDR), //output
  .ZERO(ZERO), //output
  .INSTRUCTION(INSTRUCTION), //output
  .DATA_IN(DATA_IN), //input
  .CTRL(CTRL), //input
  .CLK(CLK), //input
  .RST(RST) //input
);
initial
begin
CLK=1'b1; RST=1'b0; 
#20 DATA_IN='h20420001; // addi r[2], r[2], 0x0001;
    CTRL=32'b00000000000000000100100000000000;
#20 RST=1'b1; DATA_IN='h20420001; // addi r[2], r[2], 0x0001;
    CTRL=32'b00000000000000000100100000000000;
$stop;
end

always
begin
  #10 CLK = ~CLK;
end

/////////
endmodule
