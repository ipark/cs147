module REGISTER_FILE_32x32(DATA_R1, DATA_R2, ADDR_R1, ADDR_R2, 
                            DATA_W, ADDR_W, READ, WRITE, CLK, RST);
// 5x32 Line decoder:  DECODER_5x32(D, I)
// input  : [4:0] ADDR_W // output : [31:0] D =>
wire [31:0] D;
DECODER_5x32 ld_5x32(D, ADDR_W);

// 32 instantiations of AND gate
wire [31:0] andOut;
genvar i;
generate
for (i = 0; i < 32; i = i + 1) begin: loop_andi
  and and_i(andOut[i], D[i], WRITE);
end
endgenerate

// 32 instantiations of REG32(Q, D, LOAD, CLK, RESET)
// input: [31:0] DATA_W, L (from AND gates)
// input: CLK, RST // output: array of [31:0] Q => 
wire [31:0] Q[0:31];
genvar j;
generate
for (j = 0; j < 32; j = j + 1) begin: loop_reg32
  REG32 reg32_j(Q[j], DATA_W, andOut[j], CLK, RST);
end
endgenerate

// 32-bit mux_32x1 : MUX32_32x1(Y, I0,..., I31, S)
// input  : [31:0] Y, I0, I1, ..., I31
// input  : [4:0] S // output : [31:0] Y =>
wire [31:0] Y1, Y2;
MUX32_32x1 mux32_G1(Y1, Q[0], Q[1], Q[2], Q[3], Q[4], Q[5], 
Q[6], Q[7], Q[8], Q[9], Q[10], Q[11], Q[12], Q[13], Q[14], 
Q[15], Q[16], Q[17], Q[18], Q[19], Q[20], Q[21], Q[22], Q[23],
Q[24], Q[25], Q[26], Q[27], Q[28], Q[29], Q[30], Q[31], ADDR_R1);

MUX32_32x1 mux32_G2(Y2, Q[0], Q[1], Q[2], Q[3], Q[4], Q[5], 
Q[6], Q[7], Q[8], Q[9], Q[10], Q[11], Q[12], Q[13], Q[14], 
Q[15], Q[16], Q[17], Q[18], Q[19], Q[20], Q[21], Q[22], Q[23],
Q[24], Q[25], Q[26], Q[27], Q[28], Q[29], Q[30], Q[31], ADDR_R2);

// 32-bit mux_2x1 : MUX32_2x1(Y, I0, I1, S)
// input  : [31:0] Y, I0, I1
// input  : [4:0] S // output : [31:0] Y =>
reg [31:0] I0, I1;
reg S;
wire [31:0] Y;
// MUX32_2x1(Y, I0, I1, S);
MUX32_2x1 mux32_r1(DATA_R1, 32'bz, Y1, READ);
MUX32_2x1 mux32_r2(DATA_R2, 32'bz, Y2, READ);
endmodule
