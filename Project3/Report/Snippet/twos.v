// 64-bit two's complement
module TWOSCOMP64(Y, A);
/**********************/
//output list
output [63:0] Y;
//input list
input [63:0] A;
// wire for outputs
wire [63:0] invA;
wire CarryOut;
// INV32_1x1(Y, A);
INV64_1x1 inv64(invA, A);
//RC_ADD_SUB_64(Y, CO, A, B, SnA);
RC_ADD_SUB_64 adder64(Y, CarryOut, invA, 64'b1, 1'b0); // SnA=0 (+)
endmodule

// 32-bit two's complement
module TWOSCOMP32(Y, A);
/**********************/
//output list
output [31:0] Y;
//input list
input [31:0] A;
// wire for outputs
wire [31:0] invA, invAplus1;
wire CarryOut;
// INV32_1x1(Y, A);
INV32_1x1 inv32(invA, A);
//RC_ADD_SUB_32(Y, CO, A, B, SnA);
RC_ADD_SUB_32 adder32(Y, CarryOut, invA, 32'b1, 1'b0); // SnA=0 (+)
endmodule
