module DATA_PATH(DATA_OUT, ADDR, ZERO, INSTRUCTION, DATA_IN, CTRL, CLK, RST);

/** OMIT **/

//  CONTROL_UNIT:INSTRUCTION(input)<---DATA_PATH:INSTRUCTION(output)
BUF32 inst32(INSTRUCTION, DATA_IN);
wire [31:0] INST;

// 1. REG32_PP 
// CTRL[0] pc_load; 
wire [31:0] pc_load_32out;
wire [31:0] pc_sel_3_32out;
defparam pc_load.PATTERN = `INST_START_ADDR; //PATTERN = 32'h00000000;
REG32_PP pc_load(
  .Q(pc_load_32out), //output [31:0] Q;
  .D(pc_sel_3_32out), //input [31:0] D;
  .LOAD(CTRL[0]), 
  .CLK(CLK), 
  .RESET(RST)
);

// 2. RC_ADD_SUB32 (adder1)
wire [31:0] adder1_32out;
assign notUsed=1'bz;
RC_ADD_SUB_32 adder1(
  .Y(adder1_32out), //output [31:0] Y; // sum
  .CO(notUsed), //output CO; // carry-out
  .A(32'b1),  //input [31:0] A;
  .B(pc_load_32out), //input [31:0] B;
  .SnA(1'b0) // SnA=0 (addition)
);

// 3. MUX32_2x1(Y, I0, I1, S); 
// CTRL[1] pc_sel_1;
wire [31:0] pc_sel_1_32out;
wire [31:0] R1_data_32out;
MUX32_2x1 pc_sel_1(
  .Y(pc_sel_1_32out), //output [31:0] Y; 
  .I0(R1_data_32out), //input [31:0] I0;
  .I1(adder1_32out), //input [31:0] I1; 
  .S(CTRL[1]) // input S;
);

// 4. RC_ADD_SUB_32 (adder2)
wire [31:0] adder2_32out;
RC_ADD_SUB_32 adder2(
  .Y(adder2_32out), //output [31:0] Y; // sum
  .CO(notUsed), //output CO; // carry-out
  .A(adder1_32out),  //input [31:0] A;
  .B({ {16{INST[15]}}, INST[15:0] }), //16-bit sign extender of imm
  .SnA(1'b0) // SnA=0 (addition)
);

// 5. MUX32_2x1(Y, I0, I1, S);
// CTRL[2] pc_sel_2; 
wire [31:0] pc_sel_2_32out; // CTRL[2]
MUX32_2x1 pc_sel_2(
  .Y(pc_sel_2_32out), //output [31:0] Y; 
  .I0(pc_sel_1_32out), //input [31:0] I0;
  .I1(adder2_32out), //input [31:0] I1; 
  .S(CTRL[2]) // input S;
);

// 6. MUX32_2x1(Y, I0, I1, S);
// CTRL[3] pc_sel_3;
//wire [31:0] pc_sel_3_32out;
MUX32_2x1 pc_sel_3(
  .Y(pc_sel_3_32out), //output [31:0] Y; 
  .I0({{6{1'b0}}, INST[25:0]}), //input [31:0] I0;
  .I1(pc_sel_2_32out), //input [31:0] I1; 
  .S(CTRL[3]) // input S;
);

// 7. REG32(Q, D, LOAD, CLK, RESET);
// CTRL[4] ir_load
//wire [31:0] INST;
wire [31:0] wd_sel_1_32out; // CTRL[23]
REG32 ir_load(
  .Q(INST),//output [31:0] Q;
  .D(DATA_IN), //input [31:0] D;
  .LOAD(CTRL[4]), //input LOAD;
  .CLK(CLK), //input CLK;
  .RESET(RST) //input RESET;
);

// 8. MUX5_2x1(Y, I0, I1, S);
// CTRL[5] r1_sel_1
wire [4:0] r1_sel_1_5out; 
MUX5_2x1 r1_sel_1(
  .Y(r1_sel_1_5out), //output [4:0] Y;
  .I0(INST[25:21]), //input [4:0] I0; // rs
  .I1(5'b0), //input [4:0] I1;
  .S(CTRL[5]) //input S;
);

// 9. REGISTER_FILE_32x32
// CTRL[6] reg_r
// CTRL[7] reg_w
wire [31:0] R2_data_32out;
wire [31:0] out; // for CTRL[25]
wire [4:0] wa_sel_3_5out; // for CTRL[28]
REGISTER_FILE_32x32 reg_rw(
  .DATA_R1(R1_data_32out), //output [31:0] DATA_R1;
  .DATA_R2(R2_data_32out), //output [31:0] DATA_R2;
  .ADDR_R1(r1_sel_1_5out), //input [4:0] ADDR_R1;
  .ADDR_R2(INST[20:16]), //input [4:0] ADDR_R2;  rt
  .DATA_W(out), //input [31:0] DATA_W;
  .ADDR_W(wa_sel_3_5out), //input [4:0] ADDR_W;
  .READ(CTRL[6]),  // reg_r
  .WRITE(CTRL[7]),  // reg_w
  .CLK(CLK), 
  .RST(RST)
);

// 10.  REG32_PP (Stack Pointer)
// CTRL[8] sp_load
wire [31:0] sp_load_32out;
wire [31:0] alu_32out; // CTRL[19:14]
defparam sp_load.PATTERN = `INIT_STACK_POINTER;
REG32_PP sp_load(
  .Q(sp_load_32out), //output [31:0] Q;
  .D(alu_32out), //input [31:0] D;
  .LOAD(CTRL[8]), 
  .CLK(CLK), 
  .RESET(RST));

// 11. MUX32_32x1
// CTRL[9] op1_sel_1
wire [31:0] op1_sel_1_32out;
MUX32_2x1 op1_sel_1(
  .Y(op1_sel_1_32out), //output [31:0] Y; 
  .I0(R1_data_32out), //input [31:0] I0;
  .I1(sp_load_32out), //input [31:0] I1; 
  .S(CTRL[9]) // input S;
);

// 12. MUX32_32x1
// CTRL[10] op2_sel_1
wire [31:0] op2_sel_1_32out;
MUX32_2x1 op2_sel_1(
  .Y(op2_sel_1_32out), //output [31:0] Y; 
  .I0(32'b1), //input [31:0] I0;
  .I1({ {27{1'b0}}, INST[10:6] }), // 27-bit zero extender
  .S(CTRL[10]) // input S;
);

// 13. MUX32_32x1
// CTRL[11] op2_sel_2
wire [31:0] op2_sel_2_32out;
MUX32_2x1 op2_sel_2(
  .Y(op2_sel_2_32out), //output [31:0] Y; 
  .I0({ {16{1'b0}}, INST[15:0] }), // 16-bit zero extender
  .I1({ {16{INST[15]}}, INST[15:0] }), // 16-bit sign extender
  .S(CTRL[11]) // input S;
);

/** OMIT **/

// 16. ALU 
// CTRL[14] alu_oprn[0]
// CTRL[15] alu_oprn[1]
// CTRL[16] alu_oprn[2]
// CTRL[17] alu_oprn[3]
// CTRL[18] alu_oprn[4]
// CTRL[19] alu_oprn[5]
//wire [31:0] alu_32out;
wire zero_1out;
ALU alu_oprn_5bit(
  .OUT(alu_32out), //output [31:0] OUT; // result of the operation
  .ZERO(zero_1out), //output ZERO;
  .OP1(op1_sel_1_32out), //input [31:0] OP1; // operand 1
  .OP2(op2_sel_4_32out), //input [31:0] OP2; // operand 2
  .OPRN(CTRL[19:14]) //input [5:0] OPRN; // operation code
);

// 17. MUX32_32x1
// CTRL[20] ma_sel_1
wire [31:0] ma_sel_1_32out;
MUX32_2x1 ma_sel_1(
  .Y(ma_sel_1_32out), //output [31:0] Y; 
  .I0(alu_32out), //input [31:0] I0;
  .I1(sp_load_32out), //input [31:0] I1; 
  .S(CTRL[20]) // input S;
);

// 18. MUX32_32x1
// CTRL[21] ma_sel_2
wire [31:0] ma_sel_2_32out; // [25:0] portion will be output ADDR
MUX32_2x1 ma_sel_2(
  .Y(ma_sel_2_32out), //output [31:0] Y; 
  .I0(ma_sel_1_32out), //input [31:0] I0;
  .I1(pc_load_32out), //input [31:0] I1; 
  .S(CTRL[21]) // input S;
);
BUF26 addr_out(ADDR, ma_sel_1_32out[25:0]);

/** OMIT **/

// 24. MUX5_2x1(Y, I0, I1, S);
// CTRL[27] wa_sel_2
wire [4:0] wa_sel_2_5out;
MUX5_2x1 wa_sel_2(
  .Y(wa_sel_2_5out), //output [4:0] Y;
  .I0(5'b0), //input [4:0] I0; // 0
  .I1(5'b11111), //input [4:0] I1; // 31
  .S(CTRL[27]) //input S;
);

// 25. MUX5_2x1(Y, I0, I1, S);
// CTRL[28] wa_sel_3
//wire [4:0] wa_sel_3_5out;
MUX5_2x1 wa_sel_3(
  .Y(wa_sel_3_5out), //output [4:0] Y;
  .I0(wa_sel_2_5out), //input [4:0] I0; // rd
  .I1(wa_sel_1_5out), //input [4:0] I1; // rt
  .S(CTRL[28]) //input S;
);
// CTRL[29] N/A
// CTRL[30] N/A
// CTRL[31] N/A

endmodule
