// 32-bit shift amount shifter
module SHIFT32(Y, D, S, LnR);
/***************************/
// output list
output [31:0] Y;
// input list
input [31:0] D;
/* 32-bit inside ALU */
input [31:0] S; 
input LnR;
/* wire for output */
wire [31:0] bsOut;
// BARREL_SHIFTER32(Y, D, S, LnR);
BARREL_SHIFTER32 bshifter32(bsOut, D, S[4:0], LnR); 
// OR gate for S[31:5] bits
// 1-bit output from 27-bit OR
// OR1_27bit(orOut, S);
wire orOut;
OR1_27bit or27(orOut, S[31:5]);

/* 32-bit ;2x1 mux */
// MUX32_2x1(Y, I0, I1, S);
//output [31:0] Y;
//input [31:0] I0;
//input [31:0] I1;
//input S;
MUX32_2x1 mux32(Y, bsOut, {32{1'b0}}, orOut);
endmodule


// Shift with control L or R shift
module BARREL_SHIFTER32(Y, D, S, LnR);
/***********************************/
// output list
output [31:0] Y;
// input list
input [31:0] D;
/* 5-bit */
input [4:0] S; 
input LnR;
/* wire for output */
wire [31:0] slOut, srOut;
/* right shifter */
SHIFT32_R sr32(srOut, D, S);
/* left shifter */
SHIFT32_L sl32(slOut, D, S); 
/* 32-bit 2x1 mux */
// MUX32_2x1(Y, I0, I1, S);
//output [31:0] Y;
//input [31:0] I0;
//input [31:0] I1;
//input S;
MUX32_2x1 mux32(Y, srOut, slOut, LnR); 
endmodule

