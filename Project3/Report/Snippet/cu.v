module CONTROL_UNIT(CTRL, READ, WRITE, ZERO, INSTRUCTION, CLK, RST); 
// registers for output
reg [`CTRL_WIDTH_INDEX_LIMIT:0]  CTRL;
reg READ, WRITE;
reg [`DATA_INDEX_LIMIT:0] INST_REG;
always @ (proc_state) begin // proc_state
case(proc_state)  // proc_state
//////////////////////////////////
`PROC_FETCH: begin  // PROF_FETCH
  READ = 1'b1; WRITE = 1'b0; 
  CTRL=32'b00000000001000000000000000000000; // CTRL[21] ma_sel_2=1
end  // PROF_FETCH
//////////////////////////////////
`PROC_DECODE: begin // PROC_DECODE
  // hold for previous bits
  READ = 1'b0; WRITE = 1'b0; 
  // Store the memory read data into INST_REG; 
  INST_REG =  INSTRUCTION;
  CTRL=32'b00000000000000000000000001010000; // CTRL[6]reg_r=1; CTRL[4]ir_load=1
end  // PROC_DECODE
//////////////////////////////////
`PROC_EXE: begin // PROC_EXE
// hold for previous bits
READ = 1'b0; WRITE = 1'b0; 
case(INST_REG[31:26]) // opcode
// R-Type ///////////////////
6'h00: begin  // R-type
  case(INST_REG[5:0])
  6'h20: begin //R1)  6'h20: add : R[rd]=R[rs]+R[rt] 
    CTRL=32'b00000000000000000110000000000000;
    //1; CTRL[13]op2_sel1_4=1; alu_oprn[19:14]=000001 end
  /** OMIT **/
end // R-type
// I-Type //////////////////////////////////////
6'h08: begin//I1)  6'h08 : addi: R[rt]=R[rs]+SignExtImm              
  CTRL=32'b00000000000000000100100000000000;
  //CTRL[11]op2_sel_2=1; alu_oprn[19:14]=000001 end
  /** OMIT **/
// J-Type //////////////////////////////////////
6'h02: begin//J1)  6'h02: jmp : PC=JumpAddr={6'b0, address}
  CTRL=32'b00000000000000000000000000000000; end
  /** OMIT **/
endcase // case(INST_REG[31:26]) // opcode
end // PROC_EXE
//////////////////////////////////
`PROC_MEM: begin  // PROC_MEM
//Only 'lw', 'sw', 'push' and 'pop' instructions are involved in this stage. 
READ = 1'b0; WRITE = 1'b0;  // memory output to HighZ (hold)
case(INST_REG[31:26]) // opcode
6'h23: begin //I9)  6'h23 : lw  : R[rt]=M[R[rs]+SignExtImm] 
  READ=1'b0; WRITE=1'b1;
  CTRL=32'b00000000000000000100100000000000; end
  /** OMIT **/
endcase // case(INST_REG[31:26]) // opcode
end // PROC_MEM
//////////////////////////////////
`PROC_WB: begin // PROC_WB
READ = 1'b0; WRITE = 1'b0;  // no-op {00} 
case(INST_REG[31:26]) // opcode
  /** OMIT **/
endcase // case(INST_REG[31:26]) // opcode
end // PROC_WB
////////////////////////
endcase // (proc_state)
end // always @ (proc_state)
endmodule
