/* CS147 - Spring2019 - Inhee Park  
   Name: full_adder.v
   Module: FULL_ADDER
   Output: S : Sum; CO : Carry Out
   Input: A : Bit 1; B : Bit 2; CI : Carry In
   Notes: 1-bit full adder implementaiton.
*/
`include "prj_definition.v"

module FULL_ADDER(S, CO, A, B, CI);
output S, CO; // sum, carry-out 
input A,B, CI; // two input bits and carry-out

wire sum1, cout1; // outputs of half-adder1;
wire cout2; // one of outputs of half-adder2; 

// instantiation of two half-adders HALF_ADDER(Y,C,A,B);
HALF_ADDER ha1(sum1, cout1, A, B);
HALF_ADDER ha2(S, cout2, sum1, CI); 
or or_gate(CO, cout1, cout2);
endmodule;

