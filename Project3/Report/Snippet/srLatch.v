// 1 bit SR latch
// Preset on nP=0, nR=1, reset on nP=1, nR=0;
// Undefined nP=0, nR=0
// normal operation nP=1, nR=1
module SR_LATCH(Q, Qbar, S, R, C, nP, nR);
/****************************************/
input S, R, C;
input nP, nR;
output Q, Qbar;
/* wire for output */
wire Sbar, Rbar;
/* 2-input NAND gates */
nand nand_G0(Sbar, S, C);
nand nand_G1(Rbar, R, C);
/* 3-input NAND gates with either preset(nP) or reset(nR) signals */
nand nand_G2(Q,    Qbar, Sbar, nP);
nand nand_G3(Qbar, Q,    Rbar, nR);
endmodule
