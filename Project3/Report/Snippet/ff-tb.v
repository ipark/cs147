`timescale 1ns/1ps // 1000, 3 digits after decimal 
module logic_tb;
/* Testbench for +VE 1-bit Flip Flop */
// Preset on nP=0, nR=1, reset on nP=1, nR=0;
// Undefined nP=0, nR=0
// normal operation nP=1, nR=1
reg D, C;
reg nP, nR;
wire Q, Qbar;
D_FF pdff_tb(.Q(Q), .Qbar(Qbar), .D(D), .C(C), .nP(nP), .nR(nR));

initial
begin
$monitor($time, "ns   C=%b, D=%b, Q=%b, Qbar=%b", C, D, Q, Qbar);
nP=1; nR=1; C=0; D=0; // normal operation
#3 D = 1; 
#1 D = 0; 
#5 D = 1; 
#7 D = 0; 
#2 D = 1; 
#8 D = 0; 
$stop; 
end

always // toggle C every 5ns
begin
  #5 C = ~C;
end
