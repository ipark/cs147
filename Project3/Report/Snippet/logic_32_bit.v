// Name: logic_32_bit.v
// Module: 
// Input: 
// Output: 
//
// Notes: Common definitions
// 
//
// Revision History:
//
// Version	Date		Who		email			note
//------------------------------------------------------------------------------------------
//  1.0     Sep 02, 2014	Kaushik Patra	kpatra@sjsu.edu		Initial creation
//------------------------------------------------------------------------------------------
//

// 32-bit NOR
module NOR32_2x1(Y,A,B);
/**********************/
//output 
output [31:0] Y;
//input
input [31:0] A;
input [31:0] B;
// generate block
genvar i;
generate
  for (i = 0; i < 32; i = i + 1)
  begin: nor32_gen_loop
    nor nor_inst(Y[i], A[i], B[i]);
  end
endgenerate
endmodule

// 32-bit AND
module AND32_2x1(Y,A,B);
/**********************/
//output 
output [31:0] Y;
//input
input [31:0] A;
input [31:0] B;
// generate block
genvar i;
generate
  for (i = 0; i < 32; i = i + 1)
  begin: and32_gen_loop
    and and_inst(Y[i], A[i], B[i]);
  end
endgenerate
endmodule


// 32-bit inverter
module INV32_1x1(Y, A);
/*********************/
//output 
output [31:0] Y;
//input
input [31:0] A;
// generate block
genvar i;
generate
  for (i = 0; i < 32; i = i + 1)
  begin: inv32_gen_loop
    not inv_inst(Y[i], A[i]);
  end
endgenerate
endmodule


// 64-bit inverter
module INV64_1x1(Y, A);
/*********************/
//output 
output [63:0] Y;
//input
input [63:0] A;
// generate block
genvar i;
generate
  for (i = 0; i < 64; i = i + 1)
  begin: inv64_gen_loop
    not inv_inst(Y[i], A[i]);
  end
endgenerate
endmodule


// 32-bit OR
module OR32_2x1(Y,A,B);
/*********************/
//output 
output [31:0] Y;
//input
input [31:0] A;
input [31:0] B;
// generate block
genvar i;
generate
  for (i = 0; i < 32; i = i + 1)
  begin: or32_gen_loop
    or or_inst(Y[i], A[i], B[i]);
  end
endgenerate
endmodule


// 32-bit BUF
module BUF32(Y, A);
/*********************/
//output 
output [31:0] Y;
//input
input [31:0] A;
// generate block
genvar i;
generate
  for (i = 0; i < 32; i = i + 1)
  begin: buf32_gen_loop
    buf buf_inst(Y[i], A[i]);
  end
endgenerate
endmodule


// 26-bit BUF
module BUF26(Y, A);
/*********************/
//output 
output [25:0] Y;
//input
input [25:0] A;
// generate block
genvar i;
generate
  for (i = 0; i < 26; i = i + 1)
  begin: buf26_gen_loop
    buf buf_inst(Y[i], A[i]);
  end
endgenerate
endmodule


// 32-bit BUFIF1
module BUFIF1_32(Y, A, Enable);
/*****************************/
//output 
output [31:0] Y;
//input
input [31:0] A;
input Enable;
// generate block
genvar i;
generate
  for (i = 0; i < 32; i = i + 1)
  begin: bufif1_32_gen_loop
    bufif1 bufif1_inst(Y[i], A[i], Enable);
  end
endgenerate
endmodule

