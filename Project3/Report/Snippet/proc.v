module PROC_CS147_SEC05(DATA_OUT, ADDR, DATA_IN, READ, WRITE, CLK, RST);

/** OMIT **/

// instantiation section
// Control unit
CONTROL_UNIT cu_inst (
  .CTRL(ctrl), 
  .READ(READ), 
  .WRITE(WRITE), 
  .ZERO(zero), 
  .INSTRUCTION(INSTRUCTION), // input (wired from DATA_PATH)
  .CLK(CLK),   
  .RST(RST)
);

// data path
DATA_PATH    data_path_inst (
  .DATA_OUT(DATA_OUT),  
  .INSTRUCTION(INSTRUCTION), // output (wired to CONTROL_UNIT)
  .DATA_IN(DATA_IN), 
  .ADDR(ADDR), 
  .ZERO(zero),
  .CTRL(ctrl),  
  .CLK(CLK),   
  .RST(RST)
);
