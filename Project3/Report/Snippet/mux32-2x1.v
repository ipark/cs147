// 32-bit mux
module MUX32_2x1(Y, I0, I1, S);
////////////////////////////////
// output list
output [31:0] Y;
//input list
input [31:0] I0;
input [31:0] I1;
input S;
// compact for-loop form
genvar i;
generate
  for (i = 0; i < 32; i = i + 1)
  begin: mux1_2x1_gen_loop
    MUX1_2x1 mux1(Y[i], I0[i], I1[i], S);
  end
endgenerate
endmodule
