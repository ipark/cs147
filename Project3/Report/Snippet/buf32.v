// 32-bit BUF
module BUF32(Y, A);
/*********************/
//output 
output [31:0] Y;
//input
input [31:0] A;
// generate block
genvar i;
generate
  for (i = 0; i < 32; i = i + 1)
  begin: buf32_gen_loop
    buf buf_inst(Y[i], A[i]);
  end
endgenerate
endmodule

