// 1-bit mux
module MUX1_2x1(Y,I0, I1, S);
///////////////////////////////////////
//output list
output Y;
//input list
input I0, I1, S;
/* internal wire declarations */
wire Sn; // ~S
wire ISn, IS; // output of AND gates
/* create ~S signal */
not (Sn, S);
/* AND gates */
and and_G0(IS, I0, Sn);
and and_G1(ISn, I1, S);
/* OR gate */
or or_G(Y, IS, ISn);
endmodule
