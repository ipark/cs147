// 32-bit register +ve edge, Reset on RESET=0
module REG32(Q, D, LOAD, CLK, RESET);
/***********************************/
output [31:0] Q;
input CLK, LOAD;
input [31:0] D;
input RESET;
genvar i;
generate
  for (i = 0; i < 32; i = i + 1) begin: loop_reg1
    /* instantiation of REG1 with preset=1, Qbar=hiZ */
    wire Qbar;
    assign Qbar=32'bz;
    REG1 reg1_i(Q[i], Qbar, D[i], LOAD, CLK, 1'b1, RESET);
  end
endgenerate
endmodule

// 1 bit register +ve edge, 
// Preset on nP=0, nR=1, reset on nP=1, nR=0;
// Undefined nP=0, nR=0
// normal operation nP=1, nR=1
module REG1(Q, Qbar, D, L, C, nP, nR);
/************************************/
input D, C, L;
input nP, nR;
output Q, Qbar;
/* wire for output */
wire Dsel;
/* instantiation of 1-bit mux_2x1 */
MUX1_2x1 mux1(Dsel, Q, D, L);
/* instantiation of D-flipflop */
D_FF dff(Q, Qbar, Dsel, C, nP, nR);
endmodule
