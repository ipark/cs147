// 1 bit D latch
// Preset on nP=0, nR=1, reset on nP=1, nR=0;
// Undefined nP=0, nR=0
// normal operation nP=1, nR=1
module D_LATCH(Q, Qbar, D, C, nP, nR);
/************************************/
input D, C;
input nP, nR;
output Q,Qbar;
/* wire for output */
wire Dbar, nout0, nout1; 
/* inverter of D */
not not_G(Dbar, D);
/* 2-input NAND gates */
nand nand2_G0(nout0, D, C);
nand nand2_G1(nout1, Dbar, C);
/* 3-input NAND gates with either preset(nP) or reset(nR) signals */
nand nand3_G2(Q, Qbar, nout0, nP);
nand nand3_G3(Qbar, Q, nout1, nR);
endmodule
