module ALU(OUT, ZERO, OP1, OP2, OPRN);

/** OMIT **/

// wire for outputs
wire SnA, LnR, OPRN0n, OPRN3andOPRN0, orOut;
not not_G(OPRN0n, OPRN[0]); // OPRN[0]'
and and_G(OPRN3andOPRN0, OPRN[3], OPRN[0]);
or or_G(orOut, OPRN0n, OPRN3andOPRN0); 
buf buf_G1(SnA, orOut); // SnA
buf buf_G2(LnR, OPRN[0]); // LnR

// OPRN_CODE = xx00001 (add) SnA=OPRN[0]'+OPRN[3].OPRN[0]
//  RC_ADD_SUB_32(Y, CO, A, B, SnA); 
wire [31:0] Y_add;  // => I1
wire CO_add;
RC_ADD_SUB_32 add32(Y_add, CO_add, OP1, OP2, SnA);

// OPRN_CODE = xx00010 (sub) SnA=OPRN[0]'+OPRN[3].OPRN[0]
wire [31:0] Y_sub;  // => I2
wire CO_sub;
RC_ADD_SUB_32 sub32(Y_sub, CO_sub, OP1, OP2, SnA);

// OPRN_CODE = xx00011 (mul)
wire [31:0] Y_multL; // 
wire [31:0] Y_multH; // => I3; 
//output [31:0] HI, LO; //input [31:0] A, B;
MULT32 mul32(Y_multH, Y_multL, OP1, OP2);

// OPRN_CODE = xx00100 (shtR) LnR=OPRN[0]
wire [31:0] Y_shtR; // => I4; 
//output [31:0] Y; //input [31:0] D; //input [31:0] S; //input LnR;
SHIFT32 sr32(Y_shtR, OP1, OP2, 1'b0);

// OPRN_CODE = xx00101 (shtL) LnR=OPRN[0]
wire [31:0] Y_shtL; // => I5; 
SHIFT32 sl32(Y_shtL, OP1, OP2, 1'b1);

// OPRN_CODE = xx00110 (and)
wire [31:0] Y_and; // => I6; 
// AND32_2x1(Y,A,B);
AND32_2x1 and32(Y_and, OP1, OP2);

// OPRN_CODE = xx00111 (or )
wire [31:0] Y_or; // => I7; 
OR32_2x1 or32(Y_or, OP1, OP2);

// OPRN_CODE = xx01000 (nor)
wire [31:0] Y_nor; // => I8; 
NOR32_2x1 nor32(Y_nor, OP1, OP2);

// OPRN_CODE = xx01001 (slt) SnA=OPRN[0]'+OPRN[3].OPRN[0]
wire [31:0] Y_slt; // => I9; 
wire CO_slt;
RC_ADD_SUB_32 slt32(Y_slt, CO_slt, OP1, OP2, SnA);

wire [31:0] mSel32;
MUX32_16x1 mux32_alu(mSel32, 
  Y_add, Y_add, Y_sub, Y_multL, Y_shtR, Y_shtL, Y_and, Y_or, 
  Y_nor, {{31{1'b0}}, {Y_slt[31]}}, Y_add, Y_add, Y_add, Y_add, Y_add, Y_add, 
  OPRN[3:0]);

// output Zout; // input [31:0] A;
XOR1_32x0 xor1_G(ZERO, mSel32);
BUF32 b32(OUT, mSel32);

endmodule
