// Name: logic.v
// Module: 
// Input: 
// Output: 
//
// Notes: Common definitions
// 
//
// Revision History:
//
// Version	Date		Who		email			note
//------------------------------------------------------------------------------------------
//  1.0     Sep 02, 2014	Kaushik Patra	kpatra@sjsu.edu		Initial creation
//------------------------------------------------------------------------------------------
//
// 64-bit two's complement
module TWOSCOMP64(Y, A);
/**********************/
//output list
output [63:0] Y;
//input list
input [63:0] A;
// wire for outputs
wire [63:0] invA;
wire CarryOut;
// INV32_1x1(Y, A);
INV64_1x1 inv64(invA, A);
//RC_ADD_SUB_64(Y, CO, A, B, SnA);
RC_ADD_SUB_64 adder64(Y, CarryOut, invA, 64'b1, 1'b0); // SnA=0 (+)
endmodule

// 32-bit two's complement
module TWOSCOMP32(Y, A);
/**********************/
//output list
output [31:0] Y;
//input list
input [31:0] A;
// wire for outputs
wire [31:0] invA, invAplus1;
wire CarryOut;
// INV32_1x1(Y, A);
INV32_1x1 inv32(invA, A);
//RC_ADD_SUB_32(Y, CO, A, B, SnA);
RC_ADD_SUB_32 adder32(Y, CarryOut, invA, 32'b1, 1'b0); // SnA=0 (+)
endmodule

// 32-bit register +ve edge, Reset on RESET=0
module REG32(Q, D, LOAD, CLK, RESET);
/***********************************/
output [31:0] Q;
input CLK, LOAD;
input [31:0] D;
input RESET;
genvar i;
generate
  for (i = 0; i < 32; i = i + 1) begin: loop_reg1
    /* instantiation of REG1 with preset=1, Qbar=hiZ */
    wire Qbar;
    assign Qbar=32'bz;
    REG1 reg1_i(Q[i], Qbar, D[i], LOAD, CLK, 1'b1, RESET);
  end
endgenerate
endmodule

// 1 bit register +ve edge, 
// Preset on nP=0, nR=1, reset on nP=1, nR=0;
// Undefined nP=0, nR=0
// normal operation nP=1, nR=1
module REG1(Q, Qbar, D, L, C, nP, nR);
/************************************/
input D, C, L;
input nP, nR;
output Q, Qbar;
/* wire for output */
wire Dsel;
/* instantiation of 1-bit mux_2x1 */
MUX1_2x1 mux1(Dsel, Q, D, L);
/* instantiation of D-flipflop */
D_FF dff(Q, Qbar, Dsel, C, nP, nR);
endmodule

// 1 bit flipflop +ve edge, 
// Preset on nP=0, nR=1, reset on nP=1, nR=0;
// Undefined nP=0, nR=0
// normal operation nP=1, nR=1
module D_FF(Q, Qbar, D, C, nP, nR);
/*********************************/
input D, C;
input nP, nR;
output Q, Qbar;
/* wire for output */
wire dQ, dQbar; // outputs from D_LATCH
wire Cbar;
/* inverter of C */
not not_G(Cbar, C);
/* +VE EDGED FF : Cbar to Master; C to Slave */
/* instantiation of D-latch : D_LATCH(Q, Qbar, D, C, nP, nR) */
D_LATCH d_inst(dQ, dQbar, D, Cbar, nP, nR); // +ve edge ff
/* instantiation of SR-latch : SR_LATCH(Q, Qbar, S, R, C, nP, nR) */
SR_LATCH sr_inst(Q, Qbar, dQ, dQbar, C, nP, nR); // +ve edge ff
endmodule

// 1 bit flipflop -ve edge, 
// Preset on nP=0, nR=1, reset on nP=1, nR=0;
// Undefined nP=0, nR=0
// normal operation nP=1, nR=1
module N_D_FF(Q, Qbar, D, C, nP, nR);
/*********************************/
input D, C;
input nP, nR;
output Q, Qbar;
/* wire for output */
wire dQ, dQbar; // outputs from D_LATCH
wire Cbar;
/* inverter of C */
not not_G(Cbar, C);
/* -VE EDGED FF : C to Master; Cbar to Slave */
D_LATCH d_inst(dQ, dQbar, D, C, nP, nR); // -ve edge ff
SR_LATCH sr_inst(Q, Qbar, dQ, dQbar, Cbar, nP, nR); // -ve edge ff
endmodule


// 1 bit D latch
// Preset on nP=0, nR=1, reset on nP=1, nR=0;
// Undefined nP=0, nR=0
// normal operation nP=1, nR=1
module D_LATCH(Q, Qbar, D, C, nP, nR);
/************************************/
input D, C;
input nP, nR;
output Q,Qbar;
/* wire for output */
wire Dbar, nout0, nout1; 
/* inverter of D */
not not_G(Dbar, D);
/* 2-input NAND gates */
nand nand2_G0(nout0, D, C);
nand nand2_G1(nout1, Dbar, C);
/* 3-input NAND gates with either preset(nP) or reset(nR) signals */
nand nand3_G2(Q, Qbar, nout0, nP);
nand nand3_G3(Qbar, Q, nout1, nR);
endmodule

// 1 bit SR latch
// Preset on nP=0, nR=1, reset on nP=1, nR=0;
// Undefined nP=0, nR=0
// normal operation nP=1, nR=1
module SR_LATCH(Q, Qbar, S, R, C, nP, nR);
/****************************************/
input S, R, C;
input nP, nR;
output Q, Qbar;
/* wire for output */
wire Sbar, Rbar;
/* 2-input NAND gates */
nand nand_G0(Sbar, S, C);
nand nand_G1(Rbar, R, C);
/* 3-input NAND gates with either preset(nP) or reset(nR) signals */
nand nand_G2(Q,    Qbar, Sbar, nP);
nand nand_G3(Qbar, Q,    Rbar, nR);
endmodule


// 5x32 Line decoder
module DECODER_5x32(D, I);
/***********************/
// output
output [31:0] D;
// input
input [4:0] I;
/* wire for output */
wire invI4;
wire [15:0] dout;
/* inverter for I[4] */
not not_G4(invI4, I[4]);
/* instantiation of DECODER_4x16 */
DECODER_4x16 ld4x16(dout, I[3:0]);
/* and gates     I3I2I1I0  I4       I4.I3I2I1I0 */
//===============================================
and  and_G0(D[0], dout[0], invI4); // 0.0000 :0  
and  and_G1(D[1], dout[1], invI4); // 0.0001 :1
and  and_G2(D[2], dout[2], invI4); // 0.0010 :2
and  and_G3(D[3], dout[3], invI4); // 0.0011 :3
and  and_G4(D[4], dout[4], invI4); // 0.0100 :4
and  and_G5(D[5], dout[5], invI4); // 0.0101 :5
and  and_G6(D[6], dout[6], invI4); // 0.0110 :6
and  and_G7(D[7], dout[7], invI4); // 0.0111 :7
and  and_G8(D[8], dout[8], invI4); // 0.1000 :8
and  and_G9(D[9], dout[9], invI4); // 0.1001 :9
and and_G10(D[10],dout[10], invI4);// 0.1010 :10
and and_G11(D[11],dout[11], invI4);// 0.1011 :11
and and_G12(D[12],dout[12], invI4);// 0.1100 :12
and and_G13(D[13],dout[13], invI4);// 0.1101 :13
and and_G14(D[14],dout[14], invI4);// 0.1110 :14
and and_G15(D[15],dout[15], invI4);// 0.1111 :15
//==============================================
and and_G16(D[16], dout[0], I[4]);// 1.0000 :16
and and_G17(D[17], dout[1], I[4]);// 1.0001 :17
and and_G18(D[18], dout[2], I[4]);// 1.0010 :18
and and_G19(D[19], dout[3], I[4]);// 1.0011 :19
and and_G20(D[20], dout[4], I[4]);// 1.0100 :20
and and_G21(D[21], dout[5], I[4]);// 1.0101 :21
and and_G22(D[22], dout[6], I[4]);// 1.0110 :22
and and_G23(D[23], dout[7], I[4]);// 1.0111 :23
and and_G24(D[24], dout[8], I[4]);// 1.1000 :24
and and_G25(D[25], dout[9], I[4]);// 1.1001 :25
and and_G26(D[26],dout[10], I[4]);// 1.1010 :26
and and_G27(D[27],dout[11], I[4]);// 1.1011 :27
and and_G28(D[28],dout[12], I[4]);// 1.1100 :28
and and_G29(D[29],dout[13], I[4]);// 1.1101 :29
and and_G30(D[30],dout[14], I[4]);// 1.1110 :30
and and_G31(D[31],dout[15], I[4]);// 1.1111 :31
//==============================================
endmodule


// 4x16 Line decoder
module DECODER_4x16(D, I);
/***********************/
// output
output [15:0] D;
// input
input [3:0] I;
/* wire for output */
wire invI3;
wire [7:0] dout;
/* inverter for I[3] */
not not_G3(invI3, I[3]);
/* instantiation of DECODER_3x8 */
DECODER_3x8 ld3x8(dout, I[2:0]);
/* AND gates     I2I1I0   I3       I3.I2I1I0 */
//============================================
and  and_G0(D[0], dout[0], invI3);// 0.000 :0 
and  and_G1(D[1], dout[1], invI3);// 0.001 :1
and  and_G2(D[2], dout[2], invI3);// 0.010 :2
and  and_G3(D[3], dout[3], invI3);// 0.011 :3
and  and_G4(D[4], dout[4], invI3);// 0.100 :4
and  and_G5(D[5], dout[5], invI3);// 0.101 :5
and  and_G6(D[6], dout[6], invI3);// 0.110 :6
and  and_G7(D[7], dout[7], invI3);// 0.111 :7
//============================================
and  and_G8(D[8], dout[0], I[3]);// 1.000 :8
and  and_G9(D[9], dout[1], I[3]);// 1.001 :9
and and_G10(D[10],dout[2], I[3]);// 1.010 :10
and and_G11(D[11],dout[3], I[3]);// 1.011 :11
and and_G12(D[12],dout[4], I[3]);// 1.100 :12
and and_G13(D[13],dout[5], I[3]);// 1.101 :13
and and_G14(D[14],dout[6], I[3]);// 1.110 :14
and and_G15(D[15],dout[7], I[3]);// 1.111 :15
//============================================
endmodule


// 3x8 Line decoder
module DECODER_3x8(D, I);
/***********************/
// output
output [7:0] D;
// input
input [2:0] I;
/* wire for output */
wire invI2;
wire [3:0] dout;
/* inverter for I[2] */
not not_G2(invI2, I[2]);
/* instantiation of DECODER_2x4 */
DECODER_2x4 ld2x4(dout, I[1:0]);
/* AND gates     I1I0     I2       I2.I1I0 */
//==========================================
and and_G0(D[0], dout[0], invI2);// 0.00 :0
and and_G1(D[1], dout[1], invI2);// 0.01 :1
and and_G2(D[2], dout[2], invI2);// 0.10 :2
and and_G3(D[3], dout[3], invI2);// 0.11 :3
//==========================================
and and_G4(D[4], dout[0], I[2]); // 1.00 :4
and and_G5(D[5], dout[1], I[2]); // 1.01 :5
and and_G6(D[6], dout[2], I[2]); // 1.10 :6
and and_G7(D[7], dout[3], I[2]); // 1.11 :7
//==========================================
endmodule


// 2x4 Line decoder
module DECODER_2x4(D, I);
/**********************/
// output
output [3:0] D;
// input
input [1:0] I;
/* wire for output */
wire invI0, invI1;
/* inverter for I[0] and I[1] */
not not_G0(invI0, I[0]);
not not_G1(invI1, I[1]);
/* AND gates     I0     I1        I0I1  */
and and_G0(D[0], invI0, invI1);// 00
and and_G1(D[1], I[0],  invI1);// 01
and and_G2(D[2], invI0, I[1]); // 10
and and_G3(D[3], I[0],  I[1]); // 11
endmodule


// 1-bit XOR with 32-input with zero
module XOR1_32x0(Zout, A);
/*************************/
// output
output Zout;
// input
input [31:0] A;
// wire for output
wire orOut[0:31];
genvar i;
generate
for (i = 0; i < 32; i = i + 1) begin: loop_i
  if (i == 0)
    or or_G(orOut[0], A[0], 1'b0);
  else 
    or or_G(orOut[i], A[i], orOut[i-1]);
end
endgenerate
buf bufZ(Zout, orOut[31]);
endmodule

// 1-bit output from 27-bit OR
module OR1_27bit(Y, A);
/**************************/
// 1-bit output
output Y;
// 27-bit input
input [26:0] A;
// wire for output
wire or0, or1, or2, or3, or4, or5, or6, or7, or8, or9,
or10, or11, or12, or13, or14, or15, or16, or17, or18, or19,
or20, or21, or22, or23, or24;
or or0G(or0, A[0], A[1]);
or or1G(or1, A[2], or0);
or or2G(or2, A[3], or1);
or or3G(or3, A[4], or2);
or or4G(or4, A[5], or3);
or or5G(or5, A[6], or4);
or or6G(or6, A[7], or5);
or or7G(or7, A[8], or6);
or or8G(or8, A[9], or7);
or or9G(or9, A[10], or8);
or or10G(or10, A[11], or9);
or or11G(or11, A[12], or10);
or or12G(or12, A[13], or11);
or or13G(or13, A[14], or12);
or or14G(or14, A[15], or13);
or or15G(or15, A[16], or14);
or or16G(or16, A[17], or15);
or or17G(or17, A[18], or16);
or or18G(or18, A[19], or17);
or or19G(or19, A[20], or18);
or or20G(or20, A[21], or19);
or or21G(or21, A[22], or20);
or or22G(or22, A[23], or21);
or or23G(or23, A[24], or22);
or or24G(or24, A[25], or23);
or or25G(Y, A[26], or24);
endmodule


/* 
* Special Registers PC, SP for project 3
* Please add the following into logic.v. 
* This is a preset pattern register where preset bit pattern 
* can be passed into it through parameter.
*/
module REG32_PP(Q, D, LOAD, CLK, RESET);
/***************************************/
parameter PATTERN = 32'h00000000;
output [31:0] Q;

input CLK, LOAD;
input [31:0] D;
input RESET;

wire [31:0] qbar;

genvar i;
generate
for(i=0; i<32; i=i+1)
begin : reg32_gen_loop
if (PATTERN[i] == 0)
REG1 reg_inst(.Q(Q[i]), .Qbar(qbar[i]), .D(D[i]), .L(LOAD), .C(CLK), .nP(1'b1), .nR(RESET));
else
REG1 reg_inst(.Q(Q[i]), .Qbar(qbar[i]), .D(D[i]), .L(LOAD), .C(CLK), .nP(RESET), .nR(1'b1));
end
endgenerate

endmodule
