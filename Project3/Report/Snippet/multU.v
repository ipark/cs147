// 32-bit unsigned multiplication
module MULT32_U(HI, LO, A, B);
// output list
output [31:0] HI;
output [31:0] LO;
// input list
input [31:0] A; // MCND
input [31:0] B; // MPLR
// wire for 32-bit width vector array 
wire [31:0]  andOut[0:31];
// wire for 1-bit width vector array of 32-bit Adder/Subtractor output
wire cout[0:31];
// wire for 32-bit width vector array of 32-bit Adder/Subtractor output
wire [31:0] sum[0:31];

genvar i;
generate
  for (i = 0; i < 32; i = i + 1) begin: and_loop
    // 32-bit AND gates with 32-bit MCND A, 32-bit repeat of MCPLR bit
    AND32_2x1 andG(andOut[i], A, {32{B[i]}});
    if (i == 0) begin
      assign cout[i] = 1'b0;
      assign sum[i] = andOut[0];
      buf buf_lo(LO[i], sum[i][0]);
      end
    else begin
      AND32_2x1 andG(andOut[i], A, {32{B[i]}});
      // 32-bit adder/subtractor with SnA=0 (i.e. adder)
      RC_ADD_SUB_32 adderi(sum[i], cout[i], andOut[i], {cout[i-1], sum[i-1][31:1]}, 1'b0);
      buf buf_lo(LO[i], sum[i][0]);
      end  
  end // EO-for
endgenerate
