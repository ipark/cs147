wire rw_load, invW, invR, WnR, RnW;
not not_W(invW, WRITE);
not not_R(invR, READ);
and and_WnR(WnR, invW, READ);
and and_RnW(RnW, invR, WRITE);
wire [31:0] bufZ32;
BUFIF1_32 bufZ(bufZ32, DATA_IN, RnW);
// REG32(Q, D, LOAD, CLK, RESET);
// input: D=DATA from MEMORY_64MB(DATA, READ, WRITE, ADDR, CLK, RST);
// output: Q=DATA_OUT
REG32 r32(DATA_OUT, bufZ32, WnR, CLK, RST);

// 32-bit BUFIF1 // Tri-state buffer, Active high en.
module BUFIF1_32(Y, A, Enable);
/*****************************/
//output 
output [31:0] Y;
//input
input [31:0] A;
input Enable;
// generate block
genvar i;
generate
  for (i = 0; i < 32; i = i + 1)
  begin: bufif1_32_gen_loop
    bufif1 bufif1_inst(Y[i], A[i], Enable);
  end
endgenerate
endmodule

