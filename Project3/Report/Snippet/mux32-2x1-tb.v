/* CS147 - Spring2019 - Inhee Park */
`timescale 1ns/1ps; // 1000, 3 digits after decimal 
module mux_tb;
/* Testbench for 32-bit 2x1 MUX */
reg [31:0] I0, I1;
reg S;
wire [31:0] Y;
/* instantiation */
MUX32_2x1 mux32_2x1_tb(.Y(Y), .I0(I0), .I1(I1), .S(S));
initial
begin
  I0=0; I1=0; S=0;
#5
  I0=32'b10000000000000000000000000000001;
  I1=32'b01111111111111111111111111111110;
  S=0;
#5
  I0=32'b10000000000000000000000000000001;
  I1=32'b01111111111111111111111111111110;
  S=1;
#5
  I0=0; I1=0; S=0;
end
endmodule
