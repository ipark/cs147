`timescale 1ns/1ps // 1000, 3 digits after decimal 
module logic_tb;
/* Testbench for 1-bit register */
// 1 bit register +ve edge, 
// Preset on nP=0, nR=1, reset on nP=1, nR=0;
// Undefined nP=0, nR=0
// normal operation nP=1, nR=1
reg D, C, L;
reg nP, nR;
wire Q, Qbar;
// REG1(Q, Qbar, D, L, C, nP, nR);
REG1 r1(.Q(Q), .Qbar(Qbar), .D(D), .L(L), .C(C), .nP(nP), .nR(nR));
initial
begin
  nP=1; nR=1; D=0; L=0; C=0;
#12  L=1; D=1;
#12  L=0; 
#12  L=1; D=0;
#12  L=0; 
$stop;
end

always
begin
  #5 C = ~C;
end
