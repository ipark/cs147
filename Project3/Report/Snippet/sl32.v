// Left shifter
module SHIFT32_L(Y, D, S);
/************************/
// output list
output [31:0] Y;
// input list
input [31:0] D;
input [4:0] S;
// 2-diemsional array for 1-bit wire intermediate output
wire  mout[0:4][0:31];  // mout[column_index][row_index]

genvar r, c;
generate
for (c = 0; c < 5; c = c + 1) begin: col_loop 
  for (r = 0; r < 32; r = r + 1) begin: row_loop
    if (c == 0) begin
      if (r == 0) 
        MUX1_2x1 mux_c0(mout[c][r], D[r], 1'b0,   S[c]); 
      else        
        MUX1_2x1 mux_c0(mout[c][r], D[r], D[r-1], S[c]); 
    end // EO(c==0)

    else if (c == 4) begin
      if (r < 2**c) 
        MUX1_2x1 mux_c4(Y[r], mout[c-1][r], 1'b0, S[c]); 
      else          
        MUX1_2x1 mux_c4(Y[r], mout[c-1][r], mout[c-1][r-2**c], S[c]); 
    end // EO(c==4)

    else begin
      if (r < 2**c) 
        MUX1_2x1 mux_c123(mout[c][r], mout[c-1][r], 1'b0, S[c]); 
      else
        MUX1_2x1 mux_c123(mout[c][r], mout[c-1][r], mout[c-1][r-2**c], S[c]); 
    end // EO(c=1,2,3)
  end // EO_row_loop
end // EO_col_loop
endgenerate
endmodule

