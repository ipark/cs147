
module DECODER_2x4(D, I);
/**********************/
// output
output [3:0] D;
// input
input [1:0] I;
/* wire for output */
wire invI0, invI1;
/* inverter for I[0] and I[1] */
not not_G0(invI0, I[0]);
not not_G1(invI1, I[1]);
/* AND gates     I0     I1        I0I1  */
and and_G0(D[0], invI0, invI1);// 00
and and_G1(D[1], I[0],  invI1);// 01
and and_G2(D[2], invI0, I[1]); // 10
and and_G3(D[3], I[0],  I[1]); // 11
endmodule
