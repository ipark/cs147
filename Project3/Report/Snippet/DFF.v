// 1 bit flipflop +ve edge, 
// Preset on nP=0, nR=1, reset on nP=1, nR=0;
// Undefined nP=0, nR=0
// normal operation nP=1, nR=1
module D_FF(Q, Qbar, D, C, nP, nR);
/*********************************/
input D, C;
input nP, nR;
output Q, Qbar;
/* wire for output */
wire dQ, dQbar; // outputs from D_LATCH
wire Cbar;
/* inverter of C */
not not_G(Cbar, C);
/* +VE EDGED FF : Cbar to Master; C to Slave */
/* instantiation of D-latch : D_LATCH(Q, Qbar, D, C, nP, nR) */
D_LATCH d_inst(dQ, dQbar, D, Cbar, nP, nR); // +ve edge ff
/* instantiation of SR-latch : SR_LATCH(Q, Qbar, S, R, C, nP, nR) */
SR_LATCH sr_inst(Q, Qbar, dQ, dQbar, C, nP, nR); // +ve edge ff
endmodule

// 1 bit flipflop -ve edge, 
// Preset on nP=0, nR=1, reset on nP=1, nR=0;
// Undefined nP=0, nR=0
// normal operation nP=1, nR=1
module N_D_FF(Q, Qbar, D, C, nP, nR);
/*********************************/
input D, C;
input nP, nR;
output Q, Qbar;
/* wire for output */
wire dQ, dQbar; // outputs from D_LATCH
wire Cbar;
/* inverter of C */
not not_G(Cbar, C);
/* -VE EDGED FF : C to Master; Cbar to Slave */
D_LATCH d_inst(dQ, dQbar, D, C, nP, nR); // -ve edge ff
SR_LATCH sr_inst(Q, Qbar, dQ, dQbar, Cbar, nP, nR); // -ve edge ff
endmodule
