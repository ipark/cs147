module RC_ADD_SUB_32(Y, CO, A, B, SnA);
// output list
output [`DATA_INDEX_LIMIT:0] Y; // sum
output CO; // carry-out
// input list
input [`DATA_INDEX_LIMIT:0] A;
input [`DATA_INDEX_LIMIT:0] B;
input SnA;
// wire for one output of full-adders
wire  cout0, cout1, cout2, ..., cout29, cout30, cout31;
// wire for xor gate output
wire  b0, b1, b2, b3, ..., b29, b30, b31;
// XOR Gates
xor xor_G0(b0, B[0], SnA);
xor xor_G1(b1, B[1], SnA);
xor xor_G2(b2, B[2], SnA);
...
xor xor_G29(b29, B[29], SnA);
xor xor_G30(b30, B[30], SnA);
xor xor_G31(b31, B[31], SnA);
// Full Adders
FULL_ADDER FA0(Y[0], cout0, A[0], b0, SnA);
FULL_ADDER FA1(Y[1], cout1, A[1], b1, cout0);
FULL_ADDER FA2(Y[2], cout2, A[2], b2, cout1);
...
FULL_ADDER FA29(Y[29], cout29, A[29], b29, cout28);
FULL_ADDER FA30(Y[30], cout30, A[30], b30, cout29);
FULL_ADDER FA31(Y[31], CO, A[31], b31, cout30);
endmodule
