// 4x16 Line decoder
module DECODER_4x16(D, I);
/***********************/
// output
output [15:0] D;
// input
input [3:0] I;
/* wire for output */
wire invI3;
wire [7:0] dout;
/* inverter for I[3] */
not not_G3(invI3, I[3]);
/* instantiation of DECODER_3x8 */
DECODER_3x8 ld3x8(dout, I[2:0]);
/* AND gates     I2I1I0   I3       I3.I2I1I0 */
//============================================
and  and_G0(D[0], dout[0], invI3);// 0.000 :0 
and  and_G1(D[1], dout[1], invI3);// 0.001 :1
and  and_G2(D[2], dout[2], invI3);// 0.010 :2
and  and_G3(D[3], dout[3], invI3);// 0.011 :3
and  and_G4(D[4], dout[4], invI3);// 0.100 :4
and  and_G5(D[5], dout[5], invI3);// 0.101 :5
and  and_G6(D[6], dout[6], invI3);// 0.110 :6
and  and_G7(D[7], dout[7], invI3);// 0.111 :7
//============================================
and  and_G8(D[8], dout[0], I[3]);// 1.000 :8
and  and_G9(D[9], dout[1], I[3]);// 1.001 :9
and and_G10(D[10],dout[2], I[3]);// 1.010 :10
and and_G11(D[11],dout[3], I[3]);// 1.011 :11
and and_G12(D[12],dout[4], I[3]);// 1.100 :12
and and_G13(D[13],dout[5], I[3]);// 1.101 :13
and and_G14(D[14],dout[6], I[3]);// 1.110 :14
and and_G15(D[15],dout[7], I[3]);// 1.111 :15
//============================================
endmodule
