/* CS147 - Spring2019 - Inhee Park */
`timescale 1ns/1ps; // 1000, 3 digits after decimal 
module rc_add_sub_32_tb;
/* registers for inputs */
reg [31:0] A;
reg [31:0] B;
reg SnA;
/* wires for outputs */
wire [31:0] Y;
wire CO;
/* instance */
RC_ADD_SUB_32 rc_add_sub_32bit_tb (.Y(Y), .CO(CO), .A(A), .B(B), .SnA(SnA));
initial
begin
     A=0; B=0; SnA=0;
#10  A=32'b10101010101010; B=32'b110011001; SnA=0; 
     // 10922(10) + 409(10) = 11331(10)
#10  A=32'b10101010101010; B=32'b110011001; SnA=1;
     // 10922(10) - 409(10) = 10513(10)
#10  A=32'b11111111111111111111111111111111; 
     B=32'b11111111111111111111111111111111;
     SnA=1; 
     // 2^32(=4294967295)-2^32=0
#10  A=32'b11111111111111111111111111111111;
     B=32'b11111111111111111111111111111111;
     SnA=0;
     // 2^32(=4294967295)+2^32=2^33=8589934590
#10  A=0; B=0; SnA=1;
end
endmodule

// output list
// input list
