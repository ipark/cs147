/* CS147 - Spring2019 - Inhee Park */
`timescale 1ns/1ps; // 1000, 3 digits after decimal 
module mux_tb;
/* registers for inputs */
reg I0, I1, S;
/* wire for outputs */
wire Y;
/* instance */
//module MUX1_2x1(Y,I0, I1, S);
MUX1_2x1 mux_2x1_tb(.Y(Y), .I0(I0), .I1(I1), .S(S));
initial
begin                
    I0=0; I1=0; S=0; //  0
//
#5  I0=0; I1=0; S=0; //  0
#5  I0=1; I1=0; S=0; //  1
#5  I0=1; I1=1; S=0; //  1
#5  I0=0; I1=0; S=1; //  0
//
#5  I0=0; I1=1; S=1; //  1
#5  I0=1; I1=0; S=1; //  0
#5  I0=1; I1=1; S=1; //  1
#5  I0=0; I1=0; S=0; //  0
//
#5  I0=0; I1=0; S=0; //  0
end
endmodule
