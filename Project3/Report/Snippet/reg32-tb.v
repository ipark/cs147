`timescale 1ns/1ps // 1000, 3 digits after decimal 
module logic_tb;
/* Testbench for 32-bit register */
// 32-bit register +ve edge, Reset on RESET=0
reg [31:0] D;
reg CLK, LOAD, RESET;
wire [31:0] Q;
//REG32(Q, D, LOAD, CLK, RESET);
REG32 r32(.Q(Q), .D(D), .LOAD(LOAD), .CLK(CLK), .RESET(RESET));
initial
begin
  D=0; LOAD=0; CLK=0; RESET=0;
#12 RESET=1; LOAD=1; D=12;
#10 LOAD=0; D=10;
#8  LOAD=1; D=8;
#4          D=4;
#15 LOAD=1; D=15;
#10 LOAD=0; D=10;
$stop;
end

always
begin
  #5 CLK = ~CLK;
end
