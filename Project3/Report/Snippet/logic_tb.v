/* CS147 - Spring2019 - Inhee Park */
`timescale 1ns/1ps // 1000, 3 digits after decimal 
module logic_tb;
/*************/

/* Testbench for 32-bit 2's complement */
/*
// input list
reg [31:0] A;
// output wire
wire [31:0] Y;
// instantiation
TWOSCOMP32 twoscompl32_tb(.Y(Y), .A(A));
initial
begin
    A=0;
#5  A='b0011110;
#5  A=0;
end
*/


/* Testbench for 64-bit 2's complement */
/*
// input list
reg [63:0] A;
// output wire
wire [63:0] Y;
// instantiation
TWOSCOMP64 twoscompl64_tb(.Y(Y), .A(A));
initial
begin
    A=0;
#5  A='b0011110;
#5  A=0;
end
*/


/* Testbench for 1-bit output from 27-bit OR */
/*
// input list
reg [26:0] A;
// output wire
wire Y;
// instantiation
OR1_27bit or1(.Y(Y), .A(A));
initial
begin
    A=0;
#5  A=27'b000000000000000000000000000;
#5  A=27'b000000000000000000000100000;
#5  A=27'b111111111111111111111111111;
#5  A=0;
end
*/

/* Testbench for SR-latch */
/*
// 1 bit SR latch
// Preset on nP=0, nR=1, reset on nP=1, nR=0;
// Undefined nP=0, nR=0
// normal operation nP=1, nR=1
reg S, R, C, nP, nR;
wire Q, Qbar;
SR_LATCH sr_latch_tb(.Q(Q), .Qbar(Qbar), .S(S), .R(R), .C(C), .nP(nP), .nR(nR));
initial
begin
  $monitor($time, "ns   S=%b, R=%b, Q=%b, Qbar=%b\n", S, R, Q, Qbar);
  nP=1; nR=1; // normal operation
#5 C=1; S=1; R=0;
#5 C=1; S=0; R=1;
#5 C=1; S=0; R=0;
end
/*


/* Testbench for D-latch */
/*
// 1 bit D latch
// Preset on nP=0, nR=1, reset on nP=1, nR=0;
// Undefined nP=0, nR=0
// normal operation nP=1, nR=1
reg D, C;
reg nP, nR;
wire Q, Qbar;
D_LATCH d_latch_tb(.Q(Q), .Qbar(Qbar), .D(D), .C(C), .nP(nP), .nR(nR));
initial
begin
nP=1; nR=1; // normal operation
#5 C=0; D=0;
#5 C=1; D=0;
#5 C=0; D=1;
#5 C=1; D=1;
#5 C=0; D=0;
#5 C=1; D=0;
end
*/


/* Testbench for +VE 1-bit Flip Flop */
// Preset on nP=0, nR=1, reset on nP=1, nR=0;
// Undefined nP=0, nR=0
// normal operation nP=1, nR=1
/*
reg D, C;
reg nP, nR;
wire Q, Qbar;
D_FF pdff_tb(.Q(Q), .Qbar(Qbar), .D(D), .C(C), .nP(nP), .nR(nR));

initial
begin
$monitor($time, "ns   C=%b, D=%b, Q=%b, Qbar=%b", C, D, Q, Qbar);
nP=1; nR=1; C=0; D=0; // normal operation
#3 D = 1; 
#1 D = 0; 
#5 D = 1; 
#7 D = 0; 
#2 D = 1; 
#8 D = 0; 
$stop; 
end

always // toggle C every 5ns
begin
  #5 C = ~C;
end
*/


/* Testbench for -VE 1-bit Flip Flop */
/*
// Preset on nP=0, nR=1, reset on nP=1, nR=0;
// Undefined nP=0, nR=0
// normal operation nP=1, nR=1
reg D, C;
reg nP, nR;
wire Q, Qbar;
N_D_FF ndff_tb(.Q(Q), .Qbar(Qbar), .D(D), .C(C), .nP(nP), .nR(nR));

initial
begin
$monitor($time, "ns   C=%b, D=%b, Q=%b, Qbar=%b", C, D, Q, Qbar);
nP=1; nR=1; C=0; D=0; // normal operation
#3 D = 1; 
#1 D = 0; 
#5 D = 1; 
#7 D = 0; 
#2 D = 1; 
#8 D = 0; 
$stop; 
end

always // toggle C every 5ns
begin
  #4 C = ~C;
end
*/

/* Testbench for 1-bit register */
/*
// 1 bit register +ve edge, 
// Preset on nP=0, nR=1, reset on nP=1, nR=0;
// Undefined nP=0, nR=0
// normal operation nP=1, nR=1
reg D, C, L;
reg nP, nR;
wire Q, Qbar;
// REG1(Q, Qbar, D, L, C, nP, nR);
REG1 r1(.Q(Q), .Qbar(Qbar), .D(D), .L(L), .C(C), .nP(nP), .nR(nR));
initial
begin
  nP=1; nR=1; D=0; L=0; C=0;
#12  L=1; D=1;
#12  L=0; 
#12  L=1; D=0;
#12  L=0; 
$stop;
end

always
begin
  #5 C = ~C;
end
*/


/* Testbench for 32-bit register */
/*
// 32-bit register +ve edge, Reset on RESET=0
reg [31:0] D;
reg CLK, LOAD, RESET;
wire [31:0] Q;
//REG32(Q, D, LOAD, CLK, RESET);
REG32 r32(.Q(Q), .D(D), .LOAD(LOAD), .CLK(CLK), .RESET(RESET));
initial
begin
  D=0; LOAD=0; CLK=0; RESET=0;
#12 RESET=1; LOAD=1; D=12;
#10 LOAD=0; D=10;
#8  LOAD=1; D=8;
#4          D=4;
#15 LOAD=1; D=15;
#10 LOAD=0; D=10;
$stop;
end

always
begin
  #5 CLK = ~CLK;
end
*/

/* Testbench for 2-to-4 line decoder */
/*
// 2x4 Line decoder
wire [3:0] D;
reg [1:0] I;
//DECODER_2x4(D, I);
DECODER_2x4 d2x4(.D(D), .I(I));
initial
begin
    I = 0;
#10 I = 1;
#10 I = 2;
#10 I = 3;
    I = 0;
$stop;
end
*/

/* Testbench for 3-to-8 line decoder */
/*
// 3x8 Line decoder
reg [2:0] I;
wire [7:0] D;
//DECODER_5x32(D, I);
DECODER_3x8 d3x8(.D(D), .I(I));
initial
begin
    I = 0;
#10 I = 1;
#10 I = 2;
#10 I = 3;
#10 I = 4;
#10 I = 5;
#10 I = 6;
#10 I = 7;
#10 I = 0;
$stop;
end
*/


/* Testbench for 4-to-16 line decoder */
/*
// 4x16 Line decoder
reg [3:0] I;
wire [15:0] D;
//DECODER_4x16(D, I);
DECODER_4x16 d4x16(.D(D), .I(I));
initial
begin
    I = 0;
#10 I = 1;
#10 I = 3;
#10 I = 5;
#10 I = 7;
#10 I = 8;
#10 I = 10;
#10 I = 12;
#10 I = 14;
#10 I = 0;
$stop;
end
*/

/* Testbench for 5-to-32 line decoder */
// 5x32 Line decoder
reg [4:0] I;
wire [31:0] D;
//DECODER_5x32(D, I);
DECODER_5x32 d5x32(.D(D), .I(I));
initial
begin
    I = 0;
#10 I = 1;
#10 I = 2;
#10 I = 3;
#10 I = 4;
#10 I = 5;
#10 I = 27;
#10 I = 28;
#10 I = 29;
#10 I = 30;
#10 I = 31;
$stop;
end


/////////
endmodule
